import { lazy } from 'react'
import { Navigate } from 'react-router-dom'


// 工作台
const WorkBench = lazy(() => import('../../views/WorkBench/WorkBench'))

// 个人资料
const PersonalData = lazy(() => import('../../views/WorkBench/PersonalData'))

// 居民管理
const ResidentManagement = lazy(() => import('../../views/Resident/ResidentManagement'))
const Addlist = lazy(() => import('../../views/Resident/operate/Add'))
const Revise = lazy(() => import('../../views/Resident/operate/revise'))
const Detail =lazy(()=>import('../../views/Resident/operate/detail'))


// 数据统计
const DataProfile = lazy(() => import('../../views/DataStatistics/DataProfile')) //数据概况
const SigningPerformance = lazy(() => import('../../views/DataStatistics/SigningPerformance')) //签约业绩统计


export default [
    // 工作台
    {
        path: "/MyLayout/WorkBench",
        element: <WorkBench />
    },

    // 个人资料
    {
        path: "/MyLayout/PersonalData",
        element: <PersonalData />
    },

    // 居民管理
    {
        path: "/MyLayout/ResidentManagement",
        element: <ResidentManagement />,

    },
    {
        path: '/MyLayout/Addlist',
        element: <Addlist />
    },
    {
        path: '/MyLayout/Revise',
        element: <Revise />
    },
    {
        path: '/MyLayout/detail',
        element: <Detail/>
    },



    // 数据统计
    {
        path: "/MyLayout/DataProfile",
        element: <DataProfile />
    },
    {
        path: "/MyLayout/SigningPerformance",
        element: <SigningPerformance />
    },
   

]