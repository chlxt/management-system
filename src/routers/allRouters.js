import { lazy, Suspense } from 'react'
import { Navigate } from "react-router-dom"

import router from '../routers/index'

// 导入路由模块
import WoReDasta from './WorkBench-Resident-DataStatistics/index'
import ConSer from './Contract-Service/index'
import DataMan from './DataManagement/index'
import SysSet from './SystemSettings/index'


// 定义全部子路由
export const allRouters = [
    // 工作台
    {
        path: '/MyLayout',
        element: <Navigate to='/MyLayout/WorkBench' />
    },
    // 居民管理、数据统计
    ...WoReDasta,

    // 签约管理、服务管理
    ...ConSer,

    // 资料管理
    ...DataMan,

    // 系统设置
    ...SysSet
]

export default function getRouter() {
    console.log("getRoutes()");
    // 1.获取当前用户权限,
    // /创建一个空数组,用来存储路由对象
    let checkedkeys = []

    let currentKeys = sessionStorage.getItem('grants')
    if (currentKeys) {
        checkedkeys = JSON.parse(currentKeys)
    }
    if (checkedkeys.length === 0) {
        checkedkeys = ['/MyLayout/WorkBench', 'sign', '/MyLayout/PendingContract', '/MyLayout/SigningRecord', 'serve', '/MyLayout/PendingService', '/MyLayout/ServiceRecord', '/MyLayout/ResidentManagement', 'data', '/MyLayout/Organizational', '/MyLayout/Team', '/MyLayout/Doctor', '/MyLayout/ServicePackage', '/MyLayout/ServiceProject', 'stat', '/MyLayout/DataProfile', '/MyLayout/SigningPerformance', '/MyLayout/ServicePerformance', 'sys', '/MyLayout/LabelManagement', '/MyLayout/RoleManagement', '/MyLayout/InformationManagement', '/MyLayout/DrugManagement', '/MyLayout/BannerManagement']
    }


    //2.获取当前用户的路由配置
    allRouters.forEach(item => {
        if (checkedkeys.includes(item.title)) {
            //动态添加路由配置, vueRouter对象.addRoute(父级路由的name，路由配置);
            let currRoute = { ...item }

            // let arr = ["0", "0", "0", "0"];
            // if (checkedkeys.includes(item.title + "-C")) {
            //     arr[0] = "1";
            // }
            // if (checkedkeys.includes(item.title + "-R")) {
            //     arr[1] = "1";
            // }
            // if (checkedkeys.includes(item.title + "-U")) {
            //     arr[2] = "1";
            // }
            // if (checkedkeys.includes(item.title + "-D")) {
            //     arr[3] = "1";
            // }

            // currRoute.meta = {
            //     crud: arr.join("")
            // }

            router.addRoute('MyLayout', currRoute)
        }
    })
}

export const clearCurrentRoutes = () => {
    // 1.获取当前用户权限,
    // /创建一个空数组,用来存储路由对象
    let checkedkeys = []

    const grants = sessionStorage.getItem('grants')
    if (grants) {
        checkedkeys = JSON.parse(grants)
    }
    if (checkedkeys.length == 0) {
        checkedkeys = ['/MyLayout/WorkBench', 'sign', '/MyLayout/PendingContract', '/MyLayout/SigningRecord', 'serve', '/MyLayout/PendingService', '/MyLayout/ServiceRecord', '/MyLayout/ResidentManagement', 'data', '/MyLayout/Organizational', '/MyLayout/Team', '/MyLayout/Doctor', '/MyLayout/ServicePackage', '/MyLayout/ServiceProject', 'stat', '/MyLayout/DataProfile', '/MyLayout/SigningPerformance', '/MyLayout/ServicePerformance', 'sys', '/MyLayout/LabelManagement', '/MyLayout/RoleManagement', '/MyLayout/InformationManagement', '/MyLayout/DrugManagement', '/MyLayout/BannerManagement']
    }


    //2.删除当前用户的路由配置
    allRouters.forEach(item => {
        if (checkedkeys.includes(item.title)) {
            //动态添加路由配置, vueRouter对象.addRoute(父级路由的name，路由配置);
            router.removeRoute(item.name)
        }
    })
}