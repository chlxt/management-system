import { lazy } from 'react'

// 签约管理
const PendingContract = lazy(() => import('../../views/Contract/PendingContract')) //待处理签约
const SigningRecord = lazy(() => import('../../views/Contract/SigningRecord')) //签约记录
const PendingContractContent = lazy(() => import('../../views/Contract/PendingContract/Conent')) //待处理签约查看详情
const PendingContractEdit = lazy(() => import('../../views/Contract/PendingContract/edit')) //待处理签约的修改
const SigningRecordContent = lazy(() => import('../../views/Contract/SigningRecord/Content'))//签约记录的查看详情
const SigningRecordEdit = lazy(() => import('../../views/Contract/SigningRecord/edit'))
// 服务管理
const PendingService = lazy(() => import('../../views/Service/PendingService')) //待处理服务
const ServiceRecord = lazy(() => import('../../views/Service/ServiceRecord')) //服务记录
const ServiceRecordConent = lazy(() => import('../../views/Service/ServiceRecordConent')) //服务记录的查看详情
const AddService = lazy(() => import('../../views/Service/pendingService/addService'))//新增服务
const ServiceContent = lazy(() => import('../../views/Service/pendingService/pendingServiceConent'))//待服务查看详情
const PendingServiceEdit = lazy(() => import('../../views/Service/pendingService/edit'))//待服务编辑按钮

export default [

    // 签约管理
    {
        path: "/MyLayout/PendingContract",
        element: <PendingContract />
    },
    {
        path: "/MyLayout/SigningRecord",
        element: <SigningRecord />
    },
    {
        path: "/MyLayout/PendingContractContent/:value",
        element: <PendingContractContent />
    },
    {
        path: "/MyLayout/SigningRecordContent/:value",
        element: <SigningRecordContent />

    },
    {
        path: "/MyLayout/PendingContractEdit/:value",
        element: <PendingContractEdit />
    },
    {
        path: "/MyLayout/SigningRecordEdit/:value",
        element: <SigningRecordEdit />
    },

    // 服务管理
    {
        path: "/MyLayout/PendingService",
        element: <PendingService />
    },
    {
        path: "/MyLayout/ServiceRecord",
        element: <ServiceRecord />
    },
    {
        path: "/MyLayout/addService",
        element: <AddService />
    },
    {
        path: "/MyLayout/ServiceContent/:value",
        element: <ServiceContent />
    },
    {
        path: "/MyLayout/PendingServiceEdit/:value",
        element: <PendingServiceEdit />
    }, {
        path: "/MyLayout/ServiceRecordConent/:value",
        element: <ServiceRecordConent />
    },

]