import { lazy, Suspense } from 'react'
import { Navigate } from "react-router-dom"
// import { allRoutes } from './sonrouter'

// 导入路由模块
import WoReDasta from './WorkBench-Resident-DataStatistics/index'
import ConSer from './Contract-Service/index'
import DataMan from './DataManagement/index'
import SysSet from './SystemSettings/index'

// 布局(一级)
const MyLayout = lazy(() => import('../components/MyLayout')) //布局
const Login = lazy(() => import('../views/Login/login')) //登录
const NotFound = lazy(() => import('../views/NotFound/NotFound')) //登录

interface Router {
    name?: string;
    path: string;
    children?: Array<Router>;
    element: any;
}

// 路由表
const routes: Array<Router> = [
    // 重定向
    {
        path: '/',
        element: <Navigate to='/Login' />
    },

    // 登录
    {
        path: "/Login",
        element: <Suspense> <Login /> </Suspense>
    },

    // 布局
    {
        path: "/MyLayout",
        name: "MyLayout",
        element: <MyLayout />,
        children: [
            // 工作台
            {
                path: '/MyLayout',
                element: <Navigate to='/MyLayout/WorkBench' />
            },
            // 居民管理、数据统计
            ...WoReDasta,

            // 签约管理、服务管理
            ...ConSer,

            // 资料管理
            ...DataMan,

            // 系统设置
            ...SysSet
        ]
    },

    // 报错
    {
        path: "*",
        element: <NotFound />
    },
]


export default routes;