import { lazy } from 'react'

// 系统设置
const LabelManagement = lazy(() => import('../../views/SystemSettings/LabelManagement/LabelManagement')) //标签管理
const RoleManagement = lazy(() => import('../../views/SystemSettings/RoleManagement/RoleManagement')) //角色管理
const InformationManagement = lazy(() => import('../../views/SystemSettings/InformationManagement/InformationManagement')) //咨讯管理
const DrugManagement = lazy(() => import('../../views/SystemSettings/DrugManagement/DrugManagement')) //药品管理
const BannerManagement = lazy(() => import('../../views/SystemSettings/BannerManagement/BannerManagement')) //轮播图管理
// 新增标签\修改标签
const AddLabel = lazy(() => import('../../views/SystemSettings/LabelManagement/AddLabel'))
const UpdateLabel = lazy(() => import('../../views/SystemSettings/LabelManagement/UpdateLabel'))
// 新增管理员\修改管理员
const AddMan = lazy(() => import('../../views/SystemSettings/RoleManagement/AddMan'))
const UpdateMan = lazy(() => import('../../views/SystemSettings/RoleManagement/UpdateMan'))
// 发布文章\编辑文章
const PubText = lazy(() => import('../../views/SystemSettings/InformationManagement/PubText'))
const UpdatePub = lazy(() => import('../../views/SystemSettings/InformationManagement/UpdatePub'))
// 新增药品\编辑药品
const PubDrug = lazy(() => import('../../views/SystemSettings/DrugManagement/PubDrug'))
const UpdateDrug = lazy(() => import('../../views/SystemSettings/DrugManagement/UpdateDrug'))
// 新增轮播图\编辑轮播图
const AddBan = lazy(() => import('../../views/SystemSettings/BannerManagement/AddBan'))
const UpdateBan = lazy(() => import('../../views/SystemSettings/BannerManagement/UpdateBan'))


export default [
    // 系统设置
    {
        path: "/MyLayout/LabelManagement",
        element: <LabelManagement />
    },
    {
        path: "/MyLayout/RoleManagement",
        element: <RoleManagement />
    },
    {
        path: "/MyLayout/InformationManagement",
        element: <InformationManagement />
    },
    {
        path: "/MyLayout/DrugManagement",
        element: <DrugManagement />
    },
    {
        path: "/MyLayout/BannerManagement",
        element: <BannerManagement />
    },
    // 新增标签、修改标签
    {
        path: "/MyLayout/AddLabel",
        element: <AddLabel />
    },
    {
        path: "/MyLayout/UpdateLabel",
        element: <UpdateLabel />
    },
    // 新增管理员、修改管理员
    {
        path: "/MyLayout/AddMan",
        element: <AddMan />
    },
    {
        path: "/MyLayout/UpdateMan",
        element: <UpdateMan />
    },
    // 发布文章、编辑文章
    {
        path: "/MyLayout/PubText",
        element: <PubText />
    },
    {
        path: "/MyLayout/UpdatePub",
        element: <UpdatePub />
    },
    // 发布药品、编辑药品
    {
        path: "/MyLayout/PubDrug",
        element: <PubDrug />
    },
    {
        path: "/MyLayout/UpdateDrug",
        element: <UpdateDrug />
    },
    // 新增轮播图、编辑轮播图
    {
        path: "/MyLayout/AddBan",
        element: <AddBan />
    },
    {
        path: "/MyLayout/UpdateBan",
        element: <UpdateBan />
    },
]