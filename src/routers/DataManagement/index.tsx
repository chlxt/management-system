import { lazy } from 'react'
import { Navigate } from 'react-router-dom'

// 资料管理
const Organizational = lazy(() => import('../../views/DataManagement/Organizational')) //机构管理
const Team = lazy(() => import('../../views/DataManagement/Team')) //团队管理
const Doctor = lazy(() => import('../../views/DataManagement/Doctor')) //医生管理
const ServicePackage = lazy(() => import('../../views/DataManagement/ServicePackage')) //服务包管理
const ServiceProject = lazy(() => import('../../views/DataManagement/ServiceProject')) //服务项目管理
const Edit = lazy(() => import('../../views/DataManagement/organ/Edit'))
const Add = lazy(() => import('../../views/DataManagement/organ/Add'))
const ManageTeam = lazy(() => import("../../views/DataManagement/team/Manage"))
const JoinTeam = lazy(() => import("../../views/DataManagement/team/Join"))
const JoinDetail = lazy(() => import("../../views/DataManagement/team/JoinDetail"))
const ManageDetail = lazy(() => import("../../views/DataManagement/team/ManageDetail"))
const AddTeam = lazy(() => import("../../views/DataManagement/team/addteam"))
const EditDoctor = lazy(() => import("../../views/DataManagement/doctor/edit"))
const AddDoctor = lazy(() => import("../../views/DataManagement/doctor/Adddoctor"))
const PackageDetail = lazy(() => import("../../views/DataManagement/package/Detail"))
const DoctorDetail = lazy(() => import("../../views/DataManagement/doctor/DoctorDetail"))
const AddPack = lazy(() => import("../../views/DataManagement/package/Addpack"))
const EditPack = lazy(() => import("../../views/DataManagement/package/Editpack"))
const Project = lazy(() => import("../../views/DataManagement/Project/AddProject"))
const EditProject = lazy(() => import("../../views/DataManagement/Project/EditProject"))

export default [
    // 资料管理
    {
        path: "/MyLayout/Organizational",
        element: <Organizational />,
    },
    // 团队管理
    {
        path: "/MyLayout/Team",
        element: <Team />,
        children: [
            {
                path: '/MyLayout/Team',
                element: <Navigate to="/MyLayout/Team/Manage" />
            },
            {
                path: '/MyLayout/Team/Manage',
                element: <ManageTeam />,
            },
            {
                path: '/MyLayout/Team/Join',
                element: <JoinTeam />,

            }
        ]
    },
    {
        path: "/MyLayout/Doctor",
        element: <Doctor />
    },
    {
        path: "/MyLayout/ServicePackage",
        element: <ServicePackage />
    },
    {
        path: "/MyLayout/ServiceProject",
        element: <ServiceProject />
    },
    // 机构管理的点击编辑页面
    {
        path: "/MyLayout/Edit",
        element: <Edit />
    },
    // 机构管理的新增机构页面
    {
        path: "/MyLayout/Add",
        element: <Add />
    },
    // 团队管理的我管理的队伍的详情页
    {
        path: '/MyLayout/JoinDetail',
        element: <JoinDetail />
    },
    {
        path: '/MyLayout/ManageDetail',
        element: <ManageDetail />
    },
    // 新增医生团队
    {
        path: '/MyLayout/AddTeam',
        element: <AddTeam />
    },
    // 编辑医生
    {
        path: '/MyLayout/EditDoctor',
        element: <EditDoctor />
    },
    // 新增医生
    {
        path: '/MyLayout/AddDoctor',
        element: <AddDoctor />
    },
    // 服务包详情
    {
        path: '/MyLayout/PackageDetail',
        element: <PackageDetail />
    },
    // 新增服务包
    {
        path: '/MyLayout/AddPack',
        element: <AddPack />
    },
    // 编辑服务包
    {
        path: '/MyLayout/EditPack',
        element: <EditPack />
    },
    // 医生详情
    {
        path: '/MyLayout/DoctorDetail',
        element: <DoctorDetail />
    },
    // 新增服务项目
    {
        path: '/MyLayout/Project',
        element: <Project />
    }, // 新增服务项目
    {
        path: '/MyLayout/EditProject',
        element: <EditProject />
    }
]