import React from "react";
import { DesktopOutlined, EditOutlined, UserOutlined, TeamOutlined, FileOutlined, PieChartOutlined, SettingOutlined } from '@ant-design/icons';

// // 工作台
// const WorkBench = lazy(() => import('../../views/WorkBench/WorkBench'))
// // 居民管理
// const ResidentManagement = lazy(() => import('../../views/Resident/ResidentManagement'))
// // 数据统计
// const DataProfile = lazy(() => import('../../views/DataStatistics/DataProfile')) //数据概况
// const SigningPerformance = lazy(() => import('../../views/DataStatistics/SigningPerformance')) //签约业绩统计
// const ServicePerformance = lazy(() => import('../../views/DataStatistics/ServicePerformance')) //服务业绩统计

// // 签约管理
// const PendingContract = lazy(() => import('../../views/Contract/PendingContract')) //待处理签约
// const SigningRecord = lazy(() => import('../../views/Contract/SigningRecord')) //签约记录
// // 服务管理
// const PendingService = lazy(() => import('../../views/Service/PendingService')) //待处理服务
// const ServiceRecord = lazy(() => import('../../views/Service/ServiceRecord')) //服务记录

// // 资料管理
// const Organizational = lazy(() => import('../../views/DataManagement/Organizational')) //机构管理
// const Team = lazy(() => import('../../views/DataManagement/Team')) //团队管理
// const Doctor = lazy(() => import('../../views/DataManagement/Doctor')) //医生管理
// const ServicePackage = lazy(() => import('../../views/DataManagement/ServicePackage')) //服务包管理
// const ServiceProject = lazy(() => import('../../views/DataManagement/ServiceProject')) //服务项目管理

// // 系统设置
// const LabelManagement = lazy(() => import('../../views/SystemSettings/LabelManagement/LabelManagement')) //标签管理
// const RoleManagement = lazy(() => import('../../views/SystemSettings/RoleManagement/RoleManagement')) //角色管理
// const InformationManagement = lazy(() => import('../../views/SystemSettings/InformationManagement/InformationManagement')) //咨讯管理
// const DrugManagement = lazy(() => import('../../views/SystemSettings/DrugManagement/DrugManagement')) //药品管理
// const BannerManagement = lazy(() => import('../../views/SystemSettings/BannerManagement/BannerManagement')) //轮播图管理

// 所有菜单数据
export const menuData = [
    {
        label: '工作台',
        title: '工作台',
        key: '/MyLayout/WorkBench',
        icon: React.createElement(DesktopOutlined),

        // element: <Suspense><WorkBench /></Suspense>
    },
    {
        label: '签约管理',
        title: '签约管理',
        key: 'sign',
        icon: React.createElement(EditOutlined),
        children: [
            {
                label: "待处理签约",
                title: '待处理签约',
                key: '/MyLayout/PendingContract',
                // element: <Suspense><PendingContract /></Suspense>
            },
            {
                label: "签约记录",
                title: '签约记录',
                key: '/MyLayout/SigningRecord',
                // element: <Suspense><SigningRecord /></Suspense>
            },
        ],
    },
    {
        label: '服务管理',
        title: '服务管理',
        key: 'serve',
        icon: React.createElement(UserOutlined),
        children: [
            {
                label: "待处理服务",
                title: '待处理服务',
                key: '/MyLayout/PendingService',
                // element: <Suspense><PendingService /></Suspense>
            },
            {
                label: "服务记录",
                title: '服务记录',
                key: '/MyLayout/ServiceRecord',
                // element: <Suspense><ServiceRecord /></Suspense>
            },
        ],
    },
    {
        label: '居民管理',
        title: '居民管理',
        key: '/MyLayout/ResidentManagement',
        icon: React.createElement(TeamOutlined),
        // element: <Suspense><ResidentManagement /></Suspense>,
    },
    {
        label: '资料管理',
        title: '资料管理',
        key: 'data',
        icon: React.createElement(FileOutlined),
        children: [
            {
                label: "机构管理",
                title: '机构管理',
                key: '/MyLayout/Organizational',
                // element: <Suspense><Organizational /></Suspense>,
            },
            {
                label: "团队管理",
                title: '团队管理',
                key: '/MyLayout/Team',
                // element: <Suspense><Team /></Suspense>,
            },
            {
                label: "医生管理",
                title: '医生管理',
                key: '/MyLayout/Doctor',
                // element: <Suspense><Doctor /></Suspense>
            },
            {
                label: "服务包管理",
                title: '服务包管理',
                key: '/MyLayout/ServicePackage',
                // element: <Suspense><ServicePackage /></Suspense>
            },
            {
                label: "服务项目管理",
                title: '服务项目管理',
                key: '/MyLayout/ServiceProject',
                // element: <Suspense><ServiceProject /></Suspense>
            },
        ],
    },
    {
        label: '数据统计',
        title: '数据统计',
        key: 'stat',
        icon: React.createElement(PieChartOutlined),
        children: [
            {
                label: "数据概况",
                title: '数据概况',
                key: '/MyLayout/DataProfile',
                // element: <Suspense><DataProfile /></Suspense>
            },
            {
                label: "签约业绩统计",
                title: '签约业绩统计',
                key: '/MyLayout/SigningPerformance',
                // element: <Suspense><SigningPerformance /></Suspense>
            },
        ],
    },
    {
        label: '系统设置',
        title: '系统设置',
        key: 'sys',
        icon: React.createElement(SettingOutlined),
        children: [
            {
                label: "标签管理",
                title: '标签管理',
                key: '/MyLayout/LabelManagement',
                // element: <Suspense><LabelManagement /></Suspense>
            },
            {
                label: "角色管理",
                title: '角色管理',
                key: '/MyLayout/RoleManagement',
                // element: <Suspense><RoleManagement /></Suspense>
            },
            {
                label: "咨讯管理",
                title: '咨讯管理',
                key: '/MyLayout/InformationManagement',
                // element: <Suspense><InformationManagement /></Suspense>
            },
            {
                label: "药品管理",
                title: '药品管理',
                key: '/MyLayout/DrugManagement',
                // element: <Suspense><DrugManagement /></Suspense>
            },
            {
                label: "轮播图管理",
                title: '轮播图管理',
                key: '/MyLayout/BannerManagement',
                // element: <Suspense><BannerManagement /></Suspense>
            },
        ],
    },
]

// 获取当前的菜单数据（动态菜单）
// 根据当前用户的权限，在所有菜单数据中过滤出当前用户的菜单
export default function getMenus() {
    // 1、获取该用户的权限
    let checkedKeys = [];

    if (sessionStorage.getItem("grants")) {
        checkedKeys = JSON.parse(sessionStorage.getItem("grants"));
    }

    // 若没有权限的(默认获取得两个值)都显示
    if (checkedKeys.length === 0) {
        checkedKeys = ['/MyLayout/WorkBench', 'sign', '/MyLayout/PendingContract', '/MyLayout/SigningRecord', 'serve', '/MyLayout/PendingService', '/MyLayout/ServiceRecord', '/MyLayout/ResidentManagement', 'data', '/MyLayout/Organizational', '/MyLayout/Team', '/MyLayout/Doctor', '/MyLayout/ServicePackage', '/MyLayout/ServiceProject', 'stat', '/MyLayout/DataProfile', '/MyLayout/SigningPerformance', '/MyLayout/ServicePerformance', 'sys', '/MyLayout/LabelManagement', '/MyLayout/RoleManagement', '/MyLayout/InformationManagement', '/MyLayout/DrugManagement', '/MyLayout/BannerManagement']
    }

    // 2、根据用户权限产生用户对应的菜单(过滤)
    // 存储当前用户的菜单项
    let currMenus = [];
    // 遍历菜单
    menuData.forEach(item => {
        // 处理第一级菜单
        if (checkedKeys.some(m => m == item.key)) {
            currMenus.push({ ...item });

            // 处理第二级菜单
            if (item.children && Array.isArray(item.children)) {

                let arr = [];
                // 遍历子一级菜单
                item.children.forEach(sonItem => {
                    if (checkedKeys.some(m => m == sonItem.key)) {
                        let obj = { ...sonItem };
                        obj.children = undefined;
                        arr.push(obj);

                        // 处理crud：
                        if (sonItem.children) {
                            obj.crud = "";
                            for (let i = 0; i < sonItem.children.length; i++) {
                                obj.crud += checkedKeys.some(m => m == sonItem.children[i].key) ? "1" : "0"
                            }
                        }
                    }
                })
                currMenus[currMenus.length - 1].children = arr;
            }
        }
    })

    console.log("currMenus", currMenus);
    return currMenus;
}