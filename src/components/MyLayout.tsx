import { DownOutlined } from '@ant-design/icons';
// 导入菜单
import getMenus from '../grants/menuData';

import React, { Suspense, useState } from 'react';
import {
  DesktopOutlined,
  EditOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined,
  SettingOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from '@ant-design/icons';
import { MenuProps, message } from 'antd';
import { Breadcrumb, Layout, Menu, theme, Dropdown, Space } from 'antd';
// 导入logo
import logo from "../assets/logo.jpg"
import { Outlet, Route, useNavigate } from 'react-router-dom';
import { clearCurrentRoutes } from '../routers/allRouters';

const { Header, Content, Footer, Sider } = Layout;

const MyLayout: React.FC = () => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  // 
  const push = useNavigate()
  const select = (item: any) => {
    // console.log(item);
    push(item.key)
  }
  const items: MenuProps['items'] = [
    {
      label: <p onClick={() => { push('/Login') }}>退出登录</p>,
      key: '0',
    },

  ];
  //  
  const menuData = getMenus()
  return (
    <Layout style={{ minHeight: '100vh' }} hasSider>
      <Sider style={{ overflow: 'auto', height: '100vh' }} collapsed={collapsed} >
        <div className="title" style={{ height: 32, margin: 16, background: 'rgba(255, 255, 255, 0.2)', overflow: 'hidden' }} >家庭医生后台管理系统</div>
        <Menu theme="light" defaultSelectedKeys={['/Layout']} mode="inline" items={menuData} onSelect={select} />
      </Sider>
      <Layout className="site-layout" style={{ width: '100%' }}>
        <Header style={{ padding: 0, background: colorBgContainer, display: 'flex', justifyContent: 'space-between' }} >
          <div className="top-left" style={{ textAlign: 'center', marginLeft: 20 }}>
            {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: () => setCollapsed(!collapsed),
            })}
          </div>
          <Dropdown menu={{ items }} trigger={['click']}>
            <a onClick={(e) => e.preventDefault()}>
              <div className="logo" style={{ marginRight: 20 }}>
                <img src={logo} alt="家庭医生" />
              </div>
            </a>
          </Dropdown>
          {/* <div className="logo" style={{ marginRight: 20 }}>
            <img src={logo} alt="家庭医生" />
          </div> */}
        </Header>
        <Content style={{ margin: '0 16px' }}>
          {/* <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>User</Breadcrumb.Item>
            <Breadcrumb.Item>Bill</Breadcrumb.Item>
          </Breadcrumb> */}
          <div style={{ marginTop: 16, padding: 24, minHeight: '80vh', background: colorBgContainer }}>
            <Suspense>
              <Outlet />
              {/* {
                allRoutes.map(item => <Route path={"/MyLayout" + item.path} element={item.element} />)
              } */}

            </Suspense>
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>copyright ©2023 果宝特攻</Footer>
      </Layout>
    </Layout>
  );
};

export default MyLayout;