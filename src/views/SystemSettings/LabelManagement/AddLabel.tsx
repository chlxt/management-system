import React, { useState } from 'react';
import '../style/public.scss'
import { Button, Form, Input, Select, DatePicker, Upload, message, Switch } from 'antd';
import type { FormInstance } from 'antd/es/form';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { Label_add, Label_list, Label_revisestatus } from '../../../axios/api'
import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';

const { RangePicker } = DatePicker;

const onChangetime = (
    value: DatePickerProps['value'] | RangePickerProps['value'],
    dateString: [string, string] | string,
) => {
    console.log('Selected Time: ', value);
    console.log('Formatted Selected Time: ', dateString);
};

const onOk = (value: DatePickerProps['value'] | RangePickerProps['value']) => {
    console.log('onOk: ', value);
};

const { Option } = Select;
interface Props {
    id: string;
    tagname: string;
    name: string;
    time: string;
    status: boolean;
}
const PubText: React.FC = () => {
    const [form] = Form.useForm()
    const push = useNavigate()
    const [data, setData] = useState([])
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    const formRef = React.useRef<FormInstance>(null);

    const onReset = () => {
        formRef.current?.resetFields();
    };

    // 获取label列表
    const labellist = () => {
        Label_list().then((res) => {
            console.log(res.data)
            setData(res.data.labeldata)
        })
    }

    // 修改标签使用状态
    const changeStatus = () => {

        Label_revisestatus({ id: val }).then((res) => {
            // console.log(res.status)
            if (res.status === 200) {
                labellist()
            }
        })
    }

    const back = () => {
        push('/MyLayout/LabelManagement')
    }
    // 实现input的响应式数据
    const onFinish = (value: any) => {
        // 发送新增轮播图的请求
        Label_add(value).then((res) => {
            console.log(res.data)
            if (res.data.code == 200) {
                message.open({
                    type: 'success',
                    content: '添加成功',
                });
                push('/MyLayout/LabelManagement')
            }
        })
    }
    return (
        <>
            <h1 className='line' style={{ paddingBottom: 20 }}> <span></span>新增轮播图</h1>
            <p>轮播图信息</p>
            <Form
                form={form}
                ref={formRef}
                name="control-ref"
                onFinish={onFinish}
                style={{ maxWidth: 600, paddingTop: 20 }}
            >
                <Form.Item name="id" label="编号" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="tagname" label="标签名称" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="name" label="添加人" required rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="time" label="修改时间" required rules={[{ required: true }]}>
                    <DatePicker showTime onChange={onChangetime} onOk={onOk} />
                </Form.Item>
                <Form.Item name="status" label="使用状态" required valuePropName="checked" rules={[{ required: true }]}>
                    <Switch checkedChildren="启用" unCheckedChildren="禁用" onChange={() => changeStatus()} />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        发布
                    </Button>
                    <Button htmlType="button" onClick={onReset} style={{ marginLeft: 20, marginRight: 20, backgroundColor: '#3dd4a7', color: '#fff' }}>
                        重置
                    </Button>
                    <Button onClick={() => back()}>
                        返回
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default PubText;