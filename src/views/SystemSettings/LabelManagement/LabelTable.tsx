import React, { useEffect, useState } from 'react';

import { Space, Table, Switch, message, Input, Modal, Popconfirm, AutoComplete, Button, DatePicker } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Label_list, Label_revisestatus, Label_edit, Label_del, Label_search } from '../../../axios/api';
import { useNavigate } from 'react-router-dom';

import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';

const { RangePicker } = DatePicker;

const onChangetime = (
    value: DatePickerProps['value'] | RangePickerProps['value'],
    dateString: [string, string] | string,
) => {
    console.log('Selected Time: ', value);
    console.log('Formatted Selected Time: ', dateString);
};

const onOk = (value: DatePickerProps['value'] | RangePickerProps['value']) => {
    console.log('onOk: ', value);
};

interface DataType {
    id: React.Key;
    tagname: string;
    name: string;
    time: string;
}

const LabelTable: React.FC = () => {
    const push = useNavigate()
    const [data, setData] = useState([])
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [messageApi, contextHolder] = message.useMessage();

    // const showModal = (record: any) => {
    //     setIsModalOpen(true);
    //     // 修改标签信息
    //     Label_edit(record.id).then((res) => {
    //         console.log(res.data)
    //     })
    // };

    // const handleOk = () => {
    //     setIsModalOpen(false);
    //     messageApi.open({
    //         type: 'success',
    //         content: '提交成功',
    //     });
    // };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    // 将获取写在外面方便后面调用
    const labellist = () => {
        // 原本获取的方式
        // axios.get("/api/LabelManagement/list").then((res) => {
        //     console.log(res.data.LabelManagement);
        //     setData(res.data.LabelManagement)
        // })

        // 封装请求后，二次封装后使用方式。
        // 获取标签管理列表
        Label_list().then((res) => {
            console.log(res.data)
            setData(res.data.labeldata)
        })
    }

    // useEffect在第一次渲染之后和每次更新之后都会执行
    useEffect(() => {
        labellist()
    }, [])

    // 修改标签使用状态
    const changeStatus = (val: any) => {
        Label_revisestatus({ id: val }).then((res) => {
            console.log(res.status)
            if (res.status === 200) {
                labellist()
                messageApi.open({
                    type: 'success',
                    content: '状态修改成功',
                });
            }
        })
    }

    // 获取搜索信息
    const onSearch = (value: string) => {
        console.log(value);

        if (value) {
            Label_search({ id: value }).then((res) => {

                setData(res.data.labeldata)
            })
        } else {
            labellist()
        }
    }

    // 定义点击删除的事件
    const del = (val: any) => {
        Label_del({ id: val }).then((res) => {
            if (res.data.code == 200) {
                labellist()
                messageApi.open({
                    type: 'success',
                    content: '标签删除成功',
                });
            }
        })
    }

    // 编辑修改信息
    const edit = (val: any) => {
        console.log(val)
        push({ pathname: '/MyLayout/UpdateLabel', search: `?data=${val}` })
    }

    const columns: ColumnsType<DataType> = [
        {
            title: '编号',
            dataIndex: 'id',
            key: 'id',
            fixed: 'left',
        },
        {
            title: '标签名称',
            dataIndex: 'tagname',
            key: 'tagname',
        },
        {
            title: '添加人',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '最后修改时间',
            dataIndex: 'time',
            key: 'time',
        },
        {
            title: '使用状态',
            dataIndex: 'status',
            key: 'status',
            render: (_, record: any) =>
                <Switch checkedChildren="启用" unCheckedChildren="禁用" defaultChecked checked={record.status} onChange={() => changeStatus(record.id)} />
        },
        {
            title: '操作',
            key: 'action',
            fixed: 'right',
            render: (_, record: any) => (
                <Space size="middle">
                    <a onClick={() => edit(record.id)}>编辑</a>
                    <Popconfirm title="你要删除吗?" onConfirm={() => del(record.id)}>
                        <a style={{ color: "red" }}> 删除</a>
                    </Popconfirm>
                </Space >
            ),
        },
    ];
    const AddLabel = () => {
        push("/MyLayout/AddLabel")
    }

    return (
        <>
            {contextHolder}
            <div className='LabelSearch' style={{ display: 'flex', justifyContent: 'space-between', margin: '20px 0' }}>
                <AutoComplete
                    dropdownMatchSelectWidth={252}
                    style={{ width: 300 }}
                >
                    <Input.Search allowClear size="large" onSearch={onSearch} placeholder="请输入关键字" />
                </AutoComplete>

                <Button type="primary" onClick={AddLabel} style={{ height: 40, backgroundColor: '#2984f8' }}>
                    新增标签
                </Button>
                {contextHolder}

                {/* <Modal title="编辑标签" open={isModalOpen} centered okText={'确认'} cancelText={'取消'} onOk={handleOk} onCancel={handleCancel} >

                    <div className="input" style={{ display: 'flex', justifyContent: 'space-around', lineHeight: 3 }}>
                        标签名称<Input placeholder="请输入" style={{ height: 40, marginLeft: 20, width: 350 }} />
                    </div>
                </Modal> */}
            </div>
            <Table columns={columns} dataSource={data} rowKey={record => record.id} />
        </>
    )
};

export default LabelTable;