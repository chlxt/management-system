import React from 'react';
import {
    CaretRightFilled
} from '@ant-design/icons';
import '../style/public.scss'
import LabelTable from './LabelTable';


function LabelManagement(props: any) {
    return (
        <div>
            <h1 className='line'> <span></span> 标签管理</h1>
            <LabelTable />
        </div>
    );
}

export default LabelManagement;