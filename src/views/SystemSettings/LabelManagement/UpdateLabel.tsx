import React, { useState, useEffect } from 'react';
import '../style/public.scss'
import { Button, Form, Input, Select, DatePicker, message, Switch } from 'antd';
import type { FormInstance } from 'antd/es/form';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { Label_edit, Label_editlist, Label_del, Label_list, Label_revisestatus } from '../../../axios/api'
import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';

// 时间
// const { RangePicker } = DatePicker;
// const onChangetime = (
//     value: DatePickerProps['value'] | RangePickerProps['value'],
//     dateString: [string, string] | string,
// ) => {
//     console.log('Selected Time: ', value);
//     console.log('Formatted Selected Time: ', dateString);
// };

// const onOk = (value: DatePickerProps['value'] | RangePickerProps['value']) => {
//     console.log('onOk: ', value);
// };

// 下拉框
const { Option } = Select;
interface Value {
    id: string;
    tagname: string;
    name: string;
    time: string;
    status: boolean;
}
const PubText: React.FC = () => {
    const [form] = Form.useForm()
    const push = useNavigate()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    console.log(val);
    // 定义空的状态，用来获取点击的哪一个文章的信息
    var [editlist, SetEditList] = useState<any>()

    const formRef = React.useRef<FormInstance>(null);

    const [data, setData] = useState([])

    const labellist = () => {
        // 获取标签管理列表
        Label_list().then((res) => {
            console.log(res.data.labeldata)
            setData(res.data.labeldata)
        })
    }

    // 将editlist通过组件传值，传递给表单
    useEffect(() => {
        Label_editlist({ id: val }).then((res: any) => {
            if (res.data.code == 200) {
                console.log(res.data.editlist);

                form.setFieldsValue(res.data.editlist)
                // SetEditList()
            }
        })

        // 当传递过来的key值发生变化时，发送请求，渲染表单
    }, [val])

    const onDel = () => {
        Label_del({ id: val }).then((res) => {
            if (res.data.code == 200) {
                labellist()
                message.open({
                    type: 'success',
                    content: '删除成功',
                });
                push('/MyLayout/LabelManagement')
            }
        })
    };

    const back = () => {
        push('/MyLayout/LabelManagement')
    }
    // 修改标签使用状态
    const changeStatus = () => {
        console.log(val);

        Label_revisestatus({ id: val }).then((res) => {
            // console.log(res.status)
            if (res.status === 200) {
                labellist()
                // message.open({
                //     type: 'success',
                //     content: '状态修改成功',
                // });
            }
        })
    }
    // 实现input的响应式数据
    const onFinish = (value: any) => {
        console.log(value)
        // 发送新增标签的请求
        Label_edit(value).then((res) => {
            console.log(res.data)
            if (res.data.code == 200) {
                message.open({
                    type: 'success',
                    content: '修改成功',
                });
                push('/MyLayout/LabelManagement')
            } else {
                message.open({
                    type: 'warning',
                    content: '未做任何修改',
                });
            }
        })
    }
    return (
        <>
            <h1 className='line' style={{ paddingBottom: 20 }}> <span></span>编辑轮播图</h1>
            <p>轮播图信息</p>
            <Form
                form={form}
                ref={formRef}
                name="control-ref"
                onFinish={onFinish}
                style={{ maxWidth: 600, paddingTop: 20 }}
            >
                <Form.Item name="id" label="编号" required rules={[{ required: true }]} >
                    <Input disabled />
                </Form.Item>
                <Form.Item name="tagname" label="标签名称" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="name" label="添加人" required rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="time" label="修改时间" required rules={[{ required: true }]}>
                    {/* <DatePicker showTime onChange={onChangetime} onOk={onOk} /> */}
                    <Input />
                </Form.Item>
                <Form.Item name="status" label="使用状态" required valuePropName="checked" rules={[{ required: true }]}>
                    <Switch checkedChildren="启用" unCheckedChildren="禁用" onChange={() => changeStatus()} />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        保存
                    </Button>
                    <Button htmlType="button" onClick={onDel} style={{ backgroundColor: '#f00', color: '#fff', marginLeft: 20, marginRight: 20 }}>
                        删除
                    </Button>
                    <Button onClick={() => back()}>
                        返回
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default PubText;