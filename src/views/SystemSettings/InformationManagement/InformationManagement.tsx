import React from 'react';
import '../style/public.scss'
import InformationTable from './InformationTable'

function InformationManagement(props: any) {
    return (
        <div>
            <h1 className='line'> <span></span> 咨讯管理</h1>
            <InformationTable />
        </div>
    );
}

export default InformationManagement;