import React, { useState, useEffect } from 'react';
import '../style/public.scss'
import { Button, Form, Input, Select, DatePicker, Upload, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import type { FormInstance } from 'antd/es/form';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { Infor_add, Infor_edit, Infor_editlist, Infor_del, Infor_list } from '../../../axios/api'
import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';

// 时间
const { RangePicker } = DatePicker;

// 下拉框
const { Option } = Select;
interface Value {
    id: React.Key;
    images: string;
    title: string;
    tclass: string;
    name: string;
    number: number;
    status: boolean;
    time: string;
    con: string
}
const PubText: React.FC = () => {
    const [form] = Form.useForm()
    const push = useNavigate()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    console.log(val);
    // 定义空的状态，用来获取点击的哪一个文章的信息
    var [editlist, SetEditList] = useState<any>()

    const formRef = React.useRef<FormInstance>(null);

    const { TextArea } = Input;

    const onChangecon = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        console.log(e);
    };

    const [data, setData] = useState([])

    const inforlist = () => {
        // 获取咨询管理列表
        Infor_list().then((res) => {
            console.log(res.data.infordata)
            setData(res.data.infordata)
        })
    }

    // 将editlist通过组件传值，传递给表单
    useEffect(() => {
        Infor_editlist({ id: val }).then((res) => {
            if (res.data.code == 200) {
                console.log(res.data.editlist);

                form.setFieldsValue(res.data.editlist)
                // SetEditList()
            }
        })

        // 当传递过来的key值发生变化时，发送请求，渲染表单
    }, [val])

    const onDel = () => {
        Infor_del({ id: val }).then((res) => {
            if (res.data.code == 200) {
                inforlist()
                message.open({
                    type: 'success',
                    content: '文章删除成功',
                });
                push('/MyLayout/InformationManagement')
            }
        })
    };

    const back = () => {
        push('/MyLayout/InformationManagement')
    }
    // 实现input的响应式数据
    const onFinish = (value: any) => {
        console.log(value)
        // 发送新增机构的请求
        Infor_edit(value).then((res) => {
            console.log(res.data)
            if (res.data.code == 200) {
                message.open({
                    type: 'success',
                    content: '修改成功',
                });
                push('/MyLayout/InformationManagement')
            } else {
                message.open({
                    type: 'warning',
                    content: '未做任何修改',
                });
            }
        })
        console.log(editlist)
    }
    return (
        <>
            <h1 className='line' style={{ paddingBottom: 20 }}> <span></span>编辑文章</h1>
            <p>文章信息</p>
            <Form
                form={form}
                ref={formRef}
                name="control-ref"
                onFinish={onFinish}
                style={{ maxWidth: 600, paddingTop: 20 }}
            >
                <Form.Item name="id" label="序号" required rules={[{ required: true }]}>
                    <Input disabled />
                </Form.Item>
                <Form.Item name="title" label="文章标题" required rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="images" label="列表图片" required rules={[{ required: true }]}>
                    {/* <Upload action="/upload.do" listType="picture-card" >
                        <div>
                            <PlusOutlined />
                            <div style={{ marginTop: 8 }}>点击上传</div>
                        </div>
                    </Upload> */}
                    <Input />
                </Form.Item>
                <Form.Item name="tclass" label="文章分类" required rules={[{ required: true }]}>
                    <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="健康头条">健康头条</Option>
                        <Option value="慢病护理">慢病护理</Option>
                    </Select>
                </Form.Item>
                <Form.Item name="name" label="发布人" required rules={[{ required: true }]}>
                    {/* <Select
                        placeholder="请选择"
                        allowClear
                        disabled
                    >
                        <Option value="李明明">李明明</Option>
                        <Option value="王汉文">王汉文</Option>
                        <Option value="李民进">李民进</Option>
                    </Select> */}
                    <Input />
                </Form.Item>
                <Form.Item name="status" label="发布状态" required rules={[{ required: true }]}>
                    <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="已发布">已发布</Option>
                        <Option value="未发布">未发布</Option>
                    </Select>
                </Form.Item>
                <Form.Item name="number" label="收藏量" required rules={[{ required: true }]}>
                    <Input disabled />
                </Form.Item>
                <Form.Item name="time" label="发布时间" rules={[{ required: true }]}>
                    <Input />
                    {/* <DatePicker renderExtraFooter={() => ''} showTime /> */}
                </Form.Item>
                <Form.Item name="con" label="文章内容" required rules={[{ required: true }]}>
                    <TextArea placeholder="请输入文章内容" allowClear onChange={onChangecon} style={{ height: 300 }} />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        保存
                    </Button>
                    <Button htmlType="button" onClick={onDel} style={{ backgroundColor: '#f00', color: '#fff', marginLeft: 20, marginRight: 20 }}>
                        删除
                    </Button>
                    <Button onClick={() => back()}>
                        返回
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default PubText;