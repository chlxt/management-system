import React, { useState } from 'react';
import '../style/public.scss'
import { Button, Form, Input, Select, DatePicker, Upload, message } from 'antd';
import type { FormInstance } from 'antd/es/form';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { Infor_add } from '../../../axios/api'
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import moment from 'moment';

dayjs.extend(customParseFormat);

const dateFormat = 'YYYY/MM/DD HH:mm:ss';

const { Option } = Select;
interface Props {
    id: string;
    images: string;
    title: string;
    tclass: string;
    name: string;
    number: number;
    status: boolean;
    time: string
}
const PubText: React.FC = () => {
    const [form] = Form.useForm()

    function onOk(value: any) {
        console.log('onOk: ', value);
        value = moment(value).format('YYYY/MM/DD HH:mm:ss')
        // setTime(value)
        console.log(value);

    }
    const push = useNavigate()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')

    // 定义响应式数据
    const [id, setId] = useState<string | undefined>()
    const [title, setTitle] = useState<string | undefined>()
    const [images, setImages] = useState<string | undefined>()
    const [tclass, setTclass] = useState<string | undefined>()
    const [name, setName] = useState<number | undefined>()
    const [status, setStatus] = useState<string | undefined>()
    const [number, setNumber] = useState<string | undefined>()
    const [time, setTime] = useState<string | undefined>()
    const [con, setCon] = useState<string | undefined>()
    // 实现input的响应式数据
    const changedata = (e: any) => {
        console.log(e.target.name)
        if (e.target.name == 'id') {
            setId(e.target.value);
        } else if (e.target.name == 'title') {
            setTitle(e.target.value);
        } else if (e.target.name == 'images') {
            setImages(e.target.value);
        } else if (e.target.name == 'tclass') {
            setTclass(e.target.value);
        } else if (e.target.name == 'name') {
            setName(e.target.value);
        } else if (e.target.name == 'status') {
            setStatus(e.target.value);
        } else if (e.target.name == 'number') {
            setNumber(e.target.value);
        } else if (e.target.name == 'time') {
            setTime(e.target.value);
        } else {
            setCon(e.target.value);
        }
    }
    // 定义一个状态当做发请求的参数
    var newdata = {
        id, title, images, name, tclass, status, number, time, con
    }

    const formRef = React.useRef<FormInstance>(null);

    const { TextArea } = Input;

    const onChangecon = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        console.log(e);
    };

    const onReset = () => {
        formRef.current?.resetFields();
    };

    const back = () => {
        push('/MyLayout/InformationManagement')
    }
    // 实现input的响应式数据
    const onFinish = (value: any) => {
        // 发送新增文章的请求
        console.log(value);

        Infor_add(value).then((res) => {

            console.log(res.data)
            if (res.data.code == 200) {
                message.open({
                    type: 'success',
                    content: '发布成功',
                });
                push('/MyLayout/InformationManagement')
            }
        })
        // console.log(editlist)
    }
    return (
        <>
            <h1 className='line' style={{ paddingBottom: 20 }}> <span></span>发布文章</h1>
            <p>文章信息</p>
            <Form
                form={form}
                onFinish={onFinish}
                ref={formRef}
                name="control-ref"
                style={{ maxWidth: 600, paddingTop: 20 }}
            >
                <Form.Item name="id" label="序号" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="title" label="文章标题" required rules={[{
                    required: true,
                }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="images" label="列表图片" required rules={[{ required: true }]}>
                    {/* <Upload action="/upload.do" listType="picture-card" >
                        <div>
                            <PlusOutlined name="images" value={images} onChange={(e) => changedata(e)} />
                            <div style={{ marginTop: 8 }}>点击上传</div>
                        </div>
                    </Upload> */}
                    <Input />
                </Form.Item>
                <Form.Item name="tclass" label="文章分类" required rules={[{ required: true }]}>
                    <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="健康头条">健康头条</Option>
                        <Option value="慢病护理">慢病护理</Option>
                    </Select>
                </Form.Item>
                <Form.Item name="name" label="发布人" required rules={[{ required: true }]}>
                    {/* <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="李明明">李明明</Option>
                        <Option value="王汉文">王汉文</Option>
                        <Option value="李民进">李民进</Option>
                    </Select> */}
                    <Input />
                </Form.Item>
                <Form.Item name="status" label="发布状态" required rules={[{ required: true }]}>
                    <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="已发布">已发布</Option>
                        <Option value="未发布">未发布</Option>
                    </Select>
                </Form.Item>
                <Form.Item name="number" label="收藏量" required rules={[{ required: true }]}>
                    <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="0">0</Option>
                    </Select>
                </Form.Item>
                <Form.Item name="time" label="发布时间" required rules={[{ required: true }]}>
                    <DatePicker showTime format={dateFormat} onOk={onOk} />
                </Form.Item>
                <Form.Item name="con" label="文章内容" required rules={[{ required: true }]}>
                    <TextArea placeholder="请输入文章内容" allowClear onChange={onChangecon} style={{ height: 300 }} />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" >
                        发布
                    </Button>
                    <Button htmlType="button" onClick={onReset} style={{ marginLeft: 20, marginRight: 20, backgroundColor: '#3dd4a7', color: '#fff' }}>
                        重置
                    </Button>
                    <Button onClick={() => back()}>
                        返回
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default PubText;