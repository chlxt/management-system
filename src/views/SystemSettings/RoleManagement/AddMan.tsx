import React, { useState } from 'react';
import { menuData } from '../../../grants/menuData';
import '../style/public.scss'
import { Button, Drawer, Form, Input, Select, Tree, DatePicker, Upload, message, Switch } from 'antd';
import type { FormInstance } from 'antd/es/form';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { User_add, User_list } from '../../../axios/Login'

const { Option } = Select;
interface Props {
    id: React.Key;
    phone: number;
    pass: string;
    name: string;
    roles: [];
}

const PubText: React.FC = () => {
    const [form] = Form.useForm()
    const push = useNavigate()
    const [data, setData] = useState()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    const formRef = React.useRef<FormInstance>(null);

    const onReset = () => {
        formRef.current?.resetFields();
    };

    const back = () => {
        push('/MyLayout/RoleManagement')
    }

    // 权限数据
    // const [selectedKeys, setSelectedKeys] = useState<React.Key[]>([]);
    const [checkedKeys, setCheckedKeys] = useState<React.Key[]>([]);
    const onCheck = (checkedKeysValue: any, e: any) => {
        console.log('onCheck', checkedKeysValue);
        console.log('onCheck.e', e);
        let arr = [...checkedKeysValue, ...e.halfCheckedKeys];
        console.log("arr", arr);
        setCheckedKeys(arr);
        form.setFieldValue('roles', arr)
    };

    // 实现input的响应式数据
    const onFinish = (value: any) => {
        // 发送新增轮播图的请求
        console.log(checkedKeys);

        console.log(value);

        User_add(value).then((res) => {
            console.log(res.data)
            if (res.data.code == 200) {
                message.open({
                    type: 'success',
                    content: '添加成功',
                });
                push('/MyLayout/RoleManagement')
            } else if (res.data.code == 304) {
                message.open({
                    type: 'error',
                    content: res.data.msg,
                });
            }
        })
    }

    return (
        <>
            <h1 className='line' style={{ paddingBottom: 20 }}> <span></span>新增操作人员</h1>
            <p>操作人员信息</p>
            <Form
                form={form}
                ref={formRef}
                name="control-ref"
                onFinish={onFinish}
                style={{ maxWidth: 600, paddingTop: 20 }}
            >
                {/* <Form.Item name="id" label="序号" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item> */}
                <Form.Item name="phone" label="电话号码" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="pass" label="密码" required rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="name" label="操作人员姓名" required rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="roles" label="权限" required rules={[{ required: true }]} >
                    <Tree
                        checkable
                        onCheck={onCheck}
                        treeData={menuData}
                    />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        添加
                    </Button>
                    <Button htmlType="button" onClick={onReset} style={{ marginLeft: 20, marginRight: 20, backgroundColor: '#3dd4a7', color: '#fff' }}>
                        重置
                    </Button>
                    <Button onClick={() => back()}>
                        返回
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default PubText;