import React, { useState, useEffect } from 'react';
import '../style/public.scss'
import { menuData } from '../../../grants/menuData';

import { Button, Form, Input, Select, DatePicker, Tree, Upload, message, Switch } from 'antd';
import type { FormInstance } from 'antd/es/form';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { User_edit, User_editlist, User_del, User_list } from '../../../axios/Login'
import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';

// 时间
const { RangePicker } = DatePicker;

// 下拉框
const { Option } = Select;
interface Value {
    id: React.Key;
    phone: string;
    pass: string;
    name: string;
    roles: [];
}
const PubText: React.FC = () => {
    const [form] = Form.useForm()
    const push = useNavigate()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    console.log(val);
    // 定义空的状态，用来获取点击的哪一个文章的信息
    var [editlist, SetEditList] = useState<any>()

    const formRef = React.useRef<FormInstance>(null);

    const [data, setData] = useState([])

    const list = () => {
        // 获取咨询管理列表
        User_list().then((res) => {
            console.log(res.data.bandata)
            setData(res.data.bandata)
        })
    }
    const [defaultCheckedKeys, SetDefaultCheckedKeys] = useState([])
    // 将editlist通过组件传值，传递给表单
    useEffect(() => {
        User_editlist({ id: val }).then((res: any) => {
            if (res.data.code == 200) {
                console.log(res.data.editlist);
                console.log(res.data.editlist.roles);
                SetDefaultCheckedKeys(res.data.editlist.roles)
                form.setFieldsValue(res.data.editlist)
                // SetEditList()
            }
        })

        // 当传递过来的key值发生变化时，发送请求，渲染表单
    }, [val])

    const onDel = () => {
        User_del({ id: val }).then((res) => {
            if (res.data.code == 200) {
                list()
                message.open({
                    type: 'success',
                    content: '删除成功',
                });
                push('/MyLayout/RoleManagement')
            }
        })
    };

    const back = () => {
        push('/MyLayout/RoleManagement')
    }

    // 实现input的响应式数据
    const onFinish = (value: any) => {
        console.log(value)
        // 发送新增机构的请求
        User_edit(value).then((res) => {
            console.log(res.data)
            if (res.data.code == 200) {
                message.open({
                    type: 'success',
                    content: '修改成功',
                });
                push('/MyLayout/RoleManagement')
            } else {
                message.open({
                    type: 'warning',
                    content: '未做任何修改',
                });
            }
        })
    }

    const [checkedKeys, setCheckedKeys] = useState<React.Key[]>([]);
    const onCheck = (checkedKeysValue: any, e: any) => {
        console.log('onCheck', checkedKeysValue);
        form.setFieldValue('roles', checkedKeys)
        console.log('onCheck.e', e);
        let arr = [...checkedKeysValue, ...e.halfCheckedKeys];
        console.log("arr", arr);
        console.log('onCheck', checkedKeysValue);
        setCheckedKeys(arr);
    };
    return (
        <>
            <h1 className='line' style={{ paddingBottom: 20 }}> <span></span>编辑操作人员信息</h1>
            <p>操作人员信息</p>
            <Form
                form={form}
                ref={formRef}
                name="control-ref"
                onFinish={onFinish}
                style={{ maxWidth: 600, paddingTop: 20 }}
            >
                <Form.Item name="id" label="序号" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="phone" label="电话号码" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="pass" label="密码" required rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="name" label="操作人员姓名" required rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="roles" label="权限" required rules={[{ required: true }]} >
                    <Tree
                        checkable
                        checkedKeys={defaultCheckedKeys}
                        onCheck={onCheck}
                        treeData={menuData}
                    />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        保存
                    </Button>
                    <Button htmlType="button" onClick={onDel} style={{ backgroundColor: '#f00', color: '#fff', marginLeft: 20, marginRight: 20 }}>
                        删除
                    </Button>
                    <Button onClick={() => back()}>
                        返回
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default PubText;