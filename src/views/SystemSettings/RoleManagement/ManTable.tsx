import React, { useEffect, useState } from 'react';

import { Space, Table, Switch, message, Popconfirm, AutoComplete, Input, Button, DatePicker } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { User_list, User_del, User_search } from '../../../axios/Login';
import { useNavigate } from 'react-router-dom';

interface DataType {
    id: React.Key;
    phone: number;
    pass: string;
    name: string;
    // roles: [];
}


const ManTable: React.FC = () => {
    const push = useNavigate()
    const [data, setData] = useState([])

    // 将获取写在外面方便后面调用
    const manlist = () => {
        User_list().then((res) => {
            console.log(res.data.list);
            setData(res.data.list)
        })
        // var list = JSON.parse(localStorage.getItem('user_info')!)
    }

    // useEffect在第一次渲染之后和每次更新之后都会执行
    useEffect(() => {
        manlist()
    }, [])

    // 获取搜索信息
    const onSearch = (value: string) => {
        console.log(value);

        if (value) {
            User_search({ id: value }).then((res) => {
                // console.log(res.data)
                // var arrsearch: any = []
                // arrsearch.push()
                console.log(res.data.list);

                setData(res.data.list)
            })
        } else {
            manlist()
        }
    }

    // 定义点击删除的事件
    const del = (val: any) => {
        User_del({ id: val }).then((res) => {
            if (res.data.code == 200) {
                manlist()
                message.open({
                    type: 'success',
                    content: '删除成功',
                });
            }
        })
    }

    // 编辑修改信息
    const edit = (val: any) => {
        console.log(val)
        push({ pathname: '/MyLayout/UpdateMan', search: `?data=${val}` })
    }

    const columns: ColumnsType<DataType> = [
        {
            title: '序号',
            dataIndex: 'id',
            key: 'id',
            fixed: 'left',
        },
        {
            title: '操作人员姓名',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '密码',
            dataIndex: 'pass',
            key: 'pass',
        },
        {
            title: '手机号码',
            dataIndex: 'phone',
            key: 'phone',
        },
        // {
        //     title: '角色',
        //     dataIndex: 'roles',
        //     key: 'roles',
        // },
        {
            title: '操作',
            key: 'action',
            fixed: 'right',
            render: (_, record: any) => (
                <Space size="middle">
                    <a onClick={() => edit(record.id)}>编辑</a>
                    <Popconfirm title="你要删除吗?" onConfirm={() => del(record.id)}>
                        <a style={{ color: "red" }}> 删除</a>
                    </Popconfirm>
                </Space>
            ),
        },
    ];
    // 发布文章
    const AddMan = () => {
        push("/MyLayout/AddMan")
    }

    return (
        <>
            <div className='LabelSearch' style={{ display: 'flex', justifyContent: 'space-between', margin: '20px 0' }}>
                <div className="ban-left" style={{ display: 'flex', alignItems: 'center', color: '#999' }}>
                    <AutoComplete
                        dropdownMatchSelectWidth={252}
                        style={{ width: 300 }}
                    >
                        <Input.Search allowClear size="large" onSearch={onSearch} placeholder="请输入关键字" style={{ height: 40, paddingLeft: 10 }} />
                    </AutoComplete>
                </div>


                <Button type="primary" onClick={AddMan} style={{ height: 40, backgroundColor: '#2984f8', }}>
                    新增操作人员
                </Button>
            </div>

            <Table columns={columns} dataSource={data} rowKey={record => record.id} />
        </>
    )
};

export default ManTable;