import React, { useState } from 'react';
import '../style/public.scss'
import { Button, Form, Input, Select, DatePicker, Upload, message, Switch } from 'antd';
import type { FormInstance } from 'antd/es/form';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { Ban_add, Ban_list, Ban_revisestatus } from '../../../axios/api'
import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';

const { RangePicker } = DatePicker;

const onChangetime = (
    value: DatePickerProps['value'] | RangePickerProps['value'],
    dateString: [string, string] | string,
) => {
    console.log('Selected Time: ', value);
    console.log('Formatted Selected Time: ', dateString);
};

const onOk = (value: DatePickerProps['value'] | RangePickerProps['value']) => {
    console.log('onOk: ', value);
};

const { Option } = Select;
interface Props {
    id: string;
    images: string;
    banname: string;
    name: string;
    status: boolean;
    time: string
}
const PubText: React.FC = () => {
    const [form] = Form.useForm()
    const push = useNavigate()
    const [data, setData] = useState([])
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    const formRef = React.useRef<FormInstance>(null);

    const onReset = () => {
        formRef.current?.resetFields();
    };

    // 获取ban列表
    const banlist = () => {
        Ban_list().then((res) => {
            console.log(res.data)
            setData(res.data.bandata)
        })
    }

    // 修改标签使用状态
    const changeStatus = () => {
        console.log(val);

        Ban_revisestatus({ id: val }).then((res) => {
            // console.log(res.status)
            if (res.status === 200) {
                banlist()
                message.open({
                    type: 'success',
                    content: '状态修改成功',
                });
            }
        })
    }

    const back = () => {
        push('/MyLayout/BannerManagement')
    }
    // 实现input的响应式数据
    const onFinish = (value: any) => {
        // 发送新增轮播图的请求
        Ban_add(value).then((res) => {
            console.log(res.data)
            if (res.data.code == 200) {
                message.open({
                    type: 'success',
                    content: '添加成功',
                });
                push('/MyLayout/BannerManagement')
            }
        })
    }
    return (
        <>
            <h1 className='line' style={{ paddingBottom: 20 }}> <span></span>新增轮播图</h1>
            <p>轮播图信息</p>
            <Form
                form={form}
                ref={formRef}
                name="control-ref"
                onFinish={onFinish}
                style={{ maxWidth: 600, paddingTop: 20 }}
            >
                <Form.Item name="id" label="序号" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="banname" label="轮播图标题" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="images" label="轮播图图片链接" required rules={[{ required: true }]}>
                    {/* <Upload action="/upload.do" listType="picture-card" >
                        <div>
                            <PlusOutlined name="images" value={images} onChange={(e) => changedata(e)} />
                            <div style={{ marginTop: 8 }}>点击上传</div>
                        </div>
                    </Upload> */}
                    <Input />
                </Form.Item>
                <Form.Item name="name" label="发布人" required rules={[{ required: true }]}>
                    {/* <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="李明明">李明明</Option>
                        <Option value="王汉文">王汉文</Option>
                        <Option value="李民进">李民进</Option>
                    </Select> */}
                    <Input />
                </Form.Item>
                <Form.Item name="status" label="轮播图状态" required valuePropName="checked" rules={[{ required: true }]}>
                    {/* <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="启用">启用</Option>
                        <Option value="禁用">禁用</Option>
                    </Select> */}
                    <Switch checkedChildren="启用" unCheckedChildren="禁用" onChange={() => changeStatus()} />
                </Form.Item>
                <Form.Item name="time" label="发布时间" required rules={[{ required: true }]}>
                    <DatePicker showTime onChange={onChangetime} onOk={onOk} />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        发布
                    </Button>
                    <Button htmlType="button" onClick={onReset} style={{ marginLeft: 20, marginRight: 20, backgroundColor: '#3dd4a7', color: '#fff' }}>
                        重置
                    </Button>
                    <Button onClick={() => back()}>
                        返回
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default PubText;