import React, { useEffect, useState } from 'react';

import { Space, Table, Switch, message, Popconfirm, AutoComplete, Input, Button, DatePicker } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import dayjs from 'dayjs';
import type { Dayjs } from 'dayjs';

import { Ban_list, Ban_revisestatus, Ban_del, Ban_search } from '../../../axios/api';
import { useNavigate } from 'react-router-dom';

interface DataType {
    id: React.Key;
    images: string;
    banname: string;
    name: string;
    time: string;
    status: boolean;
}

// 选择时间
const { RangePicker } = DatePicker;

const onRangeChange = (dates: null | (Dayjs | null)[], dateStrings: string[]) => {
    if (dates) {
        console.log('From: ', dates[0], ', to: ', dates[1]);
        console.log('From: ', dateStrings[0], ', to: ', dateStrings[1]);
    } else {
        console.log('Clear');
    }
};

const rangePresets: {
    label: string;
    value: [Dayjs, Dayjs];
}[] = [
        { label: '最近一周', value: [dayjs().add(-7, 'd'), dayjs()] },
        { label: '最近半个月', value: [dayjs().add(-14, 'd'), dayjs()] },
        { label: '最近一个月', value: [dayjs().add(-30, 'd'), dayjs()] },
        { label: '最近半三个月', value: [dayjs().add(-90, 'd'), dayjs()] },
    ];


const BanTable: React.FC = () => {
    const push = useNavigate()
    const [data, setData] = useState([])

    // 将获取写在外面方便后面调用
    const banlist = () => {
        Ban_list().then((res) => {
            console.log(res.data)
            setData(res.data.bandata)
        })
    }

    // useEffect在第一次渲染之后和每次更新之后都会执行
    useEffect(() => {
        banlist()
    }, [])

    // 获取搜索信息
    const onSearch = (value: string) => {
        console.log(value);

        if (value) {
            Ban_search({ id: value }).then((res) => {

                setData(res.data.bandata)
            })
        } else {
            banlist()
        }
    }

    // 修改标签使用状态
    const changeStatus = (val: any) => {
        console.log(val);

        Ban_revisestatus({ id: val }).then((res) => {
            // console.log(res.status)
            if (res.status === 200) {
                banlist()
                message.open({
                    type: 'success',
                    content: '状态修改成功',
                });
            }
        })
    }
    // 定义点击删除的事件
    const del = (val: any) => {
        Ban_del({ id: val }).then((res) => {
            if (res.data.code == 200) {
                banlist()
                message.open({
                    type: 'success',
                    content: '删除成功',
                });
            }
        })
    }

    // 编辑修改信息
    const edit = (val: any) => {
        console.log(val)
        push({ pathname: '/MyLayout/UpdateBan', search: `?data=${val}` })
    }

    const columns: ColumnsType<DataType> = [
        {
            title: '序号',
            dataIndex: 'id',
            key: 'id',
            fixed: 'left',
        },
        {
            title: '图片',
            dataIndex: 'images',
            key: 'images',
            render: (record: any) => <img src={record} alt="" width="120px" height="90px" />
        },
        {
            title: 'Banner图名称',
            dataIndex: 'banname',
            key: 'banname',
        },
        {
            title: '发布人',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '发布时间',
            dataIndex: 'time',
            key: 'time',
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            render: (_, record: any) =>
                <Switch checkedChildren="启用" unCheckedChildren="禁用" defaultChecked checked={record.status} onChange={() => changeStatus(record.id)} />
        },
        {
            title: '操作',
            key: 'action',
            fixed: 'right',
            render: (_, record: any) => (
                <Space size="middle">
                    <a onClick={() => edit(record.id)}>编辑</a>
                    <Popconfirm title="你要删除吗?" onConfirm={() => del(record.id)}>
                        <a style={{ color: "red" }}> 删除</a>
                    </Popconfirm>
                </Space>
            ),
        },
    ];
    // 发布文章
    const AddBan = () => {
        push("/MyLayout/AddBan")
    }

    return (
        <>
            <div className='LabelSearch' style={{ display: 'flex', justifyContent: 'space-between', margin: '20px 0' }}>
                <div className="ban-left" style={{ display: 'flex', alignItems: 'center', color: '#999' }}>
                    {/* 发布日期
                    <RangePicker
                        style={{ height: 40, marginLeft: 10 }}
                        presets={rangePresets}
                        showTime
                        format="YYYY/MM/DD HH:mm:ss"
                        onChange={onRangeChange} /> */}

                    <AutoComplete
                        dropdownMatchSelectWidth={252}
                        style={{ width: 300 }}
                    >
                        <Input.Search allowClear size="large" onSearch={onSearch} placeholder="请输入关键字" style={{ height: 40, paddingLeft: 10 }} />
                    </AutoComplete>
                </div>


                <Button type="primary" onClick={AddBan} style={{ height: 40, backgroundColor: '#2984f8', }}>
                    新增轮播图
                </Button>
            </div>

            <Table columns={columns} dataSource={data} rowKey={record => record.id} />
        </>
    )
};

export default BanTable;