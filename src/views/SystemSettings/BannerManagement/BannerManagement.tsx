import React from 'react';
import '../style/public.scss'
import BannerTable from './BannerTable'

function BannerManagement(props: any) {
    return (
        <div>
            <h1 className='line'> <span></span> 轮播图管理</h1>
            <BannerTable />
        </div>
    );
}

export default BannerManagement;