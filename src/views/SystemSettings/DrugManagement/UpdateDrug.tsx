import React, { useState, useEffect } from 'react';
import '../style/public.scss'
import { Button, Form, Input, Select, DatePicker, Upload, message } from 'antd';
import type { FormInstance } from 'antd/es/form';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { Drug_add, Drug_edit, Drug_editlist, Drug_del, Drug_list } from '../../../axios/api'

// 时间
const { RangePicker } = DatePicker;

// 下拉框
const { Option } = Select;
interface Value {
    id: React.Key;
    images: string;
    title: string;
    tclass: string;
    name: string;
    number: number;
    status: boolean;
    time: string;
    con: string
}
const PubText: React.FC = () => {
    const [form] = Form.useForm()
    const push = useNavigate()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    console.log(val);
    // 定义空的状态，用来获取点击的哪一个文章的信息
    var [editlist, SetEditList] = useState<any>()

    const formRef = React.useRef<FormInstance>(null);

    const { TextArea } = Input;

    const onChangecon = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        console.log(e);
    };

    const [data, setData] = useState([])

    const druglist = () => {
        // 获取咨询管理列表
        Drug_list().then((res) => {
            console.log(res.data.drugdata)
            setData(res.data.drugdata)
        })
    }

    // 将editlist通过组件传值，传递给表单
    useEffect(() => {
        Drug_editlist({ id: val }).then((res) => {
            if (res.data.code == 200) {
                console.log(res.data.editlist);

                form.setFieldsValue(res.data.editlist)
                // SetEditList()
            }
        })

        // 当传递过来的key值发生变化时，发送请求，渲染表单
    }, [val])

    const onDel = () => {
        Drug_del({ id: val }).then((res) => {
            if (res.data.code == 200) {
                druglist()
                message.open({
                    type: 'success',
                    content: '删除成功',
                });
                push('/MyLayout/DrugManagement')
            }
        })
    };

    const back = () => {
        push('/MyLayout/DrugManagement')
    }
    // 实现input的响应式数据
    const onFinish = (value: any) => {
        console.log(value)
        // 发送新增机构的请求
        Drug_edit(value).then((res) => {
            console.log(res.data)

            if (res.data.code == 200) {
                message.open({
                    type: 'success',
                    content: '修改成功',
                });
                push('/MyLayout/DrugManagement')
            } else {
                message.open({
                    type: 'warning',
                    content: '未做任何修改',
                });
            }
        })
        console.log(editlist)
    }
    return (
        <>
            <h1 className='line' style={{ paddingBottom: 20 }}> <span></span>编辑药品</h1>
            <p>药品信息</p>
            <Form
                form={form}
                ref={formRef}
                name="control-ref"
                onFinish={onFinish}
                style={{ maxWidth: 600, paddingTop: 20 }}
            >
                <Form.Item name="id" label="序号" required rules={[{ required: true }]}>
                    <Input disabled />
                </Form.Item>
                <Form.Item name="title" label="药品名称" required rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="images" label="药品图片" required rules={[{ required: true }]}>
                    {/* <Upload action="/upload.do" listType="picture-card" >
                        <div>
                            <PlusOutlined />
                            <div style={{ marginTop: 8 }}>点击上传</div>
                        </div>
                    </Upload> */}
                    <Input />
                </Form.Item>
                <Form.Item name="tclass" label="药品分类" required rules={[{ required: true }]}>
                    <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="处方药">处方药</Option>
                        <Option value="非处方药">非处方药</Option>
                    </Select>
                </Form.Item>
                <Form.Item name="name" label="发布人" required rules={[{ required: true }]}>
                    {/* <Select
                        placeholder="请选择"
                        allowClear
                        disabled
                    >
                        <Option value="李明明">李明明</Option>
                        <Option value="王汉文">王汉文</Option>
                        <Option value="李民进">李民进</Option>
                    </Select> */}
                    <Input disabled />
                </Form.Item>
                <Form.Item name="time" label="发布时间" rules={[{ required: true }]}>
                    <Input />
                    {/* <DatePicker renderExtraFooter={() => ''} showTime /> */}
                </Form.Item>
                <Form.Item name="number" label="收藏量" required rules={[{ required: true }]}>
                    <Input disabled />
                </Form.Item>
                <Form.Item name="con" label="药品详情" required rules={[{ required: true }]}>
                    <TextArea placeholder="请输入药品内容" allowClear onChange={onChangecon} style={{ height: 300 }} />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        保存
                    </Button>
                    <Button htmlType="button" onClick={onDel} style={{ backgroundColor: '#f00', color: '#fff', marginLeft: 20, marginRight: 20 }}>
                        删除
                    </Button>
                    <Button onClick={() => back()}>
                        返回
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default PubText;