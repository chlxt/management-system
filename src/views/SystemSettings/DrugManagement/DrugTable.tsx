import React, { useEffect, useState } from 'react';

import { Space, Table, Switch, message, Popconfirm, AutoComplete, Input, Button, DatePicker } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import dayjs from 'dayjs';
import type { Dayjs } from 'dayjs';

import { Drug_list, Drug_del, Drug_search } from '../../../axios/api';
import { useNavigate } from 'react-router-dom';

interface DataType {
    id: React.Key;
    images: string;
    title: string;
    tclass: string;
    name: string;
    number: number;
    status: boolean;
}

// 选择时间
const { RangePicker } = DatePicker;

const onRangeChange = (dates: null | (Dayjs | null)[], dateStrings: string[]) => {
    if (dates) {
        console.log('From: ', dates[0], ', to: ', dates[1]);
        console.log('From: ', dateStrings[0], ', to: ', dateStrings[1]);
    } else {
        console.log('Clear');
    }
};

const rangePresets: {
    label: string;
    value: [Dayjs, Dayjs];
}[] = [
        { label: '最近一周', value: [dayjs().add(-7, 'd'), dayjs()] },
        { label: '最近半个月', value: [dayjs().add(-14, 'd'), dayjs()] },
        { label: '最近一个月', value: [dayjs().add(-30, 'd'), dayjs()] },
        { label: '最近半三个月', value: [dayjs().add(-90, 'd'), dayjs()] },
    ];

const DrugTable: React.FC = () => {
    const push = useNavigate()
    const [data, setData] = useState([])

    // 将获取写在外面方便后面调用
    const druglist = () => {
        // 获取咨询管理列表
        Drug_list().then((res) => {
            console.log(res.data);

            setData(res.data.drugdata)
        })
    }

    // useEffect在第一次渲染之后和每次更新之后都会执行
    useEffect(() => {
        druglist()
    }, [])

    // 定义点击删除的事件
    const del = (val: any) => {
        Drug_del({ id: val }).then((res) => {
            if (res.data.code == 200) {
                druglist()
                message.open({
                    type: 'success',
                    content: '删除成功',
                });
            }
        })
    }


    // 获取搜索信息
    const onSearch = (value: string) => {
        console.log(value);

        if (value) {
            Drug_search({ id: value }).then((res) => {

                setData(res.data.drugdata)
            })
        } else {
            druglist()
        }
    }

    // 编辑修改信息
    const edit = (val: any) => {
        console.log(val)
        push({ pathname: '/MyLayout/UpdateDrug', search: `?data=${val}` })
    }

    const columns: ColumnsType<DataType> = [
        {
            title: '序号',
            dataIndex: 'id',
            key: 'id',
            fixed: 'left',
        },
        {
            title: '列表图',
            dataIndex: 'images',
            key: 'images',
            render: (record: any) => <img src={record} alt="" width="80px" height="80px" />
        },
        {
            title: '药品标题',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: '药品分类',
            dataIndex: 'tclass',
            key: 'tclass',
        },
        {
            title: '发布人',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '发布状态',
            dataIndex: 'status',
            key: 'status',
            render: (_, record) => <>
                <span>
                    {
                        record.status ? <span style={{ color: '#3dd4a7' }}>已发布</span> : <span style={{ color: '#fa746b' }}>未发布</span>
                    }
                </span>
            </>
        },
        {
            title: '收藏量',
            dataIndex: 'number',
            key: 'number',
        },

        {
            title: '操作时间',
            dataIndex: 'time',
            key: 'time',
        },
        {
            title: '操作',
            key: 'action',
            fixed: 'right',
            render: (_, record: any) => (
                <Space size="middle">
                    <a onClick={() => edit(record.id)}>编辑</a>
                    <Popconfirm title="你要删除吗?" onConfirm={() => del(record.id)}>
                        <a style={{ color: "red" }}> 删除</a>
                    </Popconfirm>
                </Space>
            ),
        },
    ];

    // 发布文章
    const PubDrug = () => {
        push("/MyLayout/PubDrug")
    }


    return (
        <>
            <div className='LabelSearch' style={{ display: 'flex', justifyContent: 'space-between', margin: '20px 0' }}>
                <div className="drug-left" style={{ display: 'flex', alignItems: 'center', color: '#999' }}>
                    {/* 发布日期
                    <RangePicker
                        style={{ height: 40, marginLeft: 10 }}
                        presets={rangePresets}
                        showTime
                        format="YYYY/MM/DD HH:mm:ss"
                        onChange={onRangeChange} /> */}

                    <AutoComplete
                        dropdownMatchSelectWidth={252}
                        style={{ width: 300 }}
                    >
                        <Input.Search allowClear size="large" onSearch={onSearch} placeholder="请输入关键字" style={{ height: 40, paddingLeft: 10 }} />
                    </AutoComplete>
                </div>


                <Button type="primary" onClick={PubDrug} style={{ height: 40, backgroundColor: '#2984f8', }}>
                    发布药品信息
                </Button>
            </div>
            <Table columns={columns} dataSource={data} rowKey={record => record.id} />
        </>
    )
};

export default DrugTable;