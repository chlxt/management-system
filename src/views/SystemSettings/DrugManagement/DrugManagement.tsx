import React from 'react';
import '../style/public.scss'
import DrugTable from './DrugTable'

function DrugManagement(props: any) {
    return (
        <div>
            <h1 className='line'> <span></span> 药品管理</h1>
            <DrugTable />
        </div>
    );
}

export default DrugManagement;