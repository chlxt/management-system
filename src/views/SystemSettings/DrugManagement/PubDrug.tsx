import React, { useState } from 'react';
import '../style/public.scss'
import { Button, Form, Input, Select, DatePicker, Upload, message } from 'antd';
import type { FormInstance } from 'antd/es/form';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { Drug_add } from '../../../axios/api'
import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';

// const { RangePicker } = DatePicker;

// const onChangetime = (
//     value: DatePickerProps['value'] | RangePickerProps['value'],
//     dateString: [string, string] | string,
// ) => {
//     console.log('Selected Time: ', value);
//     console.log('Formatted Selected Time: ', dateString);
// };

// const onOk = (value: DatePickerProps['value'] | RangePickerProps['value']) => {
//     console.log('onOk: ', value);
// };

const { Option } = Select;
interface Props {
    id: string;
    images: string;
    title: string;
    tclass: string;
    name: string;
    number: number;
    status: boolean;
    time: string
}
const PubText: React.FC = () => {
    const [form] = Form.useForm()
    const push = useNavigate()

    const formRef = React.useRef<FormInstance>(null);

    const onReset = () => {
        formRef.current?.resetFields();
    };

    const { TextArea } = Input;

    const onChangecon = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        console.log(e);
    };

    const back = () => {
        push('/MyLayout/DrugManagement')
    }
    // 实现input的响应式数据
    const onFinish = (value: any) => {

        Drug_add(value).then((res) => {
            console.log(res.data)
            if (res.data.code == 200) {
                message.open({
                    type: 'success',
                    content: '发布成功',
                });
                push('/MyLayout/DrugManagement')
            }
        })
    }
    return (
        <>
            <h1 className='line' style={{ paddingBottom: 20 }}> <span></span>新增药品信息</h1>
            <p>基础信息</p>
            <Form
                form={form}
                onFinish={onFinish}
                ref={formRef}
                name="control-ref"
                style={{ maxWidth: 600, paddingTop: 20 }}
            >
                <Form.Item name="id" label="序号" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="title" label="药品名称" required rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="images" label="药品图片" required rules={[{ required: true }]} >
                    <Input />
                </Form.Item>
                <Form.Item name="tclass" label="药品分类" required rules={[{ required: true }]}>
                    <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="处方药">处方药</Option>
                        <Option value="非处方药">非处方药</Option>
                    </Select>
                </Form.Item>
                <Form.Item name="name" label="发布人" required rules={[{ required: true }]}>
                    {/* <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="李明明">李明明</Option>
                        <Option value="王汉文">王汉文</Option>
                        <Option value="李民进">李民进</Option>
                    </Select> */}
                    <Input />
                </Form.Item>
                <Form.Item name="time" label="发布时间" required rules={[{ required: true }]}>
                    <DatePicker showTime format="YYYY/MM/DD HH:mm:ss" />
                </Form.Item>
                <Form.Item name="number" label="收藏量" required rules={[{ required: true }]}>
                    <Select
                        placeholder="请选择"
                        allowClear
                    >
                        <Option value="0">0</Option>
                    </Select>
                </Form.Item>
                <Form.Item name="con" label="药品详情" required rules={[{ required: true }]}>
                    <TextArea placeholder="请输入药品内容" allowClear onChange={onChangecon} style={{ height: 300 }} />
                </Form.Item>


                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        发布
                    </Button>
                    <Button htmlType="button" onClick={onReset} style={{ marginLeft: 20, marginRight: 20, backgroundColor: '#3dd4a7', color: '#fff' }}>
                        重置
                    </Button>
                    <Button onClick={() => back()}>
                        返回
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default PubText;