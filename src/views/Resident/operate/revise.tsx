import React, { useState, useEffect, memo } from 'react';
import { Button, Form, Input, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import { FormInstance } from 'antd/es/form/Form';
import { getBable_list, editBable_list } from '../../../axios/apiM';
import '../style.scss'
// import { Login } from '../../axios/Login'

interface Props {

}
interface Value {
    key: React.Key,
    name: string,
    phone: number,
    car: string,
    state: string,
    states: string
}
const { TextArea } = Input;
function Edit(props: Props) {
    const [form] = Form.useForm()

    const formRef = React.useRef<FormInstance>(null);

    const { TextArea } = Input;

    // 定义路由跳转
    const push = useNavigate()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    console.log(val);
    // 定义空的状态，用来获取点击的哪一个机构的信息

    // 将editlist通过组件传值，传递给表单
    useEffect(() => {
        getBable_list({ key: val }).then((res) => {
            if (res.data.code == 200) {
                console.log(res.data);

                form.setFieldsValue(res.data.babledata)
            }


        })

    }, [val])


    const back = () => {
        push('/MyLayout/ResidentManagement')
    }




    const onFinish = (value: any) => {
        console.log(value);

        editBable_list(value).then((res) => {
            if (res.data.code == 200) {
                console.log(res.data.list);
                push('/MyLayout/ResidentManagement')
            } else {
                message.open({
                    type: 'error',
                    content: '用户信息没有发生修改，请修改后提交',
                });
            }
        })

    }
    return (

        <div>
            <h1 className='h1'> <span></span>编辑居民信息</h1>
            <div style={{
                marginTop:20
            }}>
                <p>居民信息</p>
                <>
                    <Form
                        form={form}
                        ref={formRef}
                        onFinish={onFinish}

                        autoComplete="off"
                        layout="horizontal"
                        style={{ maxWidth: 800, display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}
                    >
                        <Form.Item label="居民编号" name='key' style={{ width: "46%" }} help  >
                            <Input disabled />
                        </Form.Item>
                        <Form.Item label="姓名" name="name" style={{ width: "46%" }} required help rules={[
                            {
                                required: true,
                                message: '不能为空!',
                            },
                        ]}>
                            <Input />
                        </Form.Item>
                        <Form.Item label="身份证号" name="car" style={{ width: "46%" }} help rules={[
                            {
                                required: true,
                                message: '不能为空!',
                            },
                        ]} >
                            <Input />
                        </Form.Item>
                        <Form.Item label="联系电话" name="phone" style={{ width: "46%" }} help rules={[
                            {
                                required: true,
                                message: '不能为空!',
                            },
                        ]}>
                            <Input />
                        </Form.Item>
                        <Form.Item label="签约状态" name="state" style={{ width: "100%" }} help required rules={[
                            {
                                required: true,
                                message: '不能为空!',
                            },
                        ]}>
                            <Input />
                        </Form.Item>
                        <Form.Item label="用户标签" name="states" style={{ width: "100%" }} help required rules={[
                            {
                                required: true,
                                message: '不能为空!',
                            },
                        ]}>
                            <Input />
                        </Form.Item>
                        <Form.Item wrapperCol={{ xs: { span: 24, offset: 0 }, sm: { span: 16, offset: 8 } }}>
                            <Button type="primary" htmlType="submit" >
                                提交
                            </Button>
                        </Form.Item>
                        <Form.Item wrapperCol={{ xs: { span: 24, offset: 0 }, sm: { span: 16, offset: 8 } }}>
                            <Button onClick={() => back()}>返回</Button>
                        </Form.Item>
                    </Form>
                </>
            </div>
        </div>
    );
}

export default memo(Edit);