import React, { useState, useEffect, memo } from 'react';
// import FromOrgan from './FromOrgan';
import { Button, Form, Input, Upload } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import { addBable_list } from "../../../axios/apiM";
import { FormInstance } from 'antd/es/form/Form';

interface Props {

}

interface Value {
  key: React.Key,
  name: string,
  phone: number,
  car: string,
  state: string,
  states: string
}
const { TextArea } = Input;
function Add(props: Props) {
  // 定义路由跳转
  const push = useNavigate()

 
  // 实现input的响应式数据
  const [form] = Form.useForm()
  // 定义一个状态当做发请求的参数
  const formRef = React.useRef<FormInstance>(null);
  const back = () => {
    push('/MyLayout/ResidentManagement')
  }
  // 实现input的响应式数据
  // const Save = () => {
  //   // 发送新增机构的请求
   
  // }
  const onFinish = (value:any) => {
    console.log(formRef);
    
    addBable_list(value).then((res) => {
      console.log(res.data)
      if (res.data.code == 200) {
        push('/MyLayout/ResidentManagement')
      }
    })
  }
  return (

    <div>
      <h1 className='Headline'><span></span>新增居民信息</h1>
      <div className="fromdata">
        <p>居民信息</p>
        <>
          <Form
            form={form}
            ref={formRef}
            onFinish={onFinish}
            layout="horizontal"
            style={{ maxWidth: 800, display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}
          >
            <Form.Item label="居民编号" name="key" style={{ width: "46%" }} required rules={[
              {
                required: true,
                message: '不能为空!',
              },
            ]}>
              <Input/>
            </Form.Item>
            <Form.Item label="姓名" name="name" style={{ width: "46%" }} required rules={[
              {
                required: true,
                message: '不能为空!',
              },
            ]}>
              <Input/ >
            </Form.Item>
            <Form.Item label="身份证号" name="car" style={{ width: "46%" }} required rules={[
              {
                required: true,
                message: '不能为空!',
              },
            ]} >
              <Input/>
            </Form.Item>
            <Form.Item label="联系电话" name="phone" style={{ width: "46%" }} required rules={[
              {
                required: true,
                message: '不能为空!',
              },
            ]} >
              <Input/>
            </Form.Item>
            <Form.Item label="签约状态" name="state" style={{ width: "100%" }} required rules={[
              {
                required: true,
                message: '不能为空!',
              },
            ]}>
              <Input/>
            </Form.Item>
            <Form.Item label="用户标签" name="states" style={{ width: "100%" }} required rules={[
              {
                required: true,
                message: '不能为空!',
              },
            ]}>
              <Input/>
            </Form.Item>
            <Form.Item wrapperCol={{ xs: { span: 24, offset: 0 }, sm: { span: 16, offset: 8 } }}>
              <Button type="primary" htmlType="submit">
                提交
              </Button>
            </Form.Item>
            <Form.Item wrapperCol={{ xs: { span: 24, offset: 0 }, sm: { span: 16, offset: 8 } }}>
              <Button onClick={() => back()}>返回</Button>
            </Form.Item>


          </Form>
        </>
      </div>
     
    </div>
  );
}

export default memo(Add);