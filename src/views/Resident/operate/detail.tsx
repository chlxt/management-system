import React from 'react';
import '../../Service/pendingService/css/ServiceConent.css'
import { Button } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useParams, useSearchParams } from 'react-router-dom';
import { useState, useEffect } from 'react'
import { getBable_list } from '../../../axios/apiM'

function Conent(props: any) {
    var [params, setParams] = useSearchParams()
    var val = params.get('data')

    console.log(params);
    
    const push = useNavigate()
    const [person, setperson] = useState<any>()
    // 传id从发请求找到对应的数据
    useEffect(() => {
        getBable_list({key:val}).then(res => {
            setperson(res.data.babledata)
            console.log(res.data.babledata);

        })
    }, [])
   
    const set = (val:any) => {

        console.log(React);

        push({ pathname: '/MyLayout/Revise', search: `?data=${val}` })
    }
console.log(person);

    return (
        <div>
            <h1>签约详情</h1>
            <div className='img-box '>
                {person?.state  }
            </div>
            <div>
                <h3>居民信息</h3>
                <div className='resident'>
                    <div> 姓名: <p className='blue-p'>{person?.name}</p></div>
                    <div> 身份证号: <p>{person?.car}</p></div>
                    <div> 性别: <p>{person?.sex}</p></div>
                    <div> 年龄: <p>{person?.age}</p></div>
                    <div> 联系电话: <p>{person?.phone}</p></div>
                    <div>现居地: <p>{person?.现居地}</p></div>
                    <div>病例: <p>{person?.states}</p></div>

                </div>
            </div>
            <div>
                <h3>服务信息</h3>
                <div className='resident'>
                    <div>  签约编号: <p >{person?.key}</p></div>
                    <div>  签约状态: <p style={{ color: 'blue' }}>{person?.state}</p></div>
                    <div> 签约机构: <p>{person?.jigou}</p></div>
                    <div>  签约团队: <p>{person?.team}</p></div>
                    <div>  签约医生: <p>{person?.doctor}</p></div>
                    <div> 服务包: <p>{person?.service}</p></div>
                    <div> 签约周期: <p >{person?.zhouqi}</p></div>
                    <div> 费用: <p>{person?.money}</p></div>
                    <div> 签约类型: <p>{person?.type}</p></div>
                    <div> 申请时间: <p>{person?.time}</p></div>
                    <div> 失效时间: <p>{person?.time}</p></div>
                    <div> 服务备注: <p>无</p></div>
                </div>
            </div>
            <div>
                
                <Button type="primary" onClick={()=>set(val)}>编辑服务信息</Button>
                <Button onClick={() => { push('/MyLayout/ResidentManagement') }}>返回</Button>
            </div>
        </div>
    );
}

export default Conent;