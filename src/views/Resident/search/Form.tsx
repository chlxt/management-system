import React, { useState, useEffect } from 'react';
import { AudioOutlined } from '@ant-design/icons';
import { Space, AutoComplete, Modal, Table, Tag, Switch, message, Popconfirm, Button, Input } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Bable_list, setBable_list, searchBable_list, delBable_list } from '../../../axios/apiM'
import { useNavigate } from 'react-router-dom';
interface DataType {
    key: React.Key;
    name: string;
    car: number;
    phone: number;
    state: string[];
    states: string[];
    status: boolean;
}

const App: React.FC = () => {
    const { Search } = Input;
    const push = useNavigate()
    const [data, setData] = useState([])
    const babledata = () => {
        Bable_list().then((res) => {
            setData(res.data.babledata)
        })
    }
    useEffect(() => {
        babledata()
    }, [])
    const changeStatus = (val: React.Key) => {
        setBable_list({ key: val }).then((res: any) => {
            setData(res.data.babledata)
        })
    }
    //删除的事件
    const del = (val: React.Key) => {
        delBable_list({ key: val }).then((res: any) => {
            setData(res.data.babledata)
        })

    }
    const set = (val: React.Key) => {

        console.log(React);

        push({ pathname: '/MyLayout/Revise', search: `?data=${val}` })
    }
    const add = () => {
        push('/MyLayout/Addlist')
    }
    const get = (val: React.Key) => {
        console.log(val);
        
        push({ pathname:'/MyLayout/detail',search:`?data=${val}`})
    }
    const onSearch = (value: string) => {
        console.log(value);

        if (value) {
            searchBable_list({ key: value }).then((res) => {

                setData(res.data.babledata)
            })
        } else {
            babledata()
        }
    }
    const columns: ColumnsType<DataType> = [
        {
            title: '编号',
            dataIndex: 'key',
            key: 'key',
            align: 'center'


        },
        {
            title: '姓名',
            dataIndex: 'name',
            key: 'name',
            align: 'center'

        },
        {
            title: '身份证号',
            dataIndex: 'car',
            align: 'center',
            key: 'car',



        },
        {
            title: '手机号码',
            dataIndex: 'phone',
            key: 'phone',
            align: 'center'

        },
        {
            title: '签约状态',
            dataIndex: 'state',
            key: 'state',

            align: 'center'
        },
        {
            title: '用户标签',
            dataIndex: 'states',
            key: 'states',

            align: 'center'
        },
        {
            title: '居民状态',
            key: 'status',

            dataIndex: 'status',
            render: (_, record: any) =>
                <Switch checkedChildren="启用" unCheckedChildren="禁用" defaultChecked checked={record.status} onChange={() => changeStatus(record.key)} />
        },
        {
            title: '操作',
            dataIndex: 'operation',
            key: 'operation',

            align: 'center',
            render: (_, record: any) => <>
                <a onClick={() => get(record.key)}>查看详情</a>&nbsp;&nbsp;&nbsp;
                <a onClick={() => set(record.key)}>编辑</a>&nbsp;&nbsp;&nbsp;
                <Popconfirm title="Sure to delete?" onConfirm={() => del(record.id)} >
                    <a style={{ color: "red" }}>删除</a>
                </Popconfirm>
            </>
        },
    ];
    return (

        <>
            <div className='LabelSearch' style={{ display: 'flex', justifyContent: 'space-between', margin: '20px 0' }}>
                <AutoComplete
                    dropdownMatchSelectWidth={252}
                    style={{ width: 300 }}

                >
                    <Input.Search size="large" placeholder="请输入关键字" allowClear enterButton onSearch={onSearch} />
                </AutoComplete>

                <Button type="primary" onClick={add} style={{ height: 40, backgroundColor: '#2984f8' }}>
                    新增标签
                </Button>



            </div>
            <Table columns={columns} dataSource={data} rowKey={record => record.key} />
        </>
    )
}

export default App;