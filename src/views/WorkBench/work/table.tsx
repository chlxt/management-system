import React, { useEffect, useState } from 'react';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Tabel_list } from '../../../axios/apiM'

interface DataType {
    id: React.Key;
    tagname: string;
    number1: number; number2: number; number3: number; number4: number; number5: number; number6: number; number7: number; number8: number;

}
const App: React.FC = () => {
    const [data, setData] = useState([])

    // 将获取写在外面方便后面调用
    const tabellist = () => {

        Tabel_list().then((res) => {
            console.log(res.data)
            setData(res.data.tabeldata)
        })
    }

    // useEffect在第一次渲染之后和每次更新之后都会执行
    useEffect(() => {
        tabellist()
    }, [])

    const columns: ColumnsType<DataType> = [
        {
            title: '团队',
            dataIndex: 'tagname',
            key: 'id',

        },
        {
            title: '2021-01-04',
            dataIndex: 'number1',
            key: 'number1',
        },
        {
            title: '2021-01-05',
            dataIndex: 'number2',
            key: 'number2',
        },
        {
            title: '2021-01-06',
            dataIndex: 'number3',
            key: 'number3',
        }, {
            title: '2021-01-07',
            dataIndex: 'number4',
            key: 'number4',
        }, {
            title: '2021-01-08',
            dataIndex: 'number5',
            key: 'number5',
        }, {
            title: '2021-01-09',
            dataIndex: 'number6',
            key: 'number6',
        }, {
            title: '2021-01-10',
            dataIndex: 'number7',
            key: 'number7',
        },

    ];
    return (
        <>
            <div style={{
                
                marginTop: 30, boxShadow: '2px 2px 5px 2px rgb(205, 203, 203)' 
                
            }}>
                <h1>本周签约概况</h1>
                <Table style={{

                }} columns={columns} dataSource={data} rowKey={record => record.id} />

            </div>

        </>
    )
}

export default App;