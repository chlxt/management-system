// import React from 'react';
import { List, Typography, Button, Space } from 'antd';
import React, { Component, useEffect } from 'react'
import { DownloadOutlined } from '@ant-design/icons';   
import { useNavigate } from 'react-router-dom';









export default function list() {
    const push = useNavigate()
    const set = (item: any) => {
        console.log(item);

        if (item.text == '待审核签约申请') {
            push('/MyLayout/PendingContract')
        }
        else if (item.text == '待审核服务申请') {
            push('/MyLayout/PendingService')
        } else if (item.text == '待完成服务') {
            push('/MyLayout/ServiceRecord')
        } else if (item.text == '待处理续约') {
            push('/MyLayout/SigningRecord')
        }
    }
    const data = [
        {
            num: 10,
            text: '待审核签约申请'

        }, {
            num: 30,
            text: '待审核服务申请'

        }, {
            num: 7,
            text: '待完成服务'

        }, {
            num: 107,
            text: '待回复消息'

        }, {
            num: 23,
            text: '待处理续约'

        },

    ];
    return (
        <div>
            <>
                <List
                    style={{
                        backgroundColor: 'white',
                        boxShadow: '2px 2px 5px 2px rgb(205, 203, 203)'
                    }}
                    header={<div>代办提醒</div>}

                    bordered={false}
                    size='large'
                    dataSource={data}
                    renderItem={(item) => (
                        <List.Item >
                            <Space wrap>
                            <Typography.Text mark>{item.num}</Typography.Text>
                            <br />
                            <p>{item.text}</p><Button onClick={() => set(item)}>查看详情</Button>
                                </Space>
                        </List.Item>
                    )}
                />

            </>
        </div>
    )
}

