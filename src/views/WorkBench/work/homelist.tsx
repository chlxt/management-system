import React,{useState,useEffect} from 'react'
import { Divider, List, Typography } from 'antd';
import * as echarts from 'echarts';
import { readFileSync } from 'fs';
import { SigningRecordList, PendingServiceList, PendingContractlist, ServiceRecordList }from '../../../axios/apiY'
type EChartsOption = echarts.EChartsOption;

export default function Homepage() {
    const [arr, setarr] = useState<any>()
    const [login, setlogn] = useState<any>()
    const [jhg, setjhg] = useState<any>()
    const [sh, setsh] = useState<any>()
    useEffect(() => {
        SigningRecordList().then((res) => {
            console.log(res.data);
            setarr(res.data.SigningRecordl)
        })
        PendingContractlist().then((res) => {
            setlogn(res.data.PendingContractl)
        })
        ServiceRecordList().then((res) => {
            setjhg(res.data.ServiceRecordl)
        })
        PendingServiceList().then((res:any) => {
            setsh(res.data.PendingServicel)
        })
    }, [])
    console.log(arr?.length);
    let list: any = []
    arr?.forEach((item: any) => {
        if (item.zhuang == '生效中') {
            list.push(item.zhuang)
        }
    });
    let bgj: any = []
    login?.forEach((item: any) => {
        if (item.state == '待审核') {
            bgj.push(item.state)
        }
    });
    let serve: any = []
    jhg?.forEach((item: any) => {
        if (item.zhuang == '已完成') {
            serve.push(item.zhuang)
        }
    });
    let she: any = []
    sh?.forEach((item: any) => {
        if (item.state == '待审核') {
            she.push(item.state)
        }
    });
    let she1: any = []

    sh?.forEach((item: any) => {
        if (item.state == '待服务') {
            she1.push(item.state)
        }
    });
    let list1: any = []
    arr?.forEach((item: any) => {
        if (item.zhuang == '已过期') {
            list1.push(item.zhuang)
        }
    });
    setTimeout(() => {
        var chartDom = document.getElementById('main')!;
        var myChart = echarts.init(chartDom);
        var option: EChartsOption;

        option = {
            xAxis: {
                type: 'category',
                data: ['1/4', '1/5', '1/6', '1/7', '1/8', '1/9', '1/10']
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: [100, 220, 123, 142, 178, 156, 210],
                    type: 'line'
                }, {
                    data: [150, 120, 156, 241, 189, 178, 220],
                    type: 'line'
                }, {
                    data: [140, 160, 234, 221, 231, 147, 235],
                    type: 'line'
                }
            ]
        };

        option && myChart.setOption(option);

    }, 300);

    const data = [
        {
            num: bgj.length,
            text: '待审核签约申请'

        }, {
            num: she.length,
            text: '待审核服务申请'

        }, {
            num: she1.length,
            text: '待完成服务'

        }, {
            num: 107,
            text: '待回复消息'

        }, {
            num: list1.length,
            text: '待处理续约'

        },

    ];


    return (
        <div className='homelist' style={{ marginTop: 30, width: '100%', height: 450, display: 'flex', justifyContent: 'space-around' }} >
            <div id='main' style={{
                width: '70%', height: 450, backgroundColor: 'white',
                boxShadow: '2px 2px 5px 2px rgb(205, 203, 203)',
                marginRight: 10
            }}>

            </div>
            <List
                style={{
                    width: '20%',
                    backgroundColor: 'white',
                    boxShadow: '2px 2px 5px 2px rgb(205, 203, 203)'
                }}
                header={<div>代办提醒</div>}

                bordered={false}
                size='large'
                dataSource={data}
                renderItem={(item) => (
                    <List.Item>
                        <Typography.Text mark>{item.num}</Typography.Text>
                        <br />
                        {item.text}
                    </List.Item>
                )}
            />


        </div>
    )
}






