
import React, { useState, useEffect } from 'react';
import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons';
import { Card, Col, Row, Statistic, Divider } from 'antd';
import { PendingContractlist, SigningRecordList, ServiceRecordList } from '../../../axios/apiY'


export default function Carts() {
    const [arr, setarr] = useState<any>()
    const [login, setlogn] = useState<any>()
    const [jhg, setjhg] = useState<any>()
    useEffect(() => {
        SigningRecordList().then((res) => {
            console.log(res.data);
            setarr(res.data.SigningRecordl)
        })
        PendingContractlist().then((res) => {
            setlogn(res.data.PendingContractl)
        })
        ServiceRecordList().then((res) => {
            setjhg(res.data.ServiceRecordl)
        })
    },[])
console.log(arr?.length);
let list:any=[]
   arr?.forEach((item:any) => {
       if (item.zhuang == '生效中') {
        list.push(item.zhuang)
    }
   });
    let bgj: any = []
    login?.forEach((item:any) => {
        if (item.state == '待审核') {
            bgj.push(item.state)
        }
    });
    let serve: any = []
    jhg?.forEach((item: any) => {
        if (item.zhuang == '已完成') {
            serve.push(item.zhuang)
        }
    });
console.log(list);

    return (
        <>

            <Row justify="space-around">
                <Col span={5}>
                    <Card bordered={false}>

                        <Statistic

                            title="居民总数量"
                            value={arr?.length}
                            valueStyle={{ color: '#3f8600', fontWeight: 'bolder', fontSize: 30 }}

                        />
                    </Card>
                </Col>
                <Col span={5}>
                    <Card bordered={false}>
                        <Statistic
                            title="签约居民数量"
                            value={list.length}

                            valueStyle={{ color: '#6C76F4', fontWeight: 'bolder', fontSize: 30 }}


                        />
                    </Card>
                </Col><Col span={5}>
                    <Card bordered={false}>
                        <Statistic
                            title="待处理服务量"
                            value={bgj.length}
                            valueStyle={{ color: '#3DD4A7', fontWeight: 'bolder', fontSize: 30 }}


                        />
                    </Card>
                </Col><Col span={5}>
                    <Card bordered={false}>
                        <Statistic
                            title="已完成服务量"
                            value={serve.length}
                            valueStyle={{ color: '#FA746B', fontWeight: 'bolder', fontSize: 30 }}


                        />
                    </Card>
                </Col>
            </Row>
        </>

    )
}




