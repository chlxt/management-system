import React from 'react';
import { useSearchParams } from 'react-router-dom';
import HomeList from './work/homelist'
// import Home from "./work/homepage"
import Card from './work/carts'
// import List from './work/list'
// import Progress from './work/Progress'
import './style.css'
import Speed from './work/speed'
import Table from './work/table'

function WorkBench(props: any) {
    // var [params, setParams] = useSearchParams()
    // const name: any = params.get('data')
    const username = sessionStorage.getItem('name')

    return (
        <div className='body'>
            <div className='center'>
                <p className='user'>欢迎你,<span className='user-name'>{username}</span></p>
                <Card />
                <HomeList />
            </div>
            <div className='speed'>
                <Speed />
            </div>
            <Table />
        </div>

    );
}

export default WorkBench;