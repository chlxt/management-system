import React from 'react';
import { Button, DatePicker, Form, TimePicker } from 'antd';

const { RangePicker } = DatePicker;

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};

const config = {
    rules: [{ type: 'object' as const, required: true, message: 'Please select time!' }],
};

const rangeConfig = {
    rules: [{ type: 'array' as const, required: true, message: 'Please select time!' }],
};

const onFinish = (fieldsValue: any) => {
    // Should format date value before submit.
    const rangeValue = fieldsValue['range-picker'];
 
    const values = {
        ...fieldsValue,
        
        '选择日期': [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],
        
    };
    console.log('Received values of form: ', values);
};

const App: React.FC = () => (
    <Form
        name="time_related_controls"
        {...formItemLayout}
        onFinish={onFinish}
        style={{ maxWidth: 600 }}
    >
       
        <Form.Item name="range-picker" label="选择日期" {...rangeConfig}>
            <RangePicker />
        </Form.Item>
        
    </Form>
);

export default App;