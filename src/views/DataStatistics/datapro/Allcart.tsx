import { execFile } from 'child_process';
import * as echarts from 'echarts';
import { useEffect, useState } from 'react';
// import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons';
// import { Card, Col, Row, Statistic, Divider } from 'antd';

// import { PendingContractlist } from "../../../axios/apiY"
// import service from '../../../axios/service';

// import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
// import { Tabel_list } from '../../../axios/apiM'/

type EChartsOption = echarts.EChartsOption;
export default function Allcart() {
    const [list, setlist] = useState<any>()
    const [list1, setlist1] = useState<any>()
    const [list2, setlist2] = useState<any>()
    const [list3, setlist3] = useState<any>()
    const [list4, setlist4] = useState<any>()
    const [list5, setlist5] = useState<any>()
    const [book6, setbook6] = useState<any>()
    const [book1, setbook1] = useState<any>()
    const [book2, setbook2] = useState<any>()
    const [book3, setbook3] = useState<any>()
    const [book4, setbook4] = useState<any>()
    const [book5, setbook5] = useState<any>()
    const [book7, setbook7] = useState<any>()
    const [book8, setbook8] = useState<any>()
    const [age, setage4] = useState<any>()
    const [age5, setage5] = useState<any>()
    const [age7, setage7] = useState<any>()
    const [age8, setage8] = useState<any>()
    const [sex, setsex] = useState<any>()
    const [sex1, setsex1] = useState<any>()
    var str1 = JSON.parse(localStorage.getItem('PendingContract')!)
    var str = JSON.parse(localStorage.getItem('labeldata')!)
    useEffect(() => {
       
        var { PendingContractl } = str1
        console.log(PendingContractl);

        let booknum = 0;
        let booknum2 = 0;
        let booknum3 = 0;
        let booknum4 = 0;
        let booknum5 = 0;
        let booknum6 = 0;
        let booknum7 = 0;
        let booknum8 = 0;
        let agenum5 = 0;
        let agenum6 = 0;
        let agenum7 = 0;
        let agenum8 = 0;
        let sexnum7 = 0;
        let sexnum8 = 0;
        PendingContractl.forEach((item: any) => {
            console.log(item.service);
            if (item.service == '基础包1') {
                setbook1(booknum = booknum + 1)
            } else if (item.service == '基础包2') {
                setbook2(booknum2 = booknum2 + 1)

            } else if (item.service == '基础包3') {
                setbook3(booknum3 = booknum3 + 1)

            } else if (item.service == '基础包4') {
                setbook4(booknum4 = booknum4 + 1)

            } else if (item.service == '老年人服务包') {
                setbook5(booknum5 = booknum5 + 1)

            } else if (item.service == '慢性病护理包') {
                setbook6(booknum6 = booknum6 + 1)

            } else if (item.service == '儿童护理包') {
                setbook7(booknum7 = booknum7 + 1)

            }
            else if (item.service == '高级特需包') {
                setbook8(booknum8 = booknum8 + 1)

            }
          
        });
        PendingContractl.forEach((item: any) => {
              if (item.age >= 18 && item.age <= 30) {
                setage4(agenum5 = agenum5 + 1)
            } else if (item.age > 30 && item.age < 40) {
                setage5(agenum6 = agenum6 + 1)
            } else if (item.age >= 40 && item.age < 50) {
                setage7(agenum7 = agenum7 + 1)
            } else if (item.age >= 50) {
                setage8(agenum8 = agenum8 + 1)
            } 

        })

       
        var { labeldata } = str
        console.log(labeldata);
        let num = 0;
        let num2 = 0;
        let num3 = 0;
        let num4 = 0;
        let num5 = 0;
        let num6 = 0;
        console.log(labeldata);
        
        labeldata.forEach((item: any) => {
            if (item.tagname == "高血压") {
                setlist(num = num + 1)
            } else if (item.tagname == "冠心病") {
                setlist1(num2 = num2 + 1)
            } else if (item.tagname == "高血糖") {
                setlist2(num3 = num3 + 1)
            } else if (item.tagname == "高血脂") {
                setlist3(num4 = num4 + 1)
            } else if (item.tagname == "慢病护理") {
                setlist4(num5 = num5 + 1)
            } else {
                setlist5(num6 = num6 + 1)
            }

        })
        PendingContractl.forEach((item: any) => {
            if (item.sex == "男") {
                setsex(sexnum7 = sexnum7 + 1)
            } else if (item.sex == "女") {
                setsex1(sexnum8 = sexnum8 + 1)
            }

        })
    }, [str,str1])
    setTimeout(() => {
        var chartDom = document.getElementById('main')!;
        var myChart = echarts.init(chartDom);
        var option: EChartsOption;

        const builderJson = {
            all: list+list2+list3+list4+list5+list1,
            charts: {
                多动症: list5,
                慢病护理: list4,
                高血脂: list3,
                高血糖: list2,
                冠心病: list1,
                高血压: list,
            } as Record<string, number>,
            components: {
                基础包1: book1,
                老年人服务包: book5,
                慢性病护理包: book6,
                儿童护理包: book7,
                高级特需包: book8,
                基础包2: book2,
                基础包3: book3,
                基础包4: book4,

            } as Record<string, number>,
            ie: 150
        };

        const downloadJson: Record<string, number> = {
            '18~30岁': age,
            '30~40岁': age5,
            '40~50岁': age7,
            '50岁以上': age8
        };

        const themeJson: Record<string, number> = {
            '男': sex,
            '女': sex1,
            
        };

        const waterMarkText = 'ECHARTS';

        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d')!;
        canvas.width = canvas.height = 100;
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.globalAlpha = 0.08;
        ctx.font = '20px Microsoft Yahei';
        ctx.translate(50, 50);
        ctx.rotate(-Math.PI / 4);
        ctx.fillText(waterMarkText, 0, 0);
        option = {
            // backgroundColor: {
            //     type: 'pattern',
            //     image: canvas,
            //     repeat: 'repeat'
            // },
            tooltip: {},
            title: [
                {
                    text: '居民标签',
                    subtext: '总计 ' + builderJson.all,
                    left: '25%',
                    textAlign: 'center'
                },
                {
                    text: '年龄占比',
                    subtext:
                        '总计 ' +
                        Object.keys(downloadJson).reduce(function (all, key) {
                            return all + downloadJson[key];
                        }, 0),
                    left: '75%',
                    textAlign: 'center'
                },
                {
                    text: '性别占比',
                    subtext:
                        '总计 ' +
                        Object.keys(themeJson).reduce(function (all, key) {
                            return all + themeJson[key];
                        }, 0),
                    left: '75%',
                    top: '50%',
                    textAlign: 'center'
                }
            ],
            grid: [
                {
                    top: 50,
                    width: '50%',
                    bottom: '45%',
                    left: 10,
                    containLabel: true
                },
                {
                    top: '55%',
                    width: '50%',
                    bottom: 0,
                    left: 10,
                    containLabel: true
                }
            ],
            xAxis: [
                {
                    type: 'value',
                    max: builderJson.all,
                    splitLine: {
                        show: false
                    }
                },
                {
                    type: 'value',
                    max: builderJson.all,
                    gridIndex: 1,
                    splitLine: {
                        show: false
                    }
                }
            ],
            yAxis: [
                {
                    type: 'category',
                    data: Object.keys(builderJson.charts),
                    axisLabel: {
                        interval: 0,
                        rotate: 30
                    },
                    splitLine: {
                        show: false
                    }
                },
                {
                    gridIndex: 1,
                    type: 'category',
                    data: Object.keys(builderJson.components),
                    axisLabel: {
                        interval: 0,
                        rotate: 30
                    },
                    splitLine: {
                        show: false
                    }
                }
            ],
            series: [
                {
                    type: 'bar',
                    stack: 'chart',
                    z: 3,
                    label: {
                        position: 'right',
                        show: true
                    },
                    data: Object.keys(builderJson.charts).map(function (key) {
                        return builderJson.charts[key];
                    })
                },
                {
                    type: 'bar',
                    stack: 'chart',
                    silent: true,
                    itemStyle: {
                        color: '#eee'
                    },
                    data: Object.keys(builderJson.charts).map(function (key) {
                        return builderJson.all - builderJson.charts[key];
                    })
                },
                {
                    type: 'bar',
                    stack: 'component',
                    xAxisIndex: 1,
                    yAxisIndex: 1,
                    z: 3,
                    label: {
                        position: 'right',
                        show: true
                    },
                    data: Object.keys(builderJson.components).map(function (key) {
                        return builderJson.components[key];
                    })
                },
                {
                    type: 'bar',
                    stack: 'component',
                    silent: true,
                    xAxisIndex: 1,
                    yAxisIndex: 1,
                    itemStyle: {
                        color: '#eee'
                    },
                    data: Object.keys(builderJson.components).map(function (key) {
                        return builderJson.all - builderJson.components[key];
                    })
                },
                {
                    type: 'pie',
                    radius: [0, '30%'],
                    center: ['75%', '25%'],
                    data: Object.keys(downloadJson).map(function (key) {
                        return {
                            name: key.replace('.js', ''),
                            value: downloadJson[key]
                        };
                    })
                },
                {
                    type: 'pie',
                    radius: [0, '30%'],
                    center: ['75%', '75%'],
                    data: Object.keys(themeJson).map(function (key) {
                        return {
                            name: key.replace('.js', ''),
                            value: themeJson[key]
                        };
                    })
                }
            ]
        }

        option && myChart.setOption(option);

    }, 300)

    return (
        <div><div id='main' style={{
            width: 1300, height: 600
        }}></div>
            
            <App/>
        </div>

    )
}
interface DataType {
    id: React.Key;
    tagname: string;
    number1: number; number2: number; number3: number; number4: number; number5: number; number6: number; number7: number; number8: number;

}
// const data =    
const App: React.FC = () => {
    const [data, setData] = useState([])

    // 将获取写在外面方便后面调用
    

    // useEffect在第一次渲染之后和每次更新之后都会执行
   

    const columns: ColumnsType<DataType> = [
        {
            title: '团队',
            dataIndex: 'tagname',
            key: 'id',

        },
        {
            title: '2021-01-04',
            dataIndex: 'number1',
            key: 'number1',
        },
        {
            title: '2021-01-05',
            dataIndex: 'number2',
            key: 'number2',
        },
        {
            title: '2021-01-06',
            dataIndex: 'number3',
            key: 'number3',
        }, {
            title: '2021-01-07',
            dataIndex: 'number4',
            key: 'number4',
        }, {
            title: '2021-01-08',
            dataIndex: 'number5',
            key: 'number5',
        }, {
            title: '2021-01-09',
            dataIndex: 'number6',
            key: 'number6',
        }, {
            title: '2021-01-10',
            dataIndex: 'number7',
            key: 'number7',
        },

    ];
    return (
        <>
            <div style={{

                marginTop: 30, boxShadow: '2px 2px 5px 2px rgb(205, 203, 203)'

            }}>
                {/* <h1>本周签约概况</h1>
                <Table style={{

                }} columns={columns} dataSource={data} rowKey={record => record.id} /> */}

            </div>

        </>
    )
}






