import * as echarts from 'echarts';
import React,{useEffect, useState} from 'react'
import { echarts_list } from '../../../axios/apiM'
import '../datapro/style.scss'


export default function Serve() {
    var arr:any=[]
    useEffect(() => {
        echarts_list().then(res => {
            console.log(res.data);
            arr=res.data

            
        })
    }, [])
    // const[num,setnum]=useState<any>([])
    // list.forEach((item:any) => {
    //     console.log(item.number);
        
    //     setnum(item.number)
    // })
    // console.log(list);
    
    // const [num, setnum] = useState([])
    // var arr:any = []

    // list.forEach((item: any) => {
    //     console.log(item.number);
    //     arr.push(item.number)
    // });
    // setnum(arr)

    // console.log(num);
    
    setTimeout(() => {
        type EChartsOption = echarts.EChartsOption;

        var chartDom = document.getElementById('main')!;
        var myChart = echarts.init(chartDom);
        var option: EChartsOption;

        // prettier-ignore
        let dataAxis = ['1/4', '1/5', '1/6', '1/7', '1/8', '1/9', '1/10', '1/11', '1/12', '1/13', '1/14', '1/15', '1/16', '1/17', '1/18', '1/19', '1/20', '1/21', '1/22', '1/23'];
        // prettier-ignore
        let data = arr;
        let yMax = 500;
        let dataShadow = [];

        for (let i = 0; i < data.length; i++) {
            dataShadow.push(yMax);
        }

        option = {
            title: {
                text: '签约量每日统计',
                subtext: '本月最好签约量300，每日服务量'
            },
            xAxis: {
                data: dataAxis,
                axisLabel: {
                    inside: true,
                    color: '#fff'
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: false
                },
                z: 10
            },
            yAxis: {
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    color: '#999'
                }
            },
            dataZoom: [
                {
                    type: 'inside'
                }
            ],
            series: [
                {
                    type: 'bar',
                    showBackground: true,
                    itemStyle: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            { offset: 0, color: '#83bff6' },
                            { offset: 0.5, color: '#188df0' },
                            { offset: 1, color: '#188df0' }
                        ])
                    },
                    emphasis: {
                        itemStyle: {
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                                { offset: 0, color: '#2378f7' },
                                { offset: 0.7, color: '#2378f7' },
                                { offset: 1, color: '#83bff6' }
                            ])
                        }
                    },
                    data: data
                }
            ]
        };

        // Enable data zoom when user click bar.
        const zoomSize = 6;
        myChart.on('click', function (params) {
            console.log(dataAxis[Math.max(params.dataIndex - zoomSize / 2, 0)]);
            myChart.dispatchAction({
                type: 'dataZoom',
                startValue: dataAxis[Math.max(params.dataIndex - zoomSize / 2, 0)],
                endValue:
                    dataAxis[Math.min(params.dataIndex + zoomSize / 2, data.length - 1)]
            });
        });

        option && myChart.setOption(option);
    }, 300);
    return (
        <div id='main' style={{
            width: 1500, height: 800
        }}>
        </div>

    )
}




