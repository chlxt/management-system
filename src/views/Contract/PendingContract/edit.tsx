import React, { useEffect, useState } from 'react';
import './Edit.css'
import { Button, Form, Input, InputNumber, Select, message } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { Params, useParams } from 'react-router-dom';
import { seekPendingContract, EditPendingContract } from '../../../axios/apiY'
import { useNavigate } from 'react-router-dom';
const { Option } = Select;
function Edit(props: any) {
    const push = useNavigate()
    const [form] = Form.useForm()
    const formRef = React.useRef<FormInstance>(null)
    const params = useParams()
    useEffect(() => {
        //查看详情请求
        seekPendingContract({ id: params.value }).then(res => {
            form.setFieldsValue(res.data.seekPendingContractlist)

        })
    }, [])
    const layout = {
        wrapperCol: { span: 16 },
    };

    const onFinish = (values: any) => {
        // 修改的请求
        message.open({ type: 'success', content: '修改成功' });
        EditPendingContract({ obj: values }).then(res => {
            console.log(res)
            push('/MyLayout/PendingContract')
        })
    };
    return (
        <div>
            <h1>编辑签约信息</h1>
            <div>
                <h3>居民信息</h3>
                <Form
                    form={form}
                    ref={formRef}
                    {...layout}
                    name="nest-messages"
                    onFinish={onFinish}
                    style={{ display: 'flex', flexWrap: 'wrap' }}>
                    <Form.Item name={'name'} label="姓名" rules={[{ required: true, message: '不能为空!', }]} >
                        <Input disabled />
                    </Form.Item>
                    <Form.Item name={'status'} label="身份证号">
                        <Input />
                    </Form.Item>
                    <Form.Item name={'sex'} label="性别" rules={[{ required: true, message: '不能为空!', }]} >
                        <Select defaultValue="请选择">
                            <Option value="男">男</Option>
                            <Option value="女">女</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item name={'time'} label="出生年月" rules={[{ required: true, message: '不能为空!', }]} >
                        <Input />
                    </Form.Item>
                    <Form.Item name={'phone'} label="联系电话" rules={[{ required: true, message: '不能为空!', }]} >
                        <Input />
                    </Form.Item>
                    <Form.Item name={'现居地'} label="现居地" >
                        <Input />
                    </Form.Item>
                    <div className='box'>
                        <h3 style={{ width: '100%' }}>签约信息</h3>

                        <Form.Item name={'id'} label="签约编号" >
                            <span>{params.value}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </Form.Item>
                        <Form.Item name={'state'} label="签约状态" >
                            <Input />
                        </Form.Item>
                        <Form.Item name={'jigou'} label="服务机构" rules={[{ required: true, message: '不能为空!', }]}  >
                            <Select defaultValue="请选择">
                                <Option value="罗西社区服务中心">罗西社区服务中心</Option>
                                <Option value="天明社区服务中心">天明社区服务中心</Option>
                                <Option value="民进社区服务中心">民进社区服务中心</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name={'team'} label="签约团队" rules={[{ required: true, message: '不能为空!', }]}  >
                            <Select defaultValue="请选择">
                                <Option value="李明团队">李明团队</Option>
                                <Option value="李军团队">李军团队</Option>
                                <Option value="汪小敏团队">汪小敏团队</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name={'doctor'} label="服务医生" rules={[{ required: true, message: '不能为空!', }]}  >
                            <Select defaultValue="请选择">
                                <Option value="李明">李明</Option>
                                <Option value="李军">李军</Option>
                                <Option value="汪小敏">汪小敏</Option>
                            </Select>
                        </Form.Item><Form.Item name={'service'} label="服务包" rules={[{ required: true, message: '不能为空!', }]} >
                            <Select defaultValue="请选择">
                                <Option value="基础包">基础包</Option>
                                <Option value="老年人基础包">老年人基础包</Option>
                                <Option value="慢性病护理包">慢性病护理包</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name={'fuwuService'} label="服务项目" rules={[{ required: true, message: '不能为空!', }]} >
                            <Select defaultValue="请选择">
                                <Option value="高血压随访">高血压随访</Option>
                                <Option value="糖尿病">糖尿病</Option>
                                <Option value="心脏病">心脏病</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name={'address'} label="服务地点" rules={[{ required: true, message: '不能为空!', }]} >
                            <Select defaultValue="请选择">
                                <Option value="签约人家里">签约人家里</Option>
                                <Option value="机构门诊">机构门诊</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name={'time'} label="服务时间" >
                            <Input />
                        </Form.Item>
                    </div>
                    <div>
                        <Button type="primary" htmlType="submit">
                            保存
                        </Button>
                        <Button onClick={() => push('/MyLayout/PendingContract')}>
                            返回
                        </Button></div>
                </Form>
            </div>

        </div>
    );
}

export default Edit;