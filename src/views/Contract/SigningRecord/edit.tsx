import React, { useEffect, useState } from 'react';
import '../PendingContract/Edit.css'
import { Form, Input, InputNumber, Select } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { Params, useParams } from 'react-router-dom';
import { seekSigningRecord, EditSigningRecord } from '../../../axios/apiY'
import { useNavigate } from 'react-router-dom';
import { Button, } from 'antd';
import { message, Spin } from 'antd';
const { Option } = Select;
function Edit(props: any) {
    const push = useNavigate()
    const [form] = Form.useForm()
    const formRef = React.useRef<FormInstance>(null)
    const params = useParams()
    useEffect(() => {
        seekSigningRecord({ id: params?.value }).then(res => {
            console.log(res.data.seekSigningRecordlist);
            form.setFieldsValue(res.data.seekSigningRecordlist)

        })
    }, [])
    const layout = {
        wrapperCol: { span: 16 },
    };
    const onFinish = (values: any) => {
        message.open({ type: 'success', content: '修改成功' });
        // 修改的请求
        EditSigningRecord({ obj: values }).then(res => {
            push('/MyLayout/SigningRecord')
        })
    };
    return (
        <div>
            <h1>编辑签约信息</h1>
            <div>
                <h3>居民信息</h3>
                <Form
                    form={form}
                    ref={formRef}
                    {...layout}
                    name="nest-messages"
                    onFinish={onFinish}
                    style={{ display: 'flex', flexWrap: 'wrap' }}>
                    <Form.Item name={'name'} label="姓名" rules={[{ required: true, message: '不能为空!', }]} >
                        <Input disabled />
                    </Form.Item>
                    <Form.Item name={'status'} label="身份证号">
                        <Input />
                    </Form.Item>
                    <Form.Item name={'sex'} label="性别" rules={[{ required: true, message: '不能为空!', }]} >
                        <Select defaultValue="请选择">
                            <Option value="男">男</Option>
                            <Option value="女">女</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item name={'time'} label="出生年月" rules={[{ required: true, message: '不能为空!', }]} >
                        <Input />
                    </Form.Item>
                    <Form.Item name={'phone'} label="联系电话" rules={[{ required: true, message: '不能为空!', }]} >
                        <Input />
                    </Form.Item>
                    <Form.Item name={'现居地'} label="现居地" >
                        <Input />
                    </Form.Item>
                    <div className='box'>
                        <h3 style={{ width: '100%' }}>签约信息</h3>

                        <Form.Item name={'id'} label="签约编号" >
                            <span>{params.value}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </Form.Item>
                        <Form.Item name={'zhuang'} label="签约状态" >
                            <Input />
                        </Form.Item>
                        <Form.Item name={'jigou'} label="签约机构" rules={[{ required: true, message: '不能为空!', }]}  >
                            <Select defaultValue="请选择">
                                <Option value="罗西社区服务中心">罗西社区服务中心</Option>
                                <Option value="天明社区服务中心">天明社区服务中心</Option>
                                <Option value="民进社区服务中心">民进社区服务中心</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name={'team'} label="签约团队" rules={[{ required: true, message: '不能为空!', }]}  >
                            <Select defaultValue="请选择">
                                <Option value="李明团队">李明团队</Option>
                                <Option value="李军团队">李军团队</Option>
                                <Option value="汪小敏团队">汪小敏团队</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name={'state'} label="签约医生" rules={[{ required: true, message: '不能为空!', }]}  >
                            <Select defaultValue="请选择">
                                <Option value="李明">李明</Option>
                                <Option value="李军">李军</Option>
                                <Option value="汪小敏">汪小敏</Option>
                            </Select>
                        </Form.Item><Form.Item name={'service'} label="服务包" rules={[{ required: true, message: '不能为空!', }]} >
                            <Select defaultValue="请选择">
                                <Option value="基础包">基础包</Option>
                                <Option value="老年人基础包">老年人基础包</Option>
                                <Option value="慢性病护理包">慢性病护理包</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name={'type'} label="签约类型" rules={[{ required: true, message: '不能为空!', }]} >
                            <Select defaultValue="请选择">
                                <Option value="首次签约">首次签约</Option>
                                <Option value="第二次签约">第二次签约</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name={'zhouqi'} label="签约周期" rules={[{ required: true, message: '不能为空!', }]} >
                            <Input />
                        </Form.Item>
                        <Form.Item name={'money'} label="费用" >
                            <Input />
                        </Form.Item>
                        <Form.Item name={'time'} label="申请时间" >
                            <Input />
                        </Form.Item><Form.Item name={'time'} label="失效时间" >
                            <Input />
                        </Form.Item>
                    </div>
                    <div>
                        <Button type="primary" htmlType="submit">
                            保存
                        </Button>
                        <Button onClick={() => push('/MyLayout/SigningRecord')}>
                            返回
                        </Button></div>
                </Form>
            </div>

        </div>
    );
}

export default Edit;