import React from 'react';
import { Select } from 'antd';
import { Button } from 'antd';
import './PendingContract.css'
import { useState, useEffect } from 'react';
import { Space, Table } from 'antd';
import { SigningRecordList, checkSigningRecord } from '../../axios/apiY'
import type { ColumnsType } from 'antd/es/table';
import type { TableRowSelection } from 'antd/es/table/interface';
import { useNavigate, useParams } from 'react-router-dom';

//表单

function SigningRecord(props: any) {
    const push = useNavigate()
    //获取四个文本框的value
    const [val1, setval1] = useState('')
    const [val2, setval2] = useState('')
    const [val3, setval3] = useState('')
    const [val4, setval4] = useState('')

    const onChange1 = (value: string) => {
        setval1(value)
    }
    const onChange2 = (value: string) => {
        setval2(value)
    }
    const onChange3 = (value: string) => {
        setval3(value)
    }
    const onChange4 = (value: string) => {
        setval4(value)
    }
    const obj = { v1: val1, v2: val2, v3: val3, v4: val4 }
    const btnClick = () => {
        checkSigningRecord({ id: obj }).then(res => {
            setdate(res.data.checkSigningRecordlist)
        })
    }
    // 表单
    interface DataType {
        id: string;
        name: string;
        status: string;
        phone: number;
        state: string;
        team: string;
        service: string;
        time: string;//待签约的数据
        age: number;
        address: string;
    }
    //发送请求获取用户数据
    const [data, setdate] = useState([])
    useEffect(() => {
        SigningRecordList().then((res: any) => {
            setdate(res.data.SigningRecordl)
        })
    }, [])

    //修改按钮
    const Edit = (str: any) => {
        push(`/MyLayout/SigningRecordEdit/${str.id}`)
    }
    const lookContent = (str: any) => {
        push(`/MyLayout/SigningRecordContent/${str.id}`)
    }
    const columns: ColumnsType<DataType> = [
        {
            title: '编号',
            dataIndex: 'id',
        },
        {
            title: '签约人姓名',
            dataIndex: 'name',
        },
        {
            title: '身份证号',
            dataIndex: 'status',
        },
        {
            title: '手机号码',
            dataIndex: 'phone',
        },
        {
            title: '签约医生团队',
            dataIndex: 'team',
        },

        {
            title: '签约医生',
            dataIndex: 'state',
        },
        {
            title: '服务包',
            dataIndex: 'service',
        },
        {
            title: '签约状态',
            dataIndex: 'zhuang',
            render: (text, record: any) => (
                <Space size="middle">
                    {
                        text == '生效中' ? <span style={{ color: '#2984F8' }}>生效中</span> : <></>
                    }
                    {
                        text == '已过期' ? <span style={{ color: '#999' }}>已过期</span> : <></>
                    }
                </Space>
            ),
        },
        {
            title: '到期时间',
            dataIndex: 'time',
        },
        {
            title: '操作',
            render: (_, record: any) => (
                <Space size="middle">
                    <a onClick={() => lookContent(record)}>查看详情</a>
                    <a onClick={() => Edit(record)}>续约</a>
                </Space>
            ),
        }
    ];
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

    const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
        setSelectedRowKeys(newSelectedRowKeys);
    };

    const rowSelection: TableRowSelection<DataType> = {
        selectedRowKeys,
        onChange: onSelectChange,
        selections: [
            Table.SELECTION_ALL,
            Table.SELECTION_INVERT,
            Table.SELECTION_NONE,
            {
                key: 'odd',
                text: 'Select Odd Row',
                onSelect: (changeableRowKeys) => {
                    let newSelectedRowKeys = [];
                    newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
                        if (index % 2 !== 0) {
                            return false;
                        }
                        return true;
                    });
                    setSelectedRowKeys(newSelectedRowKeys);
                },
            },
            {
                key: 'even',
                text: 'Select Even Row',
                onSelect: (changeableRowKeys) => {
                    let newSelectedRowKeys = [];
                    newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
                        if (index % 2 !== 0) {
                            return true;
                        }
                        return false;
                    });
                    setSelectedRowKeys(newSelectedRowKeys);
                },
            },
        ],
    };
    return (
        <div className='box'>
            <div className='big-box'>
                <h1>签约记录</h1>
                {/* 顶部的几个选项框和查询按钮 */}
                <div className='btn-flex'>
                    <div>签约状态  <Select
                        showSearch
                        placeholder="请选择"
                        optionFilterProp="children"
                        onChange={onChange1}
                        // onSearch={onSearch}
                        filterOption={(input, option) =>
                            (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                        }
                        options={[
                            {
                                value: '生效中',
                                label: '生效中',
                            },
                            {
                                value: '已过期',
                                label: '已过期',
                            },
                        ]} /></div>
                    <div>
                        签约机构  <Select
                            showSearch
                            placeholder="请选择"
                            optionFilterProp="children"
                            onChange={onChange2}
                            // onSearch={onSearch}
                            filterOption={(input, option) =>
                                (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                            }
                            options={[
                                {
                                    value: '罗西社区服务中心',
                                    label: '罗西社区服务中心',
                                },
                                {
                                    value: '天明社区服务中心',
                                    label: '天明社区服务中心',
                                },
                                {
                                    value: '民进社区服务中心',
                                    label: '民进社区服务中心',
                                },
                            ]} /></div>
                    <br></br>
                    <br></br>
                    <div>
                        医生团队  <Select
                            showSearch
                            placeholder="请选择"
                            optionFilterProp="children"
                            onChange={onChange3}
                            // onSearch={onSearch}
                            filterOption={(input, option) =>
                                (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                            }
                            options={[
                                {
                                    value: '李明团队',
                                    label: '李明团队',
                                },
                                {
                                    value: '李军团队',
                                    label: '李军团队',
                                },
                                {
                                    value: '汪小敏团队',
                                    label: '汪小敏团队',
                                },
                            ]} /></div>
                    <div>
                        服务包  <Select
                            showSearch
                            placeholder="请选择"
                            optionFilterProp="children"
                            onChange={onChange4}
                            // onSearch={onSearch}
                            filterOption={(input, option) =>
                                (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                            }
                            options={[
                                {
                                    value: '基础包',
                                    label: '基础包',
                                },
                                {
                                    value: '老年人服务包',
                                    label: '老年人服务包',
                                },
                                {
                                    value: '慢性病护理包',
                                    label: '慢性病护理包',
                                },
                            ]} /></div>
                    <Button type="primary" size='large' onClick={btnClick}>查询 </Button>
                </div>
                {/* from信息表单 */}
                <Table rowSelection={rowSelection} columns={columns} dataSource={data} rowKey={record => record.id}
                />
            </div>
        </div>
    );
}

export default SigningRecord;