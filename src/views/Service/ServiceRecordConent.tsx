import React, { useEffect, useState } from 'react';
import './pendingService/css/ServiceConent.css'
import { Button } from 'antd';
import { Params, useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { seekServiceRecord } from '../../axios/apiY'
function ServiceRecordConent(props: any) {
    const push = useNavigate()
    const params = useParams()
    const [person, setperson] = useState<any>()
    useEffect(() => {
        seekServiceRecord({ id: params.value }).then(res => {
            setperson(res.data.seekServiceRecord)
        })
    }, [])
    return (
        <div>
            <h1>服务详情</h1>
            <div className='img-box'>
                {person?.zhuang}
            </div>
            <div>
                <h3>居民信息</h3>
                <div className='resident'>
                    <div> 姓名: <p className='blue-p'>{person?.name}</p></div>
                    <div> 身份证号: <p>{person?.status}</p></div>
                    <div> 性别: <p>{person?.sex}</p></div>
                    <div> 年龄: <p>{person?.age}</p></div>
                    <div> 联系电话: <p>{person?.phone}</p></div>
                    <div>现居地: <p>{person?.现居地}</p></div>

                </div>
            </div>
            <div>
                <h3>服务信息</h3>
                <div className='resident'>
                    <div>  服务编号: <p >{person?.id}</p></div>
                    <div>  服务状态: <p style={{ color: 'blue' }}>{person?.zhuang}</p></div>
                    <div> 服务机构: <p>{person?.服务机构}</p></div>
                    <div>  服务团队: <p>{person?.tuandui}</p></div>
                    <div>  服务医生: <p>{person?.state}</p></div>
                    <div> 服务包: <p>{person?.service}</p></div>
                    <div> 服务项目: <p >{person?.team}</p></div>
                    <div> 服务来源: <p>{person?.laiyuan}</p></div>
                    <div> 服务地点: <p>{person?.serviceadrs}</p></div>
                    <div> 预约时间: <p>{person?.time}</p></div>
                    <div> 提交时间: <p>{person?.time}</p></div>
                    <div> 审核时间: <p>{person?.time}</p></div>
                    <div> 审核人: <p>{person?.time}</p></div>
                    <div> 完成时间: <p>{person?.time}</p></div>
                    <div> 服务备注: <p>无</p></div>
                </div>
            </div>
            <div>
                <Button onClick={() => { push('/MyLayout/ServiceRecord') }}>返回</Button>

            </div>
        </div>
    );
}

export default ServiceRecordConent;