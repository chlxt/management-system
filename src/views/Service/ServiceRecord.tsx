import React from 'react';
import { Select } from 'antd';
import { Button } from 'antd';
import '../Contract/PendingContract.css'
import { useState, useEffect } from 'react';
import { Space, Table } from 'antd';
import { ServiceRecordList, checkServiceRecord } from '../../axios/apiY'
import type { ColumnsType } from 'antd/es/table';
import type { TableRowSelection } from 'antd/es/table/interface';
import { useNavigate } from 'react-router-dom';
//表单

function SigningRecord(props: any) {
    const push = useNavigate()
    //获取四个文本框的value
    const [val1, setval1] = useState('')
    const [val2, setval2] = useState('')
    const [val3, setval3] = useState('')
    const [val4, setval4] = useState('')

    const onChange1 = (value: string) => {
        setval1(value)
    }
    const onChange2 = (value: string) => {
        setval2(value)
    }
    const onChange3 = (value: string) => {
        setval3(value)
    }
    const onChange4 = (value: string) => {
        setval4(value)
    }
    const obj = { v1: val1, v2: val2, v3: val3, v4: val4 }
    const btnClick = () => {
        checkServiceRecord({ id: obj }).then(res => {
            setdate(res.data.checkServiceRecordlist)
        })
    }
    // 表单
    interface DataType {
        id: string;
        name: string;
        status: string;
        phone: number;
        state: string;
        team: string;
        service: string;
        time: string;//待签约的数据
        age: number;
        address: string;
    }
    //发送请求获取用户数据
    const [data, setdate] = useState([])
    useEffect(() => {
        ServiceRecordList().then((res: any) => {
            setdate(res.data.ServiceRecordl)
        })
    }, [])
    // 点击查看详情按钮
    const lookConent = (str: any) => {
        push(`/MyLayout/ServiceRecordConent/${str.id}`)
    }

    const columns: ColumnsType<DataType> = [
        {
            title: '编号',
            dataIndex: 'id',
        },
        {
            title: '姓名',
            dataIndex: 'name',
        },
        {
            title: '身份证号',
            dataIndex: 'status',
        },
        {
            title: '手机号码',
            dataIndex: 'phone',
        },
        {
            title: '服务包',
            dataIndex: 'service',
        },
        {
            title: '服务项目',
            dataIndex: 'team',
        },

        {
            title: '服务医生',
            dataIndex: 'state',
        },
        {
            title: '服务状态',
            dataIndex: 'zhuang',
            render: (text, record: any) => (
                <Space size="middle">
                    {
                        text == '已完成' ? <span style={{ color: '#2984F8' }}>已完成</span> : <></>
                    }
                    {
                        text == '已取消' ? <span style={{ color: '#999' }}>已取消</span> : <></>
                    }
                </Space>
            ),
        },
        {
            title: '申请时间',
            dataIndex: 'time',
        },
        {
            title: '操作',
            dataIndex: 'address',
            render: (_, record: any) => (
                <Space size="middle">
                    <a onClick={() => lookConent(record)}>查看详情</a>
                </Space>
            ),
        }
    ];
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

    const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
        setSelectedRowKeys(newSelectedRowKeys);
    };

    const rowSelection: TableRowSelection<DataType> = {
        selectedRowKeys,
        onChange: onSelectChange,
        selections: [
            Table.SELECTION_ALL,
            Table.SELECTION_INVERT,
            Table.SELECTION_NONE,
            {
                key: 'odd',
                text: 'Select Odd Row',
                onSelect: (changeableRowKeys) => {
                    let newSelectedRowKeys = [];
                    newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
                        if (index % 2 !== 0) {
                            return false;
                        }
                        return true;
                    });
                    setSelectedRowKeys(newSelectedRowKeys);
                },
            },
            {
                key: 'even',
                text: 'Select Even Row',
                onSelect: (changeableRowKeys) => {
                    let newSelectedRowKeys = [];
                    newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
                        if (index % 2 !== 0) {
                            return true;
                        }
                        return false;
                    });
                    setSelectedRowKeys(newSelectedRowKeys);
                },
            },
        ],
    };
    return (
        <div className='box'>
            <div className='big-box'>
                <h1>服务记录</h1>
                {/* 顶部的几个选项框和查询按钮 */}
                <div className='btn-flex'>
                    <div>签约状态  <Select
                        showSearch
                        placeholder="请选择"
                        optionFilterProp="children"
                        onChange={onChange1}
                        // onSearch={onSearch}
                        filterOption={(input, option) =>
                            (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                        }
                        options={[
                            {
                                value: '已完成',
                                label: '已完成',
                            },
                            {
                                value: '已取消',
                                label: '已取消',
                            },
                        ]} /></div>
                    <div>
                        签约机构  <Select
                            showSearch
                            placeholder="请选择"
                            optionFilterProp="children"
                            onChange={onChange2}
                            // onSearch={onSearch}
                            filterOption={(input, option) =>
                                (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                            }
                            options={[
                                {
                                    value: '罗西社区服务中心',
                                    label: '罗西社区服务中心',
                                },
                                {
                                    value: '天明社区服务中心',
                                    label: '天明社区服务中心',
                                },
                                {
                                    value: '民进社区服务中心',
                                    label: '民进社区服务中心',
                                },
                            ]} /></div>
                    <br></br>
                    <br></br>
                    <div>
                        医生团队  <Select
                            showSearch
                            placeholder="请选择"
                            optionFilterProp="children"
                            onChange={onChange3}
                            // onSearch={onSearch}
                            filterOption={(input, option) =>
                                (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                            }
                            options={[
                                {
                                    value: '李明团队',
                                    label: '李明团队',
                                },
                                {
                                    value: '李军团队',
                                    label: '李军团队',
                                },
                                {
                                    value: '汪小敏团队',
                                    label: '汪小敏团队',
                                },
                            ]} /></div>
                    <div>
                        服务包  <Select
                            showSearch
                            placeholder="请选择"
                            optionFilterProp="children"
                            onChange={onChange4}
                            // onSearch={onSearch}
                            filterOption={(input, option) =>
                                (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                            }
                            options={[
                                {
                                    value: '基础包',
                                    label: '基础包',
                                },
                                {
                                    value: '老年人服务包',
                                    label: '老年人服务包',
                                },
                                {
                                    value: '慢性病护理包',
                                    label: '慢性病护理包',
                                },
                            ]} /></div>
                    <Button type="primary" size='large' onClick={btnClick}>查询 </Button>
                </div>
                {/* from信息表单 */}
                <Table rowSelection={rowSelection} columns={columns} dataSource={data} rowKey={record => record.id}
                />
            </div>
        </div>
    );
}

export default SigningRecord;