import React from 'react';
import { useState } from 'react';
import { Button, Col, Form, Input, Row, Select, theme, DatePicker, message } from 'antd';
import { addServices } from '../../../axios/apiY';
import { useNavigate } from 'react-router-dom';
import { render } from '@testing-library/react';
const { Option } = Select;
//居民信息的的子组件
const AdvancedSearchForm = () => {

    const { token } = theme.useToken();
    const [form] = Form.useForm();
    const [expand, setExpand] = useState(['姓名', '性别', '身份证号', '手机号', '现居地', '服务编号', '出生年月', '服务机构', '服务团队', '服务医生', '服务包', '服务项目', '服务地点',]);

    const formStyle = {
        maxWidth: 'none',
        background: token.colorFillAlter,
        borderRadius: token.borderRadiusLG,
        padding: 24,
    };
    const getFields = () => {
        const children: any[] = [];
        expand.forEach(item => {
            if (item == '性别') {//如果是性别
                children.push(
                    <Col span={8} key={item}>
                        <Form.Item
                            name={`${item}`}
                            label={`${item}`}
                            rules={[
                                {
                                    required: true,
                                    message: '不能为空!',
                                },
                            ]}>
                            <Select defaultValue="请选择">
                                <Option value="男">男</Option>
                                <Option value="女">女</Option>
                            </Select>
                        </Form.Item>
                    </Col>,
                );
            }//如果是出生年月
            else if (item == '出生年月') {
                children.push(
                    <Col span={8} key={item}>
                        <Form.Item
                            name={`${item}`}
                            label={`${item}`}
                            rules={[
                                {
                                    required: true,
                                    message: '不能为空!',
                                },
                            ]}>

                            <DatePicker></DatePicker>
                        </Form.Item>
                    </Col>)
            } else if (item == '服务编号') {
                children.push(
                    <Col span={8} key={item}>
                        <Form.Item
                            name={`${item}`}
                            label={`${item}`}
                            rules={[
                                {
                                    required: true,
                                    message: '不能为空!',
                                },
                            ]}>
                            <Input value='123123'></Input>
                        </Form.Item>
                    </Col>)
            } else if (item == '服务机构') {
                children.push(
                    <Col span={8} key={item}>
                        <Form.Item
                            name={`${item}`}
                            label={`${item}`}
                            rules={[
                                {
                                    required: true,
                                    message: '不能为空!',
                                },
                            ]}>
                            <Select defaultValue="请选择">
                                <Option value="罗西社区服务中心">罗西社区服务中心</Option>
                                <Option value="天明社区服务中心">天明社区服务中心</Option>
                                <Option value="民进社区服务中心">民进社区服务中心</Option>
                            </Select>
                        </Form.Item>
                    </Col>)
            } else if (item == '服务团队') {
                children.push(
                    <Col span={8} key={item}>
                        <Form.Item
                            name={`${item}`}
                            label={`${item}`}
                            rules={[
                                {
                                    required: true,
                                    message: '不能为空!',
                                },
                            ]}>
                            <Select defaultValue="请选择">
                                <Option value="李军团队">李军团队</Option>
                                <Option value="汪小敏团队">汪小敏团队</Option>
                                <Option value="李明团队">李明团队</Option>
                            </Select>
                        </Form.Item>
                    </Col>)
            } else if (item == '服务医生') {
                children.push(
                    <Col span={8} key={item}>
                        <Form.Item
                            name={`${item}`}
                            label={`${item}`}
                            rules={[
                                {
                                    required: true,
                                    message: '不能为空!',
                                },
                            ]}>
                            <Select defaultValue="请选择">
                                <Option value="李军">李军</Option>
                                <Option value="汪小敏">汪小敏</Option>
                                <Option value="李明">李明</Option>
                            </Select>
                        </Form.Item>
                    </Col>)
            } else if (item == '服务包') {
                children.push(
                    <Col span={8} key={item}>
                        <Form.Item
                            name={`${item}`}
                            label={`${item}`}
                            rules={[
                                {
                                    required: true,
                                    message: '不能为空!',
                                },
                            ]}>
                            <Select defaultValue="请选择">
                                <Option value="基础包">基础包</Option>
                                <Option value="老年人服务包">老年人服务包</Option>
                                <Option value="慢性病护理包">慢性病护理包</Option>
                            </Select>
                        </Form.Item>
                    </Col>)
            } else if (item == '服务项目') {
                children.push(
                    <Col span={8} key={item}>
                        <Form.Item
                            name={`${item}`}
                            label={`${item}`}
                            rules={[
                                {
                                    required: true,
                                    message: '不能为空!',
                                },
                            ]}>
                            <Select defaultValue="请选择">
                                <Option value="高血压随访">高血压随访</Option>
                                <Option value="低血糖">低血糖</Option>
                                <Option value="心脏病">心脏病</Option>
                            </Select>
                        </Form.Item>
                    </Col>)
            } else if (item == '服务地点') {
                children.push(
                    <Col span={8} key={item}>
                        <Form.Item
                            name={`${item}`}
                            label={`${item}`}
                            rules={[
                                {
                                    required: true,
                                    message: '不能为空!',
                                },
                            ]}>
                            <Select defaultValue="请选择">
                                <Option value="签约人家里">签约人家里</Option>
                                <Option value="机构门诊">机构门诊</Option>
                            </Select>
                        </Form.Item>
                    </Col>)
            } else {
                children.push(
                    <Col span={8} key={item}>
                        <Form.Item
                            name={`${item}`}
                            label={`${item}`}
                            rules={[
                                {
                                    required: true,
                                    message: '不能为空!',
                                },
                            ]}>
                            <Input />
                        </Form.Item>
                    </Col>)
            }
        })
        return children;
    };
    const push = useNavigate()
    //确认新增按钮
    const onFinish = (values: any) => {
        values = { ...values, 'state': '待审核', 'time': '2020/10/09 10:00', 'id': `${values.服务编号}` }
        message.open({ type: 'success', content: '添加成功' });
        addServices({ ...values })
            .then((res) => {
                push('/MyLayout/PendingService')
            })
    };
    return (
        <div>
            <Form form={form} name="advanced_search" style={formStyle} onFinish={onFinish}>
                <Row gutter={12}>{getFields()}</Row>
                <Row>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit">
                            提交
                        </Button>
                        <Button
                            style={{ margin: '0 8px' }}
                            onClick={() => {
                                form.resetFields();
                            }}>
                            清空
                        </Button>
                    </Col>
                </Row>
            </Form>
        </div>
    );
};
function addService(props: any) {
    const { token } = theme.useToken();
    const listStyle: React.CSSProperties = {
        lineHeight: '200px',
        textAlign: 'center',
        background: token.colorFillAlter,
        borderRadius: token.borderRadiusLG,
        marginTop: 16,
    };
    return (
        <div>
            <h1>新增服务信息</h1>
            <div>
                <AdvancedSearchForm />
            </div>
        </div>
    );
}

export default addService;