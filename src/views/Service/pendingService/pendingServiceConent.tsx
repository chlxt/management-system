import React, { useEffect, useState } from 'react';
import './css/ServiceConent.css'
import { Button, message } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { seekPendingServices, passPendingServices, rejectPendingServices } from '../../../axios/apiY'
function PendingServiceContent(props: any) {
    const push = useNavigate()
    const params = useParams()
    const [person, setperson] = useState<any>()
    // 传id从发请求找到对应的数据
    useEffect(() => {
        seekPendingServices({ id: params.value }).then(res => {
            setperson(res.data.seeklist)
        })
    }, [])
    //点击编辑服务信息按钮
    const Edit = (str: any) => {
        push(`/MyLayout/PendingServiceEdit/${str.value}`)
    }
    //点击审核通过按钮
    const pass = (str: any) => {
        message.open({ type: 'success', content: '修改成功' });
        passPendingServices({ id: str.value }).then(res => {
            seekPendingServices({ id: params.value }).then(res => {
                setperson(res.data.seeklist)
            })
        })
    }
    // 点击驳回按钮
    const reject = (str: any) => {
        message.open({ type: 'success', content: '驳回成功' });
        rejectPendingServices({ id: str.value }).then(res => {
            seekPendingServices({ id: params.value }).then(res => {
                setperson(res.data.seeklist)
            })
        })
    }
    return (
        <div>
            <h1>服务详情</h1>
            <div className='img-box'>
                {person?.state}
            </div>
            <div>
                <h3>居民信息</h3>
                <div className='resident'>
                    <div> 姓名: <p className='blue-p'>{person?.姓名}</p></div>
                    <div> 身份证号: <p>{person?.身份证号}</p></div>
                    <div> 性别: <p>{person?.性别}</p></div>
                    <div> 年龄: <p>{person?.age}</p></div>
                    <div> 联系电话: <p>{person?.手机号}</p></div>
                    <div>现居地: <p>{person?.现居地}</p></div>

                </div>
            </div>
            <div>
                <h3>服务信息</h3>
                <div className='resident'>
                    <div>  服务编号: <p >{person?.服务编号}</p></div>
                    <div>  服务状态: <p style={{ color: 'blue' }}>{person?.state}</p></div>
                    <div> 服务机构: <p>{person?.服务机构}</p></div>
                    <div>  服务团队: <p>{person?.服务团队}</p></div>
                    <div>  服务医生: <p>{person?.服务医生}</p></div>
                    <div> 服务包: <p>{person?.服务包}</p></div>
                    <div> 服务项目: <p >{person?.服务项目}</p></div>
                    <div> 居民申请: <p>{person?.手机号}</p></div>
                    <div> 服务地点: <p>{person?.服务地点}</p></div>
                    <div> 预约时间: <p>{person?.time}</p></div>
                    <div> 提交时间: <p>{person?.出生年月}</p></div>
                    <div> 服务备注: <p>无</p></div>
                </div>
            </div>
            <div>
                <Button type="primary" onClick={() => Edit(params)}>编辑服务信息</Button>
                <Button type="link" onClick={() => pass(params)}>审核通过</Button>
                <Button type="primary" danger onClick={() => reject(params)}>驳回</Button>
                <Button onClick={() => { push('/MyLayout/PendingService') }}>返回</Button>

            </div>
        </div>
    );
}

export default PendingServiceContent;