import React from 'react';
import { Button, message, Divider, notification, Space, Checkbox, Form, Input } from 'antd';
import { Login } from '../../axios/Login'
import { useNavigate, useLocation, useSearchParams } from 'react-router-dom';
import './login.scss'


export default function User() {
    const push = useNavigate();
    const onFinish = (values: any) => {
        Login({ ...values }).then((res) => {
            console.log(res.data);
            if (res.data.code == 200) {

                message.open({
                    type: 'success',
                    content: '登录成功',
                });
                console.log('res.data.name', res.data.name);
                sessionStorage.setItem('name', res.data.name)
                console.log('res.data.roles', res.data.roles)
                sessionStorage.setItem('grants', JSON.stringify(res.data.roles))

                sessionStorage.setItem('token', res.data.token)

                push({ pathname: '/MyLayout/WorkBench', search: `?data=${res.data.name}` })

            } else if (res.data.code == 501) {

                message.open({
                    type: 'error',
                    content: '用户名输入错误或用户不存在',
                });

            } else {
                message.open({
                    type: 'error',
                    content: '用户名或密码输入错误',
                });

            }

        })
    };
    return (
        <div>


            <div className='centerbox' >
                <div className='login-left'>
                    <p style={{ color: '#fff', marginLeft: 30, marginTop: 30 }}>家庭医生后台管理系统</p>
                    <img src="https://cdn7.axureshop.com/demo/1881228/images/%E7%99%BB%E5%BD%95/u1111.png" alt="" />
                </div>
                <div className='login-right'>
                    <Form
                        className='login-from'
                        name="basic"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 25 }}
                        initialValues={{ remember: true }}
                        onFinish={onFinish}

                        autoComplete="off"
                    >
                        <img src="https://cdn7.axureshop.com/demo/1881228/images/%E7%BB%84%E4%BB%B6_%E8%A7%84%E8%8C%83/u408.jpg" alt="" />
                        <h1 style={{ marginBottom: 50 }}>欢迎登陆</h1>
                        <Form.Item
                            name="phone"
                            rules={[{ required: true, message: '请输入你的账户名称！' }, {
                                pattern: /^\d*$/,
                                message: '禁止输入除数字以外的字符',
                            }]}
                        >
                            <Input placeholder='请输入手机号码' />
                        </Form.Item>

                        <Form.Item
                            name="pass"
                            rules={[{ required: true, message: '请输入你的密码！' }, {
                                pattern: /^\d{6,8}$/,
                                message: '请输入最少六位的密码',
                            }]}
                        >
                            <Input.Password placeholder='请输入密码' />
                        </Form.Item>

                        <Form.Item name="remember" valuePropName="checked" >
                            <Checkbox style={{ color: '#999' }}>我已阅读并同意<a>《用户隐私政策》</a></Checkbox>
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" style={{ width: '100%' }}>
                                登录
                            </Button>
                        </Form.Item>
                        <p style={{ color: '#999' }}> 忘记密码请联系管理员</p>
                    </Form>

                </div>


            </div>




        </div >



    )
}

