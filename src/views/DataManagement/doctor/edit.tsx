import React, { useState, useEffect, memo } from 'react';
// import FromOrgan from './FromOrgan';
import { Button, Form, Input, Upload, Select, Alert, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import "../style/pub.scss"
import '../style/organedit.scss'
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import { doctor_doctorlit, doctor_edit, orgin_Edit, orgin_EditList } from '../../../axios/api1';
import { useForm } from 'antd/es/form/Form';
interface Props {

}
interface Value {
    address: string,
    key: string,
    name: string,
    phone: number,
    title: string
}
const { TextArea } = Input;

// interface chuan {

// }
function Edit(props: Props) {
    // 定义路由跳转
    const push = useNavigate()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    console.log(val);
    // 定义空的状态，用来获取点击的哪一个哪一个医生的信息
    var [editlist, SetEditList] = useState<Value>()
    console.log(editlist);
    // 定义响应式数据
    const [form] = Form.useForm()
    // 将editlist通过组件传值，传递给表单
    useEffect(() => {
        doctor_doctorlit({ key: val }).then((res) => {
            if (res.data.code == 200) {
                SetEditList(res.data.doctorlist)
                form.setFieldsValue(res.data.doctorlist)
                console.log(res.data.doctorlist);
            }
        })
    }, [])

    const back = () => {
        push(-1)
    }
    const pic = ({ file }: any) => {
        console.log(file.thumbUrl);
    }
    const Save = () => {
        const editdoctor = form.getFieldsValue()
        doctor_edit(editdoctor).then((res) => {
            console.log(res);
            if (res.data.code == '200') {
                push("/MyLayout/Doctor")
                message.open({
                    type: 'success',
                    content: '修改成功'
                })
            } else {
                message.open({
                    type: 'warning',
                    content: res.data.msg
                })
            }
        })
        console.log(editdoctor)
    }
    const options = [
        { value: '全科医师', label: '全科医师' },
        { value: '主任医师', label: '主任医师' },
        { value: '儿科医师', label: '儿科医师' },
        { value: '副主任医师', label: '副主任医师' }
    ]
    const sex = [
        { value: '男', label: '男' },
        { value: '女', label: '女' }
    ]
    return (

        <div>
            <h1 className='Headline'><span></span>编辑机构信息</h1>
            <div className="fromdata">
                <p>机构信息</p>
                {/* 表单信息 */}
                <>
                    <Form
                        form={form}
                        layout="horizontal"
                        style={{ maxWidth: 800, display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}
                    >
                        <Form.Item name='key' label="医生编号" style={{ width: "46%" }} >
                            <Input disabled />
                        </Form.Item>
                        <Form.Item name="phone" label="账号" style={{ width: "46%" }} >
                            <Input />
                        </Form.Item>
                        <Form.Item label="头像" valuePropName="fileList">
                            <Upload action="upload.do" maxCount={1} listType="picture-card" onChange={(value: any) => pic(value)}>
                                <div>
                                    <PlusOutlined />
                                    <div style={{ marginTop: 8 }}>点击上传</div>
                                </div>
                            </Upload>
                        </Form.Item>
                        <Form.Item name="name" label="医生姓名" style={{ width: "46%" }} >
                            <Input />
                        </Form.Item>
                        <Form.Item name="role" label="医生角色" style={{ width: "46%" }} required>
                            <Select options={options} />
                        </Form.Item>
                        <Form.Item name="sex" label="性别" style={{ width: "100%" }} required>
                            <Select options={sex} />
                        </Form.Item>
                        <Form.Item name="doctorbreif" label="医生简介" style={{ width: "100%" }}>
                            <TextArea rows={4} />
                        </Form.Item>
                        <Form.Item name="doctoradept" label="医生擅长" style={{ width: "100%" }}>
                            <TextArea rows={4} />
                        </Form.Item>
                    </Form>
                </>
            </div>
            <div className="font1">
                <Button type='primary' onClick={Save}>保存</Button>
                <Button onClick={() => back()}>返回</Button>
            </div>
        </div>
    );
}

export default memo(Edit);