import React, { useState, useEffect, memo } from 'react';
// import FromOrgan from './FromOrgan';
import { Button, Form, Input, Upload, Select } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import "../style/pub.scss"
import '../style/organedit.scss'
import { useNavigate } from 'react-router-dom';
import { doctor_add } from '../../../axios/api1';
interface Props {

}
interface Value {
    address: string,
    key: string,
    name: string,
    phone: number,
    title: string
}
const { TextArea } = Input;

function Adddoctor(props: Props) {
    // 定义路由跳转
    const push = useNavigate()
    const [form] = Form.useForm()
    const back = () => {
        push(-1)
    }
    const pic = ({ file }: any) => {
        console.log(file.thumbUrl);
    }
    const Save = () => {
        const editdoctor = form.getFieldsValue()
        doctor_add(editdoctor).then((res) => {
            console.log(res);
            if (res.data.code == '200') {
                push(-1)
            }
        })
        console.log(editdoctor)
    }
    const options = [
        { value: '全科医师', label: '全科医师' },
        { value: '主任医师', label: '主任医师' },
        { value: '儿科医师', label: '儿科医师' },
        { value: '副主任医师', label: '副主任医师' }
    ]
    const sex = [
        { value: '男', label: '男' },
        { value: '女', label: '女' }
    ]
    const orgin = [
        { value: '陕西省西安市雁塔区卫生服务站', label: '陕西省西安市雁塔区卫生服务站' },
        { value: '陕西省西安市长安区卫生服务站', label: '陕西省西安市长安区卫生服务站' },
        { value: '陕西省西安市未央区卫生服务站', label: '陕西省西安市未央区卫生服务站' },
        { value: '陕西省西安市新城区卫生服务站', label: '陕西省西安市新城区卫生服务站' },
        { value: '陕西省西安市灞桥区卫生服务站', label: '陕西省西安市灞桥区卫生服务站' }
    ]
    return (

        <div>
            <h1 className='Headline'><span></span>新增医生</h1>
            <div className="fromdata">
                <p>医生信息</p>
                {/* 表单信息 */}
                <>
                    <Form
                        form={form}
                        onFinish={Save}
                        layout="horizontal"
                        style={{ maxWidth: 800, display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}
                    >
                        <Form.Item name='key' label="医生编号" style={{ width: "46%" }} required rules={[{ required: true }]} >
                            <Input />
                        </Form.Item>
                        <Form.Item name="phone" label="账号" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item label="头像" valuePropName="fileList">
                            <Upload action="upload.do" maxCount={1} listType="picture-card" onChange={(value: any) => pic(value)}>
                                <div>
                                    <PlusOutlined />
                                    <div style={{ marginTop: 8 }}>点击上传</div>
                                </div>
                            </Upload>
                        </Form.Item>
                        <Form.Item name="name" label="医生姓名" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="role" label="医生角色" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Select options={options} />
                        </Form.Item>
                        <Form.Item name="orgin" label="所属机构" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Select options={orgin} />
                        </Form.Item>
                        <Form.Item name="sex" label="性别" style={{ width: "100%", color: '#999' }} required rules={[{ required: true }]}>
                            <Select options={sex} />
                        </Form.Item>
                        <Form.Item name="doctorbreif" label="医生简介" style={{ width: "100%" }} required rules={[{ required: true }]}>
                            <TextArea rows={4} />
                        </Form.Item>
                        <Form.Item name="doctoradept" label="医生擅长" style={{ width: "100%" }} required rules={[{ required: true }]}>
                            <TextArea rows={4} />
                        </Form.Item>
                        <Form.Item>
                            <div className="font1">
                                <Button type='primary' htmlType='submit'>保存</Button>
                                <Button onClick={() => back()}>返回</Button>
                            </div>
                        </Form.Item>
                    </Form>
                </>
            </div>

        </div>
    );
}

export default memo(Adddoctor);