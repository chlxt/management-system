import { Button, message, Switch } from 'antd';
import React, { useEffect, useState } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { doctor_del, doctor_search_a, doctor_Setsstatus, team_Setsstatus_a } from '../../../axios/api1';
import "../style/pub.scss"
import "../style/doctordetail.scss"

interface Props {

}
function DoctorDetail(props: Props) {
    // 定义路由跳转
    const push = useNavigate()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    var [doctorDetail, setDoctorDetail] = useState([])
    const setstatus = () => {
        doctor_search_a({ value: val }).then((res) => {
            if (res.data.code == 200) {
                console.log(res.data.doctorlist);
                setDoctorDetail(res.data.doctorlist)
            }
        })
    }
    useEffect(() => {
        setstatus()
    }, [val])
    // 更改状态
    const changeStatus = (val: any) => {
        console.log(val);
        doctor_Setsstatus({ key: val }).then((res: any) => {
            // setDoctorDetail(res.data.doctorlist)
            if (res.data.code == 200) {
                if (res.data.status) {
                    message.open({
                        type: 'success',
                        content: '已启用'
                    })
                } else {
                    message.open({
                        type: 'success',
                        content: '已禁用'
                    })
                }
                setstatus()
            }
        })
    }
    // 返回
    const back = () => {
        push(-1)
    }
    // 删除
    const del = (val: any) => {
        doctor_del({ key: val }).then((res) => {
            if (res.data.code == 200) {
                message.open({
                    type: 'success',
                    content: '删除成功'
                })
                push(-1)
            }
        })
    }
    // 编辑医生信息
    const edit = (val: any) => {
        push({ pathname: '/MyLayout/EditDoctor', search: `?data=${val}` })
    }
    return (
        <div>
            {/* 标题部分 */}
            <h1 className='Headline'><span></span>医生详情</h1>
            {
                doctorDetail.map((item: any, index) => {
                    return (
                        <div key={index}>
                            <div className="doctorXinxi">
                                <div className='one'>
                                    <p>医生编号：<span>{item.key}</span></p>
                                    <p>医生姓名：<span>{item.name}</span></p>
                                    <p>医生头像：<img src={item.images} alt="" style={{
                                        width: 40, height: 40, borderRadius: 40
                                    }} /></p>
                                    <p>联系电话: <span>{item.phone}</span></p>
                                </div>
                                <div className="two">
                                    <p>性别: <span>{item.sex}</span> </p>
                                    <p className='jianjie'><p>医生简介：</p> <span>{item.doctorbreif}</span> </p>
                                    <p className='shanchang'><p>医生擅长：</p><span>{item.doctoradept}</span> </p>
                                </div>

                            </div>
                            <div className="join">
                                <p className="jianjie"><p>加入团队</p><span>{item.orgin}/{item.team}/{item.role}</span></p>
                                <p>签约人数:<span className='jiange'>{item.person}</span> </p>
                                <p>医生评分:<span className='jiange'>{item.score}</span></p>
                                <p>账号状态 <Switch checkedChildren="启用" unCheckedChildren="禁用" defaultChecked checked={item.status} onChange={() => changeStatus(item.key)} /></p>
                            </div>
                            <div className="font">
                                <Button type='primary' onClick={() => { edit(item.key) }}>编辑医生信息</Button>
                                <Button type='primary' danger onClick={() => del(item.key)}>删除</Button>
                                <Button onClick={back}>返回</Button>
                            </div>


                        </div>
                    )
                })

            }

        </div>
    );
}

export default DoctorDetail;