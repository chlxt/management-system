import React, { useState, useEffect } from 'react';
import type { ColumnsType } from 'antd/es/table';
import { Popconfirm, Switch, Table, Select, Button, Input, message } from 'antd'
import { doctor_del, doctor_List, doctor_search, doctor_search_a, doctor_search_b, doctor_Setsstatus } from '../../axios/api1';
import ExportToExcel from './exportExcel'
import './style/pub.scss'
import './style/doctor.scss'
import { useNavigate } from 'react-router-dom';

interface DataType {
    key: React.Key;
    name: string;
    images: string;
    age: number;
    address: string;
}
// 定义数据类型
interface Props {

}
const { Search } = Input;
function Doctor(props: Props) {
    const push = useNavigate()
    // 定义变量用来接收数据
    const [doctor, Alldoctor] = useState([])
    const res = () => {
        doctor_List().then((res: any) => {
            console.log(res);
            Alldoctor(res.data.doctorlist)
        })
    }

    // 在useEffect钩子函数中获取列表
    useEffect(() => {
        res()
    }, [])
    console.log(doctor);
    // 改变状态的事件
    const changeStatus = (val: React.Key) => {
        doctor_Setsstatus({ key: val }).then((res: any) => {
            if (res.data.status) {
                message.open({
                    type: 'success',
                    content: '已启用'
                })
            } else {
                message.open({
                    type: 'success',
                    content: '已禁用'
                })
            }
            Alldoctor(res.data.doctorlist)
        })
    }
    // 删除的事件
    const del = (val: React.Key) => {
        doctor_del({ key: val }).then((res) => {
            if (res.data.code == 200) {
                Alldoctor(res.data.doctorlist)
                message.open({
                    type: 'success',
                    content: '删除成功'
                })
            }
        })
    }
    // 定义每一行选择的数据的key
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
    const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
        console.log('selectedRowKeys changed: ', newSelectedRowKeys);
        setSelectedRowKeys(newSelectedRowKeys);
    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    const edit = (val: any) => {
        push({ pathname: '/MyLayout/EditDoctor', search: `?data=${val}` })
    }
    const detail = (val: any) => {
        push({ pathname: '/MyLayout/DoctorDetail', search: `?data=${val}` })
    }
    // 定义医生的数据
    // 定义表格的每一列数据
    const columns: ColumnsType<DataType> = [
        {
            title: '编号',
            dataIndex: 'key',
        },
        {
            title: '头像',
            dataIndex: 'images',
            render: (record: any) => <img src={record} alt="" width="50px" height="50px" style={{ borderRadius: "50px" }} />
        },
        {
            title: '姓名',
            dataIndex: 'name',
            align: 'center'
        },
        {
            title: '联系电话',
            dataIndex: 'phone',
            align: 'center'
        },
        {
            title: '角色',
            dataIndex: 'role',
            align: 'center'
        },
        {
            title: '所属团队',
            dataIndex: 'team',
            align: 'center'
        },
        {
            title: '所属机构',
            dataIndex: 'orgin',
            align: 'center'
        },
        {
            title: '状态',
            dataIndex: 'status',
            align: 'center',
            render: (_, record: any) => <Switch checkedChildren="启用" unCheckedChildren="禁用" defaultChecked checked={record.status} onChange={() => changeStatus(record.key)} />,
        },
        {
            title: '操作',
            dataIndex: 'operation',
            align: 'center',
            render: (_, record: any) => <>
                <a onClick={() => detail(record.key)}>详情</a>&nbsp;&nbsp;&nbsp;
                <a onClick={() => edit(record.key)}>编辑</a>&nbsp;&nbsp;&nbsp;
                <Popconfirm title="Sure to delete?" onConfirm={() => del(record.key)}>
                    <a style={{ color: "red" }}>删除</a>
                </Popconfirm>
            </>
        },
    ];
    const search = (val: any) => {
        if (val) {
            doctor_search_a({ value: val }).then((res) => {
                if (res.data.code == 200) {
                    Alldoctor(res.data.doctorlist)
                }
            })
        } else {
            res()
        }

    }
    // 新增医生团队
    const addDoctor = () => {
        push('/MyLayout/AddDoctor')
    }
    const options = [
        { value: '陕西省西安市雁塔区卫生服务站', label: '雁塔区卫生服务站' },
        { value: '陕西省西安市长安区卫生服务站', label: '长安区卫生服务站' },
        { value: '陕西省西安市未央区卫生服务站', label: '未央区卫生服务站' },
        { value: '陕西省西安市新城区卫生服务站', label: '新城区区卫生服务站' },
        { value: '陕西省西安市灞桥区卫生服务站', label: '灞桥区卫生服务站' },
    ]
    const teams = [
        { value: '李军团队', label: '李军团队' },
        { value: '汪小敏团队', label: '汪小敏团队' },
        { value: '李明团队', label: '李明团队' }
    ]
    // 下拉框搜索
    const [a, seta] = useState()
    const handleChange = (value: any) => {
        console.log(value);
        // 通过选择下拉框的属性来查询数据
        seta(value)
        if (value) {
            doctor_search({ doctororgin: value }).then((res) => {
                if (res.data.code == 200) {
                    Alldoctor(res.data.doctorlist)
                }
            })
        } else {
            res()
        }
    }
    const searchTeam = (value: any) => {
        console.log(value);
        if (value) {
            doctor_search_b({ doctorteam: value }).then((res) => {
                if (res.data.code == 200) {
                    Alldoctor(res.data.doctorlist)
                }
            })
        } else {
            if (a) {
                doctor_search({ doctororgin: a }).then((res) => {
                    if (res.data.code == 200) {
                        Alldoctor(res.data.doctorlist)
                    }
                })
            } else {
                res()
            }
        }
    }
    const daochu = () => {
        var title = columns.map(item => {
            return item.title
        })
        var dataIndex = columns.map((item: any) => {
            return item.dataIndex
        })
        var name = '医生管理'
        ExportToExcel(doctor, title, dataIndex, name)
        ExportToExcel()
    }
    return (
        <div>
            {/* 标题部分 */}
            <h1 className='Headline'><span></span>医生管理</h1>
            <div className="middle">
                <div className="left">
                    <span>所属机构</span>
                    <Select
                        allowClear
                        onChange={handleChange}
                        placeholder="请选择"
                        style={{ width: 320 }}
                        options={options}
                        size="large"
                    />

                    <span>所属团队</span>
                    <Select
                        allowClear
                        onChange={searchTeam}
                        placeholder="请选择"
                        style={{ width: 320 }}
                        options={teams}
                        size="large"
                    />

                </div>
                <div className="right">
                    <Button type='primary' onClick={addDoctor}>新增医生信息</Button>
                    <Button onClick={daochu}>导出</Button>
                </div>
            </div>
            <Search allowClear onSearch={search} className='search' placeholder="请输入关键字" style={{ width: 340 }} size="large" />
            <Table rowSelection={rowSelection} columns={columns} dataSource={doctor} />
        </div>
    );
}

export default Doctor;