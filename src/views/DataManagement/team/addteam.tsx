import React, { useCallback, useEffect, useState } from 'react';
import { Form, Input, Select, Upload, Tree, Button, Modal, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import type { DataNode } from 'antd/es/tree';
import '../style/pub.scss'
import '../style/addTeam.scss'
import '../style/package.scss'

import { useNavigate } from 'react-router-dom';
import { service_package, service_packageDetail, team_addteam } from '../../../axios/api1';
interface Props {

}

function Addteam(props: Props) {
    // 定义路由跳转
    const push = useNavigate()
    // 定义表单响应式
    const [form] = Form.useForm()
    // 定义选择框的值
    const options = [
        { value: '陕西省西安市雁塔区卫生服务站', label: '雁塔区卫生服务站' },
        { value: '陕西省西安市长安区卫生服务站', label: '长安区卫生服务站' },
        { value: '陕西省西安市未央区卫生服务站', label: '未央区卫生服务站' },
        { value: '陕西省西安市新城区卫生服务站', label: '新城区卫生服务站' },
        { value: '陕西省西安市灞桥区卫生服务站', label: '灞桥区卫生服务站' }
    ]
    // 定义标签
    const treeData: DataNode[] = [
        {
            title: '高血压',
            key: '高血压',
        },
        {
            title: '糖尿病',
            key: '糖尿病',
        },
        {
            title: '高血糖',
            key: '高血糖',
        },
        {
            title: '冠心病',
            key: '冠心病',
        },
        {
            title: '肺结核',
            key: '肺结核',
        },
        {
            title: '肾结石',
            key: '肾结石',
        },
        {
            title: '胆囊炎',
            key: '胆囊炎',
        },
        {
            title: '低血糖',
            key: '低血糖',
        },
    ];
    // 定义存放服务包的状态
    const [baoList, setBaoList] = useState([])
    // 控制服务包弹出框的显示隐藏
    const [bao, setBao] = useState(false)
    // 定义状态用来存储添加的服务包列表
    const [servicePackage, setServicePackage] = useState([])
    const res = () => {
        service_package().then(async (res) => {
            console.log(res.data.servicepackage)
            setBaoList(res.data.servicepage)
        })

    }
    // 发送请求
    useEffect(() => {
        res()
    }, [])
    console.log(baoList);

    const [expandedKeys, setExpandedKeys] = useState<React.Key[]>();
    const [checkedKeys, setCheckedKeys] = useState<React.Key[]>();
    const [selectedKeys, setSelectedKeys] = useState<React.Key[]>([]);
    const [autoExpandParent, setAutoExpandParent] = useState<boolean>(true);

    const onExpand = (expandedKeysValue: React.Key[]) => {
        console.log('onExpand', expandedKeysValue);
        setExpandedKeys(expandedKeysValue);
        setAutoExpandParent(false);
    };

    const onCheck = (checkedKeysValue: any) => {
        var title1 = Object.assign({}, checkedKeysValue);
        form.setFieldValue('title', title1)
        console.log(title1);

        setCheckedKeys(checkedKeysValue);
    };

    const onSelect = (selectedKeysValue: React.Key[], info: any) => {
        console.log('onSelect', info);
        setSelectedKeys(selectedKeysValue);
    };
    // 点击返回跳路由
    const back = () => {
        push('/MyLayout/Team/Manage')
    }

    const addTeam = () => {
        const json = form.getFieldsValue()
        console.log(json);
        team_addteam(json).then((res) => {
            if (res.data.code == 200) {
                push('/MyLayout/Team/Manage')
            }
        })
    }
    const upload = (value: any) => {
        console.log(value.file.thumbUrl);
    }
    // 添加服务包
    const addpage = () => {
        service_package().then((res) => {
            console.log(res.data.servicepackage)
            setBaoList(res.data.servicepackage)
        })
        console.log(baoList);

        setBao(true)
    }
    const handleCancel = () => [
        setBao(false)
    ]
    // 点击添加服务包事件
    const packDetail = (key: any) => {
        console.log(key);
        service_packageDetail({ id: key }).then((res) => {
            if (res.data.code == 200) {
                console.log(res.data.servicepackage);
                var arr: any = []
                arr.push(res.data.servicepackage)
                setServicePackage(arr)
                form.setFieldValue('servicePackage', arr)
                setBao(false)
            }
        })
    }
    const finsh = () => {
        const json = form.getFieldsValue()
        console.log(json);
        team_addteam(json).then((res) => {
            if (res.data.code == 200) {
                push('/MyLayout/Team/Manage')
                message.open({
                    type: 'success',
                    content: '新增成功'
                })
            }
        })
    }
    return (
        <div>
            {/* 标题部分 */}
            <h1 className='Headline'><span></span>新增医生队伍</h1>
            {/* 团队信息 */}
            <div className="teamInfo">
                <h2>团队信息</h2>
                <Form form={form} onFinish={finsh}>
                    <Form.Item name='key' label='团队编号' required rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name='name' label='团队名称' required rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name='images' label='团队头像'>
                        <Upload action="/upload.do" listType="picture-card" onChange={upload}>
                            <div>
                                <PlusOutlined />
                                <div style={{ marginTop: 8 }}>点击上传</div>
                            </div>
                        </Upload>
                    </Form.Item>
                    <Form.Item name='orgin' label='所属机构' required rules={[{ required: true }]}>
                        <Select options={options} />
                    </Form.Item>
                    <Form.Item name='title' label='团队标签' required rules={[{ required: true }]}>
                        <Tree
                            checkable
                            onExpand={onExpand}
                            expandedKeys={expandedKeys}
                            autoExpandParent={autoExpandParent}
                            onCheck={onCheck}
                            checkedKeys={checkedKeys}
                            onSelect={onSelect}
                            selectedKeys={selectedKeys}
                            treeData={treeData}
                        />
                    </Form.Item>
                    <Form.Item name='servicePackage' />
                    <Form.Item name='teamMember' />
                    <Form.Item>
                        <h2>团队成员</h2>
                        {/* <Button htmlType='submit'>点击新增</Button> */}
                    </Form.Item>
                    <Form.Item>
                        <h2>服务包</h2>

                        <div className="page">
                            {
                                servicePackage.map((item: any) => {
                                    return (
                                        <>
                                            <div className="primary" key={item.packageid} >
                                                <div className="basics">
                                                    <img src={item.images} alt="" style={{ width: 136, height: 106 }} />
                                                    <div className="right">
                                                        <p>{item.packagename}</p>
                                                        <p>服务对象：{item.packageobject}</p>
                                                        <span>随访服务</span>
                                                        <span>常规护理</span>
                                                    </div>
                                                    <div id="u6738-1" className='ax_default u6738' data-label="标签" style={{ visibility: 'inherit' }}>
                                                        {/* <!-- Unnamed (矩形) --> */}
                                                        <div id="u6738-1_state0" className="panel_state u6738_state0" data-label="State1" style={{ width: 80, height: 80, visibility: "inherit", position: 'absolute', top: 0, right: 0, overflow: 'clip' }}>
                                                            <p className={` ${item.packagestatus == 1 ? 'tab1' : 'tab'}`}>使用中</p>
                                                            <p className={` ${item.packagestatus == 2 ? 'tab2' : 'tab'}`}>待审核</p>
                                                            <p className={` ${item.packagestatus == 3 ? 'tab3' : 'tab'}`}>已停用</p>
                                                            <p className={` ${item.packagestatus == 4 ? 'tab4' : 'tab'}`}>已驳回</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </>
                                    )
                                })
                            }
                            <div id="u5589" className="mouseOver" style={{ cursor: "pointer" }} onClick={addpage}>
                                <div className="text ">
                                    <p style={{ fontSize: 16 }} id="cache2"><span style={{ fontSize: 36 }} id="cache3">+ </span></p>
                                    <p style={{ fontSize: 16 }} id="cache3"><span id="cache3">添加服务包</span></p>
                                </div>
                            </div>
                        </div>
                    </Form.Item>
                    <Form.Item>
                        <Button type='primary' htmlType='submit'>确定</Button>
                        <Button onClick={back}>返回</Button>
                    </Form.Item>
                </Form>
            </div>

            {/* 添加服务包、弹出详情 */}
            <Modal style={{ width: 500 }} title="选择服务包" cancelText="取消" okText="确定" open={bao} onOk={addpage} onCancel={handleCancel}>
                <div className="chuji">
                    {
                        baoList?.map((item: any) => {
                            return (
                                <div className="primary" key={item.packageid} onClick={() => packDetail(item.packageid)}>
                                    <div className="basics">
                                        <img src={item.images} alt="" style={{ width: 136, height: 106 }} />
                                        <div className="right">
                                            <p>{item.packagename}</p>
                                            <p>服务对象：{item.packageobject}</p>
                                            <span>随访服务</span>
                                            <span>常规护理</span>
                                        </div>
                                        <div id="u6738-1" className='ax_default u6738' data-label="标签" style={{ visibility: 'inherit' }}>
                                            {/* <!-- Unnamed (矩形) --> */}
                                            <div id="u6738-1_state0" className="panel_state u6738_state0" data-label="State1" style={{ width: 80, height: 80, visibility: "inherit", position: 'absolute', top: 0, right: 0, overflow: 'clip' }}>
                                                <p className={` ${item.packagestatus == 1 ? 'tab1' : 'tab'}`}>使用中</p>
                                                <p className={` ${item.packagestatus == 2 ? 'tab2' : 'tab'}`}>待审核</p>
                                                <p className={` ${item.packagestatus == 3 ? 'tab3' : 'tab'}`}>已停用</p>
                                                <p className={` ${item.packagestatus == 4 ? 'tab4' : 'tab'}`}>已驳回</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </Modal>
        </div>
    );
}

export default Addteam;