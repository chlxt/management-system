import React, { useState, useEffect } from 'react';
import { Table, Popconfirm, Switch, Input, Select, message } from 'antd'
import type { ColumnsType } from 'antd/es/table';
import { team_dissolve, team_List, team_search, team_search_a, team_Setsstatus } from '../../../axios/api1';
import { useNavigate } from 'react-router-dom';

function Join(props: any) {
    // 定义点击退出团队事件
    const del = (val: React.Key) => {
        team_dissolve({ key: val }).then((res) => {
            if (res.data.code == 200) {
                reslist()
                message.open({
                    type: 'success',
                    content: '已退出'
                })
            }
        })
    }
    const dianji = (val: any) => {
        console.log(val);

    }
    const reslist = () => {
        team_List().then((res: any) => {
            console.log(res.data.teamlist);
            setAllData(res.data.teamlist)
        })
    }
    // 在useEffect()里面发送请求获取团队管理的数据
    useEffect(() => {
        reslist()
    }, [])
    // 定义变量用来接收团队管理列表的数据
    const [alldata, setAllData] = useState([])
    const { Search } = Input
    // 定义跳路由
    var push = useNavigate()
    // 定义下拉框的事件
    const handleChange = (value: any) => {
        console.log(value);
        // 通过选择下拉框的属性来查询数据
        if (value) {
            team_search({ orgin: value }).then((res) => {
                if (res.data.code == 200) {
                    setAllData(res.data.teamlist)
                }
            })
        } else {
            reslist()
        }
    }
    // 定义数据类型
    interface DataType {
        key: React.Key;
        name: string;
        images: string;
        age: number;
        address: string;
    }
    // 定义点击搜索事件
    const onSearch = (value: any) => {
        if (value) {
            team_search_a({ captain: value }).then((res) => {
                console.log(res.data)
                setAllData(res.data.teamlist)
            })
        } else {
            reslist()
        }
    }
    // 定义查看详情
    const detail = (key: any) => {
        push({ pathname: '/MyLayout/JoinDetail', search: `?key=${key}` })
    }
    // 定义表格的每一列数据
    const columns: ColumnsType<DataType> = [
        {
            title: '编号',
            dataIndex: 'key',
        },
        {
            title: '头像',
            dataIndex: 'images',
            render: (record: any) => <img src={record} alt="" width="50px" height="50px" style={{ borderRadius: "50px" }} />
        },
        {
            title: '团队名称',
            dataIndex: 'name',
            align: 'center'
        },
        {
            title: '团队长',
            dataIndex: 'captain',
            align: 'center'
        },
        {
            title: '团队标签',
            dataIndex: 'title',
            align: 'center',
            render: (_, record: any) => <>
                <a onClick={() => dianji(Object.values(record.title))}>aa</a>
            </>
        },
        {
            title: '所属机构',
            dataIndex: 'orgin',
            align: 'center'
        },
        {
            title: '签约人数',
            dataIndex: 'person',
            align: 'center'
        },
        {
            title: '评分',
            dataIndex: 'score',
            align: 'center'
        },
        {
            title: '状态',
            dataIndex: 'status',
            align: 'center',
            render: (_, record: any) => <>
                <span style={{ color: 'lightgreen', display: record.status ? 'block' : 'none' }} >启用中</span>
                <span style={{ color: 'hotpink', display: record.status ? 'none' : 'block' }} >未启用</span>
            </>
        },
        {
            title: '操作',
            dataIndex: 'operation',
            align: 'center',
            render: (_, record: any) => <>
                <a onClick={() => detail(record.key)}>详情</a>&nbsp;&nbsp;&nbsp;
                <Popconfirm title="Sure to delete?" onConfirm={() => del(record.key)}>
                    <a>退队</a>
                </Popconfirm>
            </>
        },
    ];
    // 定义每一行选择的数据的key
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
    const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
        console.log('selectedRowKeys changed: ', newSelectedRowKeys);
        setSelectedRowKeys(newSelectedRowKeys);
    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    return (
        <div>
            {/* 下拉框部分 */}
            <div className="select">
                <span>所属机构</span>
                <Select
                    allowClear
                    placeholder="请选择"
                    style={{ width: 200 }}
                    onChange={handleChange}
                    options={[
                        { value: '陕西省西安市雁塔区卫生服务站', label: '雁塔区卫生服务站' },
                        { value: '陕西省西安市长安区卫生服务站', label: '长安区卫生服务站' },
                        { value: '陕西省西安市未央区卫生服务站', label: '未央区卫生服务站' },
                        { value: '陕西省西安市新城区卫生服务站', label: '新城区卫生服务站' },
                        { value: '陕西省西安市灞桥区卫生服务站', label: '灞桥区卫生服务站' },
                    ]}

                    size="large"
                />
                <Search allowClear placeholder="请输入关键字" style={{ width: 340 }} onSearch={onSearch} size="large" />
            </div>
            <Table rowSelection={rowSelection} columns={columns} dataSource={alldata} />
        </div>
    );
}

export default Join;