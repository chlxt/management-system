import React, { useState, useEffect, memo } from 'react';
// import FromOrgan from './FromOrgan';
import { Button, Form, Input, message, Upload } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import "../style/pub.scss"
import '../style/organedit.scss'
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import { orgin_Edit, orgin_EditList } from '../../../axios/api1';
import { useForm } from 'antd/es/form/Form';
interface Props {

}
interface Value {
    address: string,
    key: string,
    name: string,
    phone: number,
    title: string
}
const { TextArea } = Input;

// interface chuan {

// }
function Edit(props: Props) {
    // 定义路由跳转
    const push = useNavigate()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    console.log(val);
    // 定义空的状态，用来获取点击的哪一个机构的信息
    var [editlist, SetEditList] = useState<Value>()
    console.log(editlist);
    // 定义响应式数据
    const [form] = Form.useForm()
    // 将editlist通过组件传值，传递给表单
    useEffect(() => {
        orgin_EditList({ key: val }).then((res) => {
            if (res.data.code == 200) {
                SetEditList(res.data.editlist)
                form.setFieldsValue(res.data.editlist)
                console.log(res.data.editlist);
            }
        })
    }, [])

    const back = () => {
        push('/MyLayout/Organizational')
    }
    const pic = ({ file }: any) => {
        console.log(file.thumbUrl);
    }
    const Save = () => {
        const newdata = form.getFieldsValue()
        orgin_Edit(newdata).then((res) => {
            console.log(res);
            if (res.data.code == 200) {
                push('/MyLayout/Organizational')
                message.open({
                    type: "success",
                    content: '编辑成功'
                })
            } else {
                message.open({
                    type: 'warning',
                    content: '请做出修改'
                })
            }
        })
        console.log(editlist)
    }

    return (
        <div>
            <h1 className='Headline'><span></span>编辑机构信息</h1>
            <div className="fromdata">
                <p>机构信息</p>
                {/* 表单信息 */}
                <>
                    <Form
                        form={form}
                        layout="horizontal"
                        style={{ maxWidth: 800, display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}
                    >
                        <Form.Item name='key' label="机构编号" style={{ width: "46%" }} >
                            <Input disabled />
                        </Form.Item>
                        <Form.Item name="title" label="机构名称" style={{ width: "46%" }} required>
                            <Input />
                        </Form.Item>
                        <Form.Item name="name" label="联系人姓名" style={{ width: "46%" }} >
                            <Input />
                        </Form.Item>
                        <Form.Item name="phone" label="联系电话" style={{ width: "46%" }} >
                            <Input />
                        </Form.Item>
                        <Form.Item name="address" label="机构地址" style={{ width: "100%" }} required>
                            <Input />
                        </Form.Item>
                        <Form.Item label="机构介绍" style={{ width: "100%" }}>
                            <TextArea rows={4} />
                        </Form.Item>
                        <Form.Item label="机构图片" valuePropName="fileList">
                            <Upload action="upload.do" maxCount={1} listType="picture-card" onChange={(value: any) => pic(value)}>
                                <div>
                                    <PlusOutlined />
                                    <div style={{ marginTop: 8 }}>点击上传</div>
                                </div>
                            </Upload>
                        </Form.Item>
                    </Form>
                </>
            </div>
            <div className="font">
                <Button type='primary' onClick={() => Save()}>保存</Button>
                <Button onClick={() => back()}>返回</Button>
            </div>
        </div>
    );
}

export default memo(Edit);