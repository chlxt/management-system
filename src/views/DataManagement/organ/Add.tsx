import React, { useState, useEffect, memo } from 'react';
// import FromOrgan from './FromOrgan';
import { Button, Form, Input, Upload, message } from 'antd';
import type { FormInstance } from 'antd/es/form';
import { PlusOutlined } from '@ant-design/icons';
import "../style/pub.scss"
import '../style/organedit.scss'
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import { orgin_add, orgin_Edit, orgin_EditList } from '../../../axios/api1';
interface Props {

}
interface Value {
    address: string,
    key: string,
    name: string,
    phone: number,
    title: string
}
const { TextArea } = Input;

function Add(props: Props) {
    // 定义路由跳转
    const push = useNavigate()
    //接受路由跳转传递过来的值search
    var [params, setParams] = useSearchParams()
    var val = params.get('data')
    console.log(val);
    const [form] = Form.useForm()
    const back = () => {
        push('/MyLayout/Organizational')
    }
    const finsh = () => {
        var newdata = form.getFieldsValue()
        orgin_add(newdata).then((res) => {
            console.log(res.data)
            if (res.data.code == 200) {
                push('/MyLayout/Organizational')
                message.open({
                    type: 'success',
                    content: '新增成功',
                });
            }
        })
    }
    return (

        <div>
            <h1 className='Headline'><span></span>新增机构信息</h1>
            <div className="fromdata">
                <p>机构信息</p>
                {/* <FromOrgan list={editlist} /> */}
                {/* 表单信息 */}
                <>
                    <Form
                        form={form}
                        layout="horizontal"
                        onFinish={finsh}
                        style={{ maxWidth: 800, display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}
                    >
                        <Form.Item name="key" label="机构编号" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="title" label="机构名称" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="name" label="联系人姓名" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="phone" label="联系电话" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="address" label="机构地址" style={{ width: "100%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item label="机构介绍" style={{ width: "100%" }} >
                            <TextArea rows={4} />
                        </Form.Item>

                        <Form.Item label="机构图片" valuePropName="fileList">
                            <Upload action="/upload.do" listType="picture-card">
                                <div>
                                    <PlusOutlined />
                                    <div style={{ marginTop: 8 }}>点击上传</div>
                                </div>
                            </Upload>
                        </Form.Item>
                        <Form.Item>
                            <Button type='primary' htmlType='submit' style={{ marginRight: 20 }} >保存</Button>
                            <Button onClick={() => back()}>返回</Button>
                        </Form.Item>
                    </Form>
                </>
            </div>
            {/* <div className="font">
                <Button type='primary' htmlType='submit' >保存</Button>
                <Button onClick={() => back()}>返回</Button>
            </div> */}
        </div>
    );
}

export default memo(Add);