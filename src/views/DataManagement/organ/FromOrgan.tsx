import React, { useEffect, useState } from 'react';
import { PlusOutlined } from '@ant-design/icons';
import {
    Form,
    Input,
    Upload,
} from 'antd';
import { orgin_EditList, orgin_List } from '../../../axios/api1';
import { useSearchParams } from 'react-router-dom';
interface Props {
    address: string,
    key: string,
    name: string,
    phone: number,
    title: string
}

const { TextArea } = Input;

const FormDisabledDemo: React.FC = () => {
    // 获取传过来的数据


    // 进行一系列转换，最终转为字符串
    // var str = ''
    // useEffect(() => {
    //     str = Object.values(data).join("")
    //     orgin_EditList({ key: str }).then((res) => {
    //         console.log(res.data)
    //     })
    // }, [str])
    return (
        <>
            <Form
                layout="horizontal"
                style={{ maxWidth: 800, display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}
            >
                <Form.Item label="机构编号" style={{ width: "46%" }} >
                    <span>208902</span>
                </Form.Item>
                <Form.Item label="机构名称" style={{ width: "46%" }} required>
                    <Input />
                </Form.Item>
                <Form.Item label="联系人姓名" style={{ width: "46%" }} >
                    <Input />
                </Form.Item>
                <Form.Item label="联系电话" style={{ width: "46%" }} >
                    <Input />
                </Form.Item>
                <Form.Item label="机构地址" style={{ width: "100%" }} required>
                    <Input />
                </Form.Item>
                <Form.Item label="机构介绍" style={{ width: "100%" }}>
                    <TextArea rows={4} />
                </Form.Item>

                <Form.Item label="机构图片" valuePropName="fileList">
                    <Upload action="/upload.do" listType="picture-card">
                        <div>
                            <PlusOutlined />
                            <div style={{ marginTop: 8 }}>点击上传</div>
                        </div>
                    </Upload>
                </Form.Item>
            </Form>
        </>
    );
};

export default () => <FormDisabledDemo />;
// address='Props.address' key='Props.key' name='Props.name' phone={Props.phone} title=
//     "Props.title"