import React, { Suspense, useEffect, useState } from 'react';
import { Modal, Button, Input, Table } from 'antd'
import type { ColumnsType } from 'antd/es/table';
import './style/pub.scss'
import './style/team.scss'
import ExportToExcel from './exportExcel'
import { NavLink, Outlet, useNavigate } from 'react-router-dom';
import { team_join, team_search_a, team_search_b } from '../../axios/api1';
interface Props {

}
interface DataType {
    key: React.Key,
    images: string,
    name: string,
    captain: string,
    orgin: string,
    status: boolean
}
function Team(props: Props) {
    // 定义跳转路由
    const push = useNavigate()
    // 控制弹出框的显示隐藏
    const [isModalOpen, setIsModalOpen] = useState(false);
    // 控制加入确认的弹出框的显示隐藏
    const [confirm, setConfirm] = useState(false)
    // 定义加入的是谁的队伍
    const [captain, setCaptain] = useState()
    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };
    const handleOk_a = () => {
        setConfirm(false);
    };

    const handleCancel_a = () => {
        setConfirm(false);
    };
    // 定义搜索出来的团队数据
    const [alldata, setAllData] = useState([])
    // 定义输入框的响应式数据
    const [serial, setSerial] = useState()
    // 定义申请加入团队事件
    const join = () => {
        setIsModalOpen(true);
    }
    // 定义输入框的onchange事件
    const change = (e: any) => {
        setSerial(e.target.value)
    }
    // 搜索团队
    const search = () => {
        team_join({ val: serial }).then((res) => {
            if (res.data.code == 200) {
                setList(true)
                setAllData(res.data.teamlist)
            }
            console.log(res.data);
        })
    }
    // 操作中的申请加入
    const jiaru = (recode: any) => {
        console.log(recode);
        team_search_b({ key: recode.key }).then((res) => {
            console.log(res.data.teamlist.name)
            if (res.data.code == 200) {
                setIsModalOpen(false);
                setConfirm(true)
                setCaptain(res.data.teamlist.name)
            }
        })

    }
    // 定义新增医生团队
    const addTeam = () => {
        push('/MyLayout/AddTeam')
    }
    //定义每一列的数据类型
    // 定义表格的每一列数据
    const columns: ColumnsType<DataType> = [
        {
            title: '编号',
            dataIndex: 'key',
        },
        {
            title: '头像',
            dataIndex: 'images',
            render: (record: any) => <img src={record} alt="" width="50px" height="50px" style={{ borderRadius: "50px" }} />
        },
        {
            title: '团队名称',
            dataIndex: 'name',
            align: 'center'
        },
        {
            title: '团队长',
            dataIndex: 'captain',
            align: 'center'
        },
        {
            title: '所属机构',
            dataIndex: 'orgin',
            align: 'center'
        },
        {
            title: '团队状态',
            dataIndex: 'status',
            align: 'center',
            render: (_, record: any) => <>
                <span style={{ color: 'lightgreen', display: record.status ? 'block' : 'none' }} >启用中</span>
                <span style={{ color: 'hotpink', display: record.status ? 'none' : 'block' }} >未启用</span>
            </>
        },
        {
            title: '操作',
            dataIndex: 'operation',
            align: 'center',
            render: (_, record: any) => <a style={{ color: 'blueviolet' }} onClick={() => jiaru(record)}>申请加入</a>

        },
    ];
    // 控制表格的显示隐藏
    const [list, setList] = useState(false)
    // // 导出
    // const daochu = () => {

    //     var title = columns.map(item => {
    //         return item.title
    //     })
    //     var dataIndex = columns.map((item: any) => {
    //         return item.dataIndex
    //     })
    //     var name = '团队管理'
    //     ExportToExcel(alldata, title, dataIndex, name)
    // }
    return (
        <div>
            {/* 标题部分 */}
            <h1 className='Headline'><span></span>团队管理</h1>
            {/* 管理团队部分 */}
            <div className="manage">
                <div className="left">
                    <NavLink to="/MyLayout/Team/Manage">我管理的团队</NavLink>
                    <NavLink to="/MyLayout/Team/Join">我加入的团队</NavLink>
                </div>
                <div className="right">
                    <Button type="primary" size='large' onClick={addTeam}>
                        新增医生团队
                    </Button>
                    <Button type="primary" size='large' onClick={join}>
                        申请加入团队
                    </Button>
                    <Button size='large' >导出</Button>
                </div>
                <Modal style={{ width: 700 }} title="申请加入团队" cancelText="取消" okText="确定" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <div className="jiaru">
                        <Input placeholder="请输入团队名称或者团队编号" onChange={(e) => change(e)} style={{ width: '80%', height: 40 }} />
                        <Button onClick={search} type='primary' style={{ height: 40 }}>搜索</Button>
                    </div>
                    {/* <Table columns={columns} dataSource={alldata} style={{ display: list ? 'block' : 'none' }} /> */}
                </Modal>
                <Modal title="加入确认？" open={confirm} onOk={handleOk_a} onCancel={handleCancel_a}>
                    <span>确认申请加入{captain}吗？</span>
                </Modal>
            </div>
            <Suspense>
                <Outlet />
            </Suspense>

        </div>
    );
}

export default Team;