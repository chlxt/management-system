import React, { memo } from 'react';
import { Button, Form, Input, Select } from 'antd';
import "../style/pub.scss"
import '../style/organedit.scss'
import '../style/addpackage.scss'
import { useNavigate } from 'react-router-dom';
import { service_addavailable } from '../../../axios/api1';
interface Props {

}
interface Value {
    address: string,
    key: string,
    name: string,
    phone: number,
    title: string
}
const { TextArea } = Input;
function Addproject(props: Props) {
    // 定义路由跳转
    const push = useNavigate()
    const [form] = Form.useForm()
    const back = () => {
        push(-1)
    }
    const pic = ({ file }: any) => {
        console.log(file.thumbUrl);
    }
    const Save = () => {
        const json = form.getFieldsValue()
        service_addavailable(json).then((res) => {
            console.log(res);
            if (res.data.code == '200') {
                push(-1)
            }
        })
        console.log(json)
    }
    const options = [
        { value: '免费项目', label: '免费项目' },
        { value: '付费项目', label: '付费项目' },
    ]
    return (

        <div>
            <h1 className='Headline'><span></span>新增服务项目</h1>
            <div className="fromdata">
                <p>服务项目信息</p>
                {/* 表单信息 */}
                <>
                    <Form
                        form={form}
                        onFinish={Save}
                        layout="horizontal"
                        style={{ maxWidth: 800, display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}
                    >
                        <Form.Item name='Availableid' label="项目编号" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name='Availablename' label="项目名称" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="Availablenum" label="服务次数" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="Availabletype" label="项目类型" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Select options={options} />
                        </Form.Item>
                        <Form.Item name="Availableprice" label="项目原价" style={{ width: "100%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="Availableintroduce" label="服务介绍" style={{ width: "100%" }} required rules={[{ required: true }]}>
                            <TextArea rows={4} />
                        </Form.Item>
                        <Form.Item>
                            <div className="font2">
                                <Button type='primary' htmlType='submit'>新增项目</Button>
                                <Button onClick={() => back()}>返回</Button>
                            </div>
                        </Form.Item>
                    </Form>
                </>
            </div>

        </div>
    );
}

export default memo(Addproject);