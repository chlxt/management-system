import { Button, Form, Input, message, Select } from 'antd';
import React, { useEffect } from 'react';
import '../style/addpackage.scss'
import { Navigate, useNavigate, useSearchParams } from 'react-router-dom';
import { edit_project, service_searchProject } from '../../../axios/api1';
interface Props {

}
const { TextArea } = Input;
function EditProject(props: Props) {
    const [form] = Form.useForm()
    const push = useNavigate()
    const [params, setParams] = useSearchParams()
    var key = params.get('key')
    console.log("editproject", key);
    const options = [
        { value: '免费项目', label: '免费项目' },
        { value: '付费项目', label: '付费项目' },
    ]
    const projectDetail = () => {
        service_searchProject({ key: key }).then((res) => {
            if (res.data.code == 200) {
                form.setFieldsValue(res.data.available)
            }
        })
    }
    useEffect(() => {
        projectDetail()
    }, [key])
    const Save = () => {
        var json = form.getFieldsValue()
        edit_project(json).then((res) => {
            if (res.data.code == 200) {
                push(-1)
                message.open({
                    type: 'success',
                    content: '编辑成功'
                })
            } else {
                message.open({
                    type: 'warning',
                    content: '请做出修改'
                })
            }
        })
    }
    const back = () => {
        push(-1)
    }
    return (
        <div>
            <h1 className='Headline'><span></span>新增服务项目</h1>
            <div className="fromdata">
                <p style={{ fontSize: 16, margin: "20px 0" }}>服务项目信息</p>
                {/* 表单信息 */}
                <>
                    <Form
                        form={form}
                        onFinish={Save}
                        layout="horizontal"
                        style={{ maxWidth: 800, display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}
                    >
                        <Form.Item name='Availableid' label="项目编号" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input disabled />
                        </Form.Item>
                        <Form.Item name='Availablename' label="项目名称" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="Availablenum" label="服务次数" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="Availabletype" label="项目类型" style={{ width: "46%" }} required rules={[{ required: true }]}>
                            <Select options={options} />
                        </Form.Item>
                        <Form.Item name="Availableprice" label="项目原价" style={{ width: "100%" }} required rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="Availableintroduce" label="服务介绍" style={{ width: "100%" }} required rules={[{ required: true }]}>
                            <TextArea rows={4} />
                        </Form.Item>
                        <Form.Item>
                            <div className="font2">
                                <Button type='primary' htmlType='submit'>保存</Button>
                                <Button onClick={() => back()}>返回</Button>
                            </div>
                        </Form.Item>
                    </Form>
                </>

            </div>
        </div>
    );
}

export default EditProject;