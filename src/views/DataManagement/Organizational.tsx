import React, { useEffect, useState } from 'react';
import { Button, Table, Popconfirm, Switch, Input, message } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import ExportToExcel from './exportExcel'
// 导入样式
import './style/orgin.scss'
import './style/pub.scss'
import { orgin_Del, orgin_EditList, orgin_List, orgin_search, orgin_Setstatus } from '../../axios/api1';
import { useNavigate } from 'react-router-dom';
interface Props {

}
interface DataType {
    key: React.Key;
    name: string;
    images: string;
    age: number;
    address: string;
}

interface Option {
    fileName: string,
    datas: []
}

// 函数式组件的事件体，Hooks只能在函数体内部使用
const Orgin: React.FC = () => {

    // 定义路由跳转
    const push = useNavigate()
    const [alldata, setAllData] = useState<DataType[]>([])
    // 封装发请求事件
    const resList = () => {
        console.log('aa');

        orgin_List().then((res: any) => {
            console.log(res.data);
            console.log(res.data.arr);
            setAllData(res.data.orginlist)
        })
    }
    // 定义点击删除的事件
    const del = (value: any) => {
        orgin_Del({ key: value }).then((res) => {
            if (res.data.code == 200) {
                resList()
                message.open({
                    type: "success",
                    content: '已删除'
                })
            }
        })
        console.log(value);
    }
    // 在钩子函数中发送请求获取数据
    useEffect(() => {
        //    使用封装的请求
        resList()
    }, [])
    // 定义改变状态的事件
    const changeStatus = (val: any) => {
        console.log(val);
        orgin_Setstatus({ key: val }).then((res: any) => {
            if (res.data.code == 200) {
                if (res.data.status) {
                    message.open({
                        type: 'success',
                        content: '已启用',
                    });
                } else {
                    message.open({
                        type: 'success',
                        content: '已禁用',
                    });
                }
                console.log(res.data);
                resList()
            }
        })
    }
    var [editlist, SetEditList] = useState({})
    // 编辑机构信息
    const edit = (val: any) => {
        console.log(val)
        push({ pathname: '/MyLayout/Edit', search: `?data=${val}` })
    }
    // 定义表格的每一行的数据格式
    const columns: ColumnsType<DataType> = [
        {
            title: '编号',
            dataIndex: 'key',
        },
        {
            title: '机构图片',
            dataIndex: 'images',
            render: (record: any) => <img src={record} alt="" width="80px" height="80px" />
        },
        {
            title: '机构名称',
            dataIndex: 'title',
            align: 'center'
        },
        {
            title: '负责人姓名',
            dataIndex: 'name',
            align: 'center'
        },
        {
            title: '电话',
            dataIndex: 'phone',
            align: 'center'
        },
        {
            title: '机构地址',
            dataIndex: 'address',
            align: 'center'
        },
        {
            title: '状态',
            dataIndex: 'status',
            render: (_, record: any) => <Switch checkedChildren="启用" unCheckedChildren="禁用" defaultChecked checked={record.status} onChange={() => changeStatus(record.key)} />,
            align: 'center'
        },
        {
            title: '操作',
            dataIndex: 'operation',
            render: (_, record: { key: React.Key }) =>
                <>
                    <a onClick={() => edit(record.key)}>edit</a>&nbsp;&nbsp;&nbsp;
                    <Popconfirm title="Sure to delete?" onConfirm={() => del(record.key)}>
                        <a>delete</a>
                    </Popconfirm>
                </>,
            align: 'center'
        },
    ];
    // 定义表格最前面的单选框，使用数组进行接收
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
    const [loading, setLoading] = useState(false);
    const start = () => {
        setLoading(true);
        setTimeout(() => {
            setSelectedRowKeys([]);
            setLoading(false);
        }, 1000);
    };

    const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
        console.log('selectedRowKeys changed: ', newSelectedRowKeys);
        setSelectedRowKeys(newSelectedRowKeys);
    };

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    // 新增机构
    const add = () => {
        push("/MyLayout/Add")
    }
    const hasSelected = selectedRowKeys.length > 0;
    // 定义搜索框
    const { Search } = Input;
    // 点击搜索触发事件
    const onSearch = (value: string) => {
        console.log(value)
        if (value) {
            orgin_search({ key: value }).then((res) => {
                console.log(value)
                // if (res.data.code == '200') {
                console.log(res.data.orginlist)
                setAllData(res.data.orginlist)
                // }
            })
        } else {
            resList()
        }

    };
    const daochu = () => {
        var title = columns.map(item => {
            return item.title
        })
        var dataIndex = columns.map((item: any) => {
            return item.dataIndex
        })
        var name = '机构管理'
        ExportToExcel(alldata, title, dataIndex, name)
    }
    return (
        <div>
            <h1 className='Headline'><span></span>机构管理</h1>
            <div className='orgin' style={{ marginBottom: 60 }}>
                {/* 定义搜索框以及新增和导出 */}
                <Search allowClear placeholder="请输入编号" style={{ width: 340 }} onSearch={onSearch} size="large" />
                <div className="btn">
                    <Button type="primary" size='large' onClick={add}>
                        新增机构
                    </Button>
                    <Button size='large' onClick={daochu}>导出</Button>
                </div>
            </div>
            <Table rowSelection={rowSelection} columns={columns} dataSource={alldata} />
        </div>
    );
}

export default Orgin;