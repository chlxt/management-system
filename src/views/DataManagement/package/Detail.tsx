import React, { useEffect, useState } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { Button, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { package_bohui, package_setstatus, service_packageDetail } from '../../../axios/api1';
import '../style/pub.scss'
import '../style/packdetail.scss'
interface Props {

}
function Detail(props: Props) {
    // 定义服务包项目类型
    interface DataType {
        Availableid: React.Key,
        Availablename: string,
        Availablenum: string,
        Availabletype: string,
        Availableintroduce: string,
    }
    const push = useNavigate()
    const [pack, setPack] = useSearchParams()
    var packageid = pack.get('id')
    console.log(packageid);
    // 定义一个状态，用来存放服务详情
    const [packdetail, setPackDetail] = useState([])
    useEffect(() => {
        service_packageDetail({ id: packageid }).then((res) => {
            if (res.data.code == 200) {
                var arr: any = []
                arr.push(res.data.servicepackage)
                setPackDetail(arr)
            }
        })
    }, [packageid])
    const columns: ColumnsType<DataType> = [
        {
            title: '项目编号',
            dataIndex: 'Availableid',
        },
        {
            title: '项目名称',
            dataIndex: 'Availablename',
        },
        {
            title: '次数',
            dataIndex: 'Availablenum',
            align: 'center'
        },
        {
            title: '项目类型',
            dataIndex: 'Availabletype',
            align: 'center'
        },
        {
            title: '项目介绍',
            dataIndex: 'Availableintroduce',
            align: 'center'
        }
    ];
    // 编辑服务包
    const edit = (key: any) => {
        console.log(key)
        push({ pathname: '/MyLayout/EditPack', search: `key=${key}` })
    }
    // 修改服务包状态
    const setStatus = (status: any) => {
        package_setstatus({ id: packageid, status: status }).then((res) => {
            if (res.data.code == 200) {
                var arr: any = []
                arr.push(res.data.servicepackage)
                setPackDetail(arr)
                push(-1)
            }
        })
    }
    // 驳回
    const Bohui = (status: any) => {
        package_bohui({ id: packageid, status: status }).then((res) => {
            if (res.data.code == 200) {
                var arr: any = []
                arr.push(res.data.servicepackage)
                setPackDetail(arr)
                push(-1)
            }
        })
    }
    const back = () => {
        push(-1)
    }
    return (
        <div>
            {/* 标题部分 */}
            <h1 className='Headline'><span></span>服务包详情</h1>
            <div className="head">
                {
                    packdetail.map((item: any) => {
                        return (
                            <>
                                <div className="detail">
                                    <img src={item.images} alt="" style={{ width: 336, height: 260 }} />
                                    <div className="right">
                                        <p>{item.packagename}</p>
                                        <p>订购定价：￥{item.price}</p>
                                        <p>标签
                                            {
                                                Object.values(item.title).map((item: any) => {
                                                    return (
                                                        <>
                                                            <span>{item}</span>
                                                        </>
                                                    )
                                                })
                                            }
                                        </p>
                                        <p>服务对象：{item.packageobject}</p>
                                        <p>签约周期:{item.period}年（可续约）</p>
                                        <p>服务介绍：{item.introduce}</p>
                                    </div>
                                </div>
                                <p>服务项目</p>
                                <Table className='packtable' columns={columns} style={{ fontSize: '12px' }} dataSource={item.servicesAvailable} />
                                <Button type='primary' onClick={() => edit(item.packageid)}>编辑</Button>
                                <Button type='primary' danger className={` ${item.packagestatus == 1 ? 'tab1' : 'tab'}`} onClick={() => setStatus(item.packagestatus)}>停用</Button>
                                <Button type='primary' style={{ backgroundColor: 'green' }} onClick={() => setStatus(item.packagestatus)} className={` ${item.packagestatus == 2 ? 'tab2' : 'tab'}`}>审核通过</Button>
                                <Button type='primary' danger className={` ${item.packagestatus == 2 ? 'tab2' : 'tab'}`} onClick={() => Bohui(item.packagestatus)}>驳回</Button>
                                <Button type='primary' style={{ backgroundColor: 'green' }} onClick={() => setStatus(item.packagestatus)} className={` ${item.packagestatus == 3 ? 'tab3' : 'tab'}`}>启用</Button>
                                {/* <Button className={` ${item.packagestatus == 4 ? 'tab4' : 'tab'}`}>已驳回</Button> */}
                                <Button onClick={back}>返回</Button>
                            </>
                        )
                    })
                }
            </div>

        </div>
    );
}

export default Detail;