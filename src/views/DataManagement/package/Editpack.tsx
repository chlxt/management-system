import React, { useState, useEffect, memo } from 'react';
import { Button, Form, Input, Upload, Select, Tree, Table, Modal } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import type { DataNode } from 'antd/es/tree';
import type { ColumnsType } from 'antd/es/table';
import "../style/pub.scss"
import '../style/organedit.scss'
import { useNavigate, useSearchParams } from 'react-router-dom';
import { available_search_a, doctor_add, service_available, service_editpackage, service_packageDetail, service_searchProject } from '../../../axios/api1';
import { useForm } from 'antd/es/form/Form';
interface Props {

}
interface Value {
    address: string,
    key: string,
    name: string,
    phone: number,
    title: string
}
// 定义服务包项目类型
interface DataType {
    Availableid: React.Key,
    Availablename: String,
    Availablenum: String,
    Availabletype: String,
    Availableintroduce: String,
}
const { TextArea } = Input;
function Addpack(props: Props) {
    // 定义路由跳转
    const push = useNavigate()
    // 接收传递过来的参数
    const [params, setParams] = useSearchParams()
    var key = params.get('key')
    // 定义响应式数据
    const [form] = Form.useForm()
    // 定义响应式数据，用来存放添加的服务项目
    const [serviceProject, setServiceProject] = useState<any>([])
    const res = () => {
        service_packageDetail({ id: key }).then((res) => {
            if (res.data.code == 200) {
                console.log(res.data.servicepackage);
                form.setFieldsValue(res.data.servicepackage)
                console.log(Object.values(res.data.servicepackage.title));
                // form.setFieldValue('title', Object.values(res.data.servicepackage.title))
                setServiceProject(res.data.servicepackage.servicesAvailable)
            }
        })
        service_available().then((res) => {
            setPack(res.data.available)
        })
    }
    useEffect(() => {
        res()
    }, [key])
    const [addPack, setPack] = useState([])
    const back = () => {
        push(-1)
    }
    // 定义一个状态用来控制新增弹出框的显示和隐藏
    const [showProject, setShowProject] = useState(false)
    const pic = ({ file }: any) => {
        console.log(file.thumbUrl);
    }
    const Save = () => {
        form.setFieldValue('packagestatus', 2)
        form.setFieldValue('servicesAvailable', serviceProject)
        const json = form.getFieldsValue()
        service_editpackage(json).then((res) => {
            if (res.data.code == '200') {
                push(-1)
            }
        })
        console.log(json)
    }
    const period = [
        { value: '1', label: '1年' },
        { value: '2', label: '2年' },
        { value: '3', label: '3年' },
        { value: '4', label: '4年' },
        { value: '5', label: '5年' }
    ]
    const type = [
        { value: '基础包', label: '基础包' },
        { value: '老人服务包', label: '老人服务包' },
        { value: '儿童服务包', label: '儿童服务包' },
        { value: '定制包', label: '定制包' },
    ]
    const [expandedKeys, setExpandedKeys] = useState<React.Key[]>();
    const [checkedKeys, setCheckedKeys] = useState<React.Key[]>();
    const [selectedKeys, setSelectedKeys] = useState<React.Key[]>([]);
    const [autoExpandParent, setAutoExpandParent] = useState<boolean>(true);

    const onExpand = (expandedKeysValue: React.Key[]) => {
        console.log('onExpand', expandedKeysValue);
        setExpandedKeys(expandedKeysValue);
        setAutoExpandParent(false);
    };

    const onCheck = (checkedKeysValue: any) => {
        var title1 = Object.assign({}, checkedKeysValue);
        form.setFieldValue('title', title1)
        console.log(title1);
        setCheckedKeys(checkedKeysValue);
    };

    const onSelect = (selectedKeysValue: React.Key[], info: any) => {
        console.log('onSelect', info);
        setSelectedKeys(selectedKeysValue);
    };// 定义标签
    const treeData: DataNode[] = [
        {
            title: '高血压',
            key: '高血压',
        },
        {
            title: '糖尿病',
            key: '糖尿病',
        },
        {
            title: '慢病护理',
            key: '慢病护理',
        },
        {
            title: '高血糖',
            key: '高血糖',
        },

    ];
    // 定义表格数据
    const columns: ColumnsType<DataType> = [
        {
            title: '项目编号',
            dataIndex: 'Availableid',
        },
        {
            title: '项目名称',
            dataIndex: 'Availablename',
        },
        {
            title: '次数',
            dataIndex: 'Availablenum',
            align: 'center'
        },
        {
            title: '项目类型',
            dataIndex: 'Availabletype',
            align: 'center'
        },
        {
            title: '项目介绍',
            dataIndex: 'Availableintroduce',
            align: 'center'
        }
    ];
    // 定义每一行选择的数据的key
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
    const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
        console.log('selectedRowKeys changed: ', newSelectedRowKeys);
        setSelectedRowKeys(newSelectedRowKeys);
    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    const addproject = () => {
        setShowProject(true)
    }
    const handlecancle = () => {
        setShowProject(false)
    }
    const handleOk = () => {
        console.log(selectedRowKeys);
        var list: any = []
        selectedRowKeys.forEach((item) => {
            service_searchProject({ key: item }).then((res) => {
                console.log(res.data.available);
                list.push(res.data.available)
                // var arr = serviceProject.push(res.data.available)
                console.log(list);
                // var arr = list.concat(serviceProject)
                var arr = serviceProject.concat(list)
                console.log(arr);
                setServiceProject(arr)
            })
        })
        setShowProject(false)
    }
    return (

        <div>
            <h1 className='Headline'><span></span>编辑服务包</h1>
            <div className="fromdata">
                <p>服务包信息</p>
                {/* 表单信息 */}
                <>
                    <Form
                        form={form}
                        layout="horizontal"
                        style={{ maxWidth: 800, display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap' }}
                    >
                        <Form.Item name='packageid' label="服务包编号" style={{ width: "46%" }} >
                            <Input disabled />
                        </Form.Item>
                        <Form.Item name="packagename" label="服务包类型" style={{ width: "46%" }} required>
                            <Select options={type} />
                        </Form.Item>
                        <Form.Item name="price" label="订购价格" style={{ width: "46%" }} >
                            <Input />
                        </Form.Item>元/年
                        <Form.Item name="packageobject" label="服务对象" style={{ width: "46%" }} >
                            <Input />
                        </Form.Item>
                        {/* <Form.Item name="title" label="服务包标签" style={{ width: "46%" }} >
                            <Tree
                                checkable
                                onExpand={onExpand}
                                expandedKeys={expandedKeys}
                                autoExpandParent={autoExpandParent}
                                onCheck={onCheck}
                                checkedKeys={checkedKeys}
                                onSelect={onSelect}
                                selectedKeys={selectedKeys}
                                treeData={treeData}
                            />
                        </Form.Item> */}
                        <Form.Item name="period" label="签约周期" style={{ width: "46%" }} required>
                            <Select options={period} />
                        </Form.Item>

                        <Form.Item label="服务包图片" valuePropName="fileList">
                            <Upload action="upload.do" maxCount={1} listType="picture-card" onChange={(value: any) => pic(value)}>
                                <div>
                                    <PlusOutlined />
                                    <div style={{ marginTop: 8 }}>点击上传</div>
                                </div>
                            </Upload>
                        </Form.Item>
                        <Form.Item name="introduce" label="服务介绍" style={{ width: "100%" }}>
                            <TextArea rows={4} />
                        </Form.Item>
                        <Form.Item name='packagestatus' />
                        <Form.Item name='servicesAvailable' />


                    </Form>
                </>
            </div>
            <div className="project">
                <p>服务项目</p>
                <Button type='primary' onClick={addproject}>新增服务项目</Button>
                <Table className='packtable' columns={columns} style={{ fontSize: '12px' }} dataSource={serviceProject} />
            </div>
            <div className="font">
                <Button type='primary' onClick={Save}>提交审核</Button>
                <Button onClick={() => back()}>返回</Button>
            </div>
            <Modal open={showProject} onCancel={handlecancle} onOk={handleOk}>
                <Table rowKey={(recode) => recode.Availableid} rowSelection={rowSelection} onChange={onCheck} className='packtable' columns={columns} style={{ fontSize: '12px' }} dataSource={addPack} />
            </Modal>
        </div>
    );
}

export default memo(Addpack);