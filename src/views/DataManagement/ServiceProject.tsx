import React, { useEffect, useState } from 'react';
import { Select, Input, Button, Table, Popconfirm, message } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import './style/pub.scss'
import './style/package.scss'
import { available_search, available_search_a, service_available, service_delavailable } from '../../axios/api1';
import { useNavigate } from 'react-router-dom';
function ServiceProject(props: any) {
    interface DataType {
        Availableid: React.Key;
        Availablename: string;
        Availablenum: string;
        Availabletype: string;
        Availableintroduce: string;
    }
    const res = () => {
        service_available().then((res) => {
            setservicesAvailable(res.data.available)
            console.log(res.data);
        })
    }
    useEffect(() => {
        res()
    }, [])
    const push = useNavigate()
    const { Search } = Input
    const [servicesAvailable, setservicesAvailable] = useState<DataType[]>([])
    // 定义下拉框的内容
    const options = [
        { value: '付费项目', label: '付费项目' },
        { value: '免费项目', label: '免费项目' },
    ]
    // 编辑事件
    const edit = (key: any) => {
        console.log(key);
        push({ pathname: '/MyLayout/EditProject', search: `key=${key}` })
    }
    // 删除事件
    const del = (key: any) => {
        service_delavailable({ key: key }).then((res) => {
            if (res.data.code == 200) {
                setservicesAvailable(res.data.available)
                message.open({
                    type: 'success',
                    content: '已删除'
                })
            }
        })
    }
    // 定义表格的每一行的数据格式
    const columns: ColumnsType<DataType> = [
        {
            title: '项目编号',
            dataIndex: 'Availableid',
        },
        {
            title: '项目名称',
            dataIndex: 'Availablename',
        },
        {
            title: '项目次数',
            dataIndex: 'Availablenum',
            align: 'center'
        },
        {
            title: '项目类型',
            dataIndex: 'Availabletype',
            align: 'center'
        },
        {
            title: '项目介绍',
            dataIndex: 'Availableintroduce',
            align: 'center'
        },
        {
            title: '操作',
            dataIndex: 'operation',
            render: (_, record: { Availableid: React.Key }) =>
                <>
                    <a onClick={() => edit(record.Availableid)}>编辑</a>&nbsp;&nbsp;&nbsp;
                    <Popconfirm title="Sure to delete?" onConfirm={() => del(record.Availableid)}>
                        <a>删除</a>
                    </Popconfirm>
                </>,
            align: 'center'
        },
    ];
    // 当下拉框数值改变时触发
    const handleChange = (value: any) => {
        if (value) {
            available_search({ Availabletype: value }).then((res) => {
                if (res.data.code == 200) {
                    setservicesAvailable(res.data.available)
                }
            })
        } else {
            res()
        }
    }
    const onSearch = (value: any) => {
        if (value) {
            available_search_a({ Availablevalue: value }).then((res) => {
                if (res.data.code == 200) {
                    setservicesAvailable(res.data.available)
                }
            })
        } else {
            res()
        }
    }
    const addproject = () => {
        push('/MyLayout/Project')
    }
    return (
        <div>
            {/* 标题部分 */}
            <h1 className='Headline'><span></span>服务项目管理</h1>
            {/* 下拉框部分 */}
            <div className="middle">
                <div className="select">
                    <span>项目类型</span>
                    <Select
                        allowClear
                        placeholder="请选择"
                        style={{ width: 200 }}
                        onChange={handleChange}
                        options={options}
                        size="large"
                    />
                    <Search allowClear placeholder="请输入关键字" style={{ width: 380 }} onSearch={onSearch} size="large" />
                </div>
                <Button type='primary' onClick={addproject}>新增服务项目</Button>
            </div>
            {/* 表格部分 */}
            <Table columns={columns} dataSource={servicesAvailable} />
        </div>
    );
}

export default ServiceProject;