import ExportJsonExcel from 'js-export-excel';
// 直接导出文件
const ExportToExcel = (data, title, dataIndex, name) => {
    var option = []
    let resdata = data
    let sheetfilter = dataIndex //对应列表数据中的key值数组，就是上面resdata中的 name，address
    let sheetheader = title//对应key值的表头，即excel表头
    option.fileName = name //导出的Excel文件名
    option.datas = [
        {
            sheetData: resdata,
            sheetName: name,  //工作表的名字
            sheetFilter: sheetfilter,
            sheetHeader: sheetheader,
        }
    ]
    var toExcel = new ExportJsonExcel(option);
    toExcel.saveExcel();
}
export default ExportToExcel