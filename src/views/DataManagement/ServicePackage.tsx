import React, { useState, useEffect } from 'react';
import { Select, Input, Button } from 'antd'
import './style/pub.scss'
import './style/package.scss'
import paimary from '../../assets/chuji.png'
import { service_package, service_search_a, service_search_b } from '../../axios/api1';
import { useNavigate } from 'react-router-dom';
const { Search } = Input

function ServicePackage(props: any) {
    const push = useNavigate()
    // 定义状态用来存储服务包列表
    const [servicePackage, setServicePackage] = useState([])
    const pack = () => {
        service_package().then((res) => {
            console.log(res.data.servicepackage)
            setServicePackage(res.data.servicepackage)
        })
    }
    // 获取服务包详情
    useEffect(() => {
        pack()
    }, [])

    const handleChange = (value: any) => {
        if (value) {
            service_search_a({ packagestatus: value }).then((res) => {
                if (res.data.code == 200) {
                    setServicePackage(res.data.servicepackage)
                }
            })
        } else {
            pack()
        }


    }
    const onSearch = (value: any) => {
        console.log(value);
        if (value) {
            service_search_b({ packagename: value }).then((res) => {
                if (res.data.code == 200) {
                    setServicePackage(res.data.servicepackage)
                }
            })
        } else {
            pack()
        }

    }
    const options = [
        { value: '1', label: '使用中' },
        { value: '2', label: '待审核' },
        { value: '3', label: '已停用' },
        { value: '4', label: '已驳回' },
    ]
    // 服务包详情
    const packDetail = (id: any) => {
        push({ pathname: '/MyLayout/PackageDetail', search: `id=${id}` })
    }
    const addPack = () => {
        push('/MyLayout/AddPack')
    }
    return (
        <div>
            {/* 标题部分 */}
            <h1 className='Headline'><span></span>服务包管理</h1>
            {/* 下拉框部分 */}
            <div className="middle">
                <div className="select">
                    <span>服务包状态</span>
                    <Select
                        allowClear
                        placeholder="请选择"
                        style={{ width: 200 }}
                        onChange={handleChange}
                        options={options}
                        size="large"
                    />
                    <Search allowClear placeholder="请输入关键字" style={{ width: 380 }} onSearch={onSearch} size="large" />
                </div>
                <Button type='primary' onClick={addPack}>新增服务包</Button>
            </div>
            <div className="chuji">
                {
                    // {/* 通过数组的map进行响应式的渲染页面 */}
                    servicePackage?.map((item: any) => {
                        return (
                            <div className="primary" key={item.packageid} onClick={() => packDetail(item.packageid)}>
                                <div className="basics">
                                    <img src={item.images} alt="" style={{ width: 136, height: 106 }} />
                                    <div className="right">
                                        <p>{item.packagename}</p>
                                        <p>服务对象：{item.packageobject}</p>
                                        <span>随访服务</span>
                                        <span>常规护理</span>
                                    </div>
                                    <div id="u6738-1" className='ax_default u6738' data-label="标签" style={{ visibility: 'inherit' }}>
                                        {/* <!-- Unnamed (矩形) --> */}
                                        <div id="u6738-1_state0" className="panel_state u6738_state0" data-label="State1" style={{ width: 80, height: 80, visibility: "inherit", position: 'absolute', top: 0, right: 0, overflow: 'clip' }}>
                                            <p className={` ${item.packagestatus == 1 ? 'tab1' : 'tab'}`}>使用中</p>
                                            <p className={` ${item.packagestatus == 2 ? 'tab2' : 'tab'}`}>待审核</p>
                                            <p className={` ${item.packagestatus == 3 ? 'tab3' : 'tab'}`}>已停用</p>
                                            <p className={` ${item.packagestatus == 4 ? 'tab4' : 'tab'}`}>已驳回</p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>

        </div >
    );
}

export default ServicePackage;