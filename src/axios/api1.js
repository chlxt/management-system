import service from "./service";
// 机构列表
export function orgin_List(params = {}) {
    return service.get('/orgin/list', { params })
}
// 修改机构状态
export function orgin_Setstatus(params = {}) {
    return service.post('/orgin/setstatus', params)
}
//获取编辑机构的信息
export function orgin_EditList(params = {}) {
    return service.post('/orgin/editlist', params)
}
// 编辑机构
export function orgin_Edit(params = {}) {
    return service.post('/orgin/edit', params)
}
// 删除机构
export function orgin_Del(params = {}) {
    return service.post('/orgin/dellist', params)
}
// 查询机构
export function orgin_search(params = {}) {
    return service.post('/orgin/search', params)
}
// 新增机构
export function orgin_add(params = {}) {
    return service.post('/orgin/add', params)
}
// 团队管理列表
export function team_List(params = {}) {
    return service.get('/team/list', { params })
}
export function jointeam_ist(params = {}) {
    return service.get('/jointeam/list', { params })
}
// 修改团队状态
export function team_Setsstatus(params = {}) {
    return service.post('/team/setStatus', params)
}
export function team_Setsstatus_a(params = {}) {
    return service.post('/team/setStatus_a', params)
}
// 新增团队
export function team_addteam(params = {}) {
    return service.post('/team/addteam', params)
}
// 申请加入团队
export function team_join(params = {}) {
    return service.post('/team/join', params)
}
// 通过下拉框搜索团队
export function team_search(params = {}) {
    return service.post('/team/search', params)
}
// 通过搜索框搜索
export function team_search_a(params = {}) {
    return service.post('/team/search_a', params)
}
// 通过编号搜索
export function team_search_b(params = {}) {
    return service.post('/team/search_b', params)
}
// 编辑团队
export function team_editteam(params = {}) {
    return service.post('/team/editteam', params)
}
// 退出队伍
export function team_exit(params = {}) {
    return service.post('/team/exitteam', params)
}
// 解散团队
export function team_dissolve(params = {}) {
    return service.post('/team/dissolve', params)
}
// 医师详情
export function team_doctor(params = {}) {
    return service.post('/team/doctor', params)
}
// 医生管理列表
export function doctor_List(params = {}) {
    return service.get('/doctor/list', { params })
}
// 修改医生状态
export function doctor_Setsstatus(params = {}) {
    return service.post('/doctor/setStatus', params)
}
// 查询服务包详情
export function team_Package(params = {}) {
    return service.post('/team/package', params)
}
// 移除服务包
export function team_yichuPackage(params = {}) {
    return service.post('/team/yichupackage', params)
}
// 获取服务包列表
export function service_package(params = {}) {
    return service.get('/service/package', { params })
}
// 新增服务包
export function service_addpackage(params = {}) {
    return service.post('/service/addpackage', params)
}
// 编辑服务包
export function service_editpackage(params = {}) {
    return service.post('/service/editpackage', params)
}
// 获取某一个服务包
export function service_packageDetail(params = {}) {
    return service.post('/service/packageDetail', params)
}
// 根据下拉框获取服务包列表
export function service_search_a(params = {}) {
    return service.post('/service/search_a', params)
}
// 根据input框搜索
export function service_search_b(params = {}) {
    return service.post('/service/search_b', params)
}
// 获取服务项目列表
export function service_available(params = {}) {
    return service.get('/service/servicesAvailable', { params })
}
// 根据编号查询服务项目
export function service_searchProject(params = {}) {
    return service.post('/service/searchProject', params)
}
// 新增服务项目
export function service_addavailable(params = {}) {
    return service.post('/available/project', params)
}
// 删除服务项目列表
export function service_delavailable(params = {}) {
    return service.post('/service/delAvailable', params)
}
// 搜索服务项目
export function available_search(params = {}) {
    return service.post('/available/search', params)
}
export function available_search_a(params = {}) {
    return service.post('/available/search_a', params)
}
// 修改服务包状态
export function package_setstatus(params = {}) {
    return service.post('/servicepackage/setstatus', params)
}
// 驳回
export function package_bohui(params = {}) {
    return service.post('/servicepackage/bohui', params)
}
// 医生管理
// 通过input搜索
export function doctor_search_a(params = {}) {
    return service.post('/doctor/search_a', params)
}
// 搜索具体是哪一个医生
export function doctor_doctorlit(params = {}) {
    return service.post('/doctor/doctorlist', params)
}
// 通过下拉框(所属机构)搜索
export function doctor_search(params = {}) {
    return service.post('/doctor/search', params)
}
// 通过下拉框(所属团队)搜索
export function doctor_search_b(params = {}) {
    return service.post('/doctor/search_b', params)
}
// 编辑医生信息
export function doctor_edit(params = {}) {
    return service.post('/doctor/edit', params)
}
// 新增医生
export function doctor_add(params = {}) {
    return service.post('/doctor/add', params)
}
// 删除医生
export function doctor_del(params = {}) {
    return service.post('/doctor/del', params)
}
// 新增服务包
export function team_addPackage(params = {}) {
    return service.post('/team/addPackage', params)
}
// 编辑服务项目
export function edit_project(params = {}) {
    return service.post('/service/editproject', params)
}