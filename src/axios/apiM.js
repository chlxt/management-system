import service from "./service";
// 标签管理
export function Tabel_list(params = {}) {
    return service.get('/TabelData/list', { params })
};
//居民管理数据列表获取
export function Bable_list(params = {}) {
    return service.get('/from/list', { params })
};
//修改居民管理状态
export function setBable_list(params = {}) {
    return service.post('/from/setstatus',  params )
};
//删除居民
export function delBable_list(params = {}) {
    return service.post('/from/delstatus', params)
};
//修改居民信息
export function editBable_list(params = {}) {
    return service.post('/from/change', params)
};
//获取居民信息

export function getBable_list(params = {}) {
    return service.post('/from/getlist', params)
};
//新增居民信息
export function addBable_list(params = {}) {
    return service.post('/from/adduser', params)
};
//根据下拉框查找
export function searchBable_list(params = {}) {
    return service.post('/from/search', params)
};
//echarts图表数据获取
export function echarts_list(params = {}){
    return service.get('/from/echarts',{params})
}