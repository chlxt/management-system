// 导入axios
import axios from 'axios';
// 全局请提示代码
import { message, Spin } from 'antd';

// 实例化axios实例对象
var service = axios.create({
    baseURL: '/api', //基准路径, 公共前缀, 
    timeout: 5 * 1000 //超时时间
})

// 设置请求拦截器
service.interceptors.request.use((config) => {
    return config;
}, (error) => {
    // 对请求错误做些什么
    return Promise.reject(error)
})

// 设置响应拦截器
service.interceptors.response.use((config) => {
    return config;
}, (error) => {
    //处理错误状态码
    if (error.response.status == 302) {
        message.open({ type: 'error', content: '接口重定向了！' });
    } else if (error.response.status == 400) {
        message.open({ type: 'error', content: '参数不正确!' });
    } else if (error.response.status == 401) {
        message.open({ type: 'error', content: '登陆过期,请重新登录!' });
    } else if (error.response.status == 403) {
        message.open({ type: 'error', content: '您没有权限操作!' });
    } else if (error.response.status == 404) {
        message.open({ type: 'error', content: '请求路径错误!' });
    } else if (error.response.status == 408) {
        message.open({ type: 'error', content: '请求超时!' });
    } else if (error.response.status == 409) {
        message.open({ type: 'error', content: '系统已存在相同数据!' });
    } else if (error.response.status == 500) {
        message.open({ type: 'error', content: '服务器内部错误!' });
    } else if (error.response.status == 501) {
        message.open({ type: 'error', content: '服务未实现!' });
    } else if (error.response.status == 502) {
        message.open({ type: 'error', content: '网关错误！' });
    } else if (error.response.status == 503) {
        message.open({ type: 'error', content: '服务不可用！' });
    } else if (error.response.status == 504) {
        message.open({ type: 'error', content: '服务暂时无法访问，请稍后再试！' });
    } else if (error.response.status == 505) {
        message.open({ type: 'error', content: 'HTTP版本不受支持!' });
    } else {
        message.open({ type: 'error', content: '异常问题，请联系管理员！' });
    }

    return Promise.reject(error)
})
export default service;