import service from "./service";
// 标签管理列表
export function Label_list(params = {}) {
    return service.get('/LabelData/list', { params })
}
// 修改标签状态
export function Label_revisestatus(params = {}) {
    return service.post('/LabelData/revisestatus', params)
}
// 新增标签
export function Label_add(params = {}) {
    return service.post('/LabelData/add', params)
}
// 修改标签名称
export function Label_edit(params = {}) {
    return service.post('/LabelData/edit', params)
}
// 编辑标签列表
export function Label_editlist(params = {}) {
    return service.post('/LabelData/editlist', params)
}
// 删除标签
export function Label_del(params = {}) {
    return service.post('/LabelData/del', params)
}

// 搜索标签关键词
export function Label_search(params = {}) {
    return service.post('/LabelData/search', params)
}

// ----------------------------
// 资讯管理列表
export function Infor_list(params = {}) {
    return service.get('/InforData/list', { params })
}
// 新增资讯管理
export function Infor_add(params = {}) {
    return service.post('/InforData/add', params)
}
// 删除资讯
export function Infor_del(params = {}) {
    return service.post('/InforData/del', params)
}
// 修改资讯
export function Infor_edit(params = {}) {
    return service.post('/InforData/edit', params)
}
// 编辑资讯列表
export function Infor_editlist(params = {}) {
    return service.post('/InforData/editlist', params)
}
// 搜索资讯关键词
export function Infor_search(params = {}) {
    return service.post('/InforData/search', params)
}

// --------------------------------
// 药品管理列表
export function Drug_list(params = {}) {
    return service.get('/DrugData/list', { params })
}
// 新增药品
export function Drug_add(params = {}) {
    return service.post('/DrugData/add', params)
}
// 删除药品
export function Drug_del(params = {}) {
    return service.post('/DrugData/del', params)
}
// 修改药品
export function Drug_edit(params = {}) {
    return service.post('/DrugData/edit', params)
}
// 编辑药品列表
export function Drug_editlist(params = {}) {
    return service.post('/DrugData/editlist', params)
}
// 搜索药品关键词
export function Drug_search(params = {}) {
    return service.post('/DrugData/search', params)
}

// --------------------------------
// 轮播图管理列表
export function Ban_list(params = {}) {
    return service.get('/BanData/list', { params })
}
// 修改轮播图状态
export function Ban_revisestatus(params = {}) {
    return service.post('/BanData/revisestatus', params)
}
// 新增轮播图
export function Ban_add(params = {}) {
    return service.post('/BanData/add', params)
}
// 删除轮播图
export function Ban_del(params = {}) {
    return service.post('/BanData/del', params)
}
// 修改轮播图
export function Ban_edit(params = {}) {
    return service.post('/BanData/edit', params)
}
// 编辑轮播图列表
export function Ban_editlist(params = {}) {
    return service.post('/BanData/editlist', params)
}
// 搜索轮播图关键词
export function Ban_search(params = {}) {
    return service.post('/BanData/search', params)
}