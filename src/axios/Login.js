import service from "./service";

// 登录接口
export function Login(params = {}) {
    return service.post('/user/login', params)
}
// 用户列表
export function User_list(params = {}) {
    return service.get('/user/list', { params })
}
// 注册接口(新增)
export function User_add(params = {}) {
    return service.post('/user/add', params)
}
// 修改密码接口
export function User_editpass(params = {}) {
    return service.post('/user/editpass', params)
}
// 修改管理员信息
export function User_edit(params = {}) {
    return service.post('/user/edit', params)
}
// 通过id获取管理员列表
export function User_editlist(params = {}) {
    return service.post('/user/editlist', params)
}
// 删除
export function User_del(params = {}) {
    return service.post('/user/del', params)
}
// 搜索
export function User_search(params = {}) {
    return service.post('/user/search', params)
}