import service from "./service";
// 待签约列表
export function PendingContractlist(params = {}) {
    return service.get('/PendingContract/list', { params })
}
//通过id查找待签约人员
export function seekPendingContract(params) {
    return service.post('/PendingContract/seek', params)
}
//修改签约人员信息
export function EditPendingContract(params) {
    return service.post('/PendingContract/Edit', params)
}
//查询待签约
export function checkPendingContract(params) {
    return service.post('/PendingContract/check', params)
}
//点击待签约审核通过
export function passPendingContract(params) {
    return service.post('/PendingContract/pass', params)
}
//点击待签约驳回
export function rejectPendingContract(params) {
    return service.post('/PendingContract/reject', params)
}
// 签约记录
export function SigningRecordList(params = {}) {
    return service.get('/SigningRecord/list', { params })
}
//通过id查找签约记录详情
export function seekSigningRecord(params) {
    return service.post('/SigningRecord/seek', params)
}
//修改签约记录信息
export function EditSigningRecord(params) {
    return service.post('/SigningRecord/Edit', params)
}
//查询待签约
export function checkSigningRecord(params) {
    return service.post('/SigningRecord/check', params)
}
// 服务记录列表
export function ServiceRecordList(params = {}) {
    return service.get('/ServiceRecord/list', { params })
}
//通过id查看服务记录详情
export function seekServiceRecord(params = {}) {
    return service.post('/ServiceRecord/seek', params)
}
// 待服务列表
export function PendingServiceList(params = {}) {
    return service.get('/PendingService/list', { params })
}
//查询待服务
export function checkPendingService(params) {
    return service.post('/PendingService/check', params)
}
// 新增服务
export function addServices(params) {
    return service.post('/PendingService/add', params)
}
//通过id查询待处理服务
export function seekPendingServices(params) {
    return service.post('/PendingService/seek', params)
}
// 修改待服务
export function EditPendingServices(params) {
    return service.post('/PendingService/Edit', params)
}
//点击待签约审核通过
export function passPendingServices(params) {
    return service.post('/PendingService/pass', params)
}
//点击待签约驳回
export function rejectPendingServices(params) {
    return service.post('/PendingService/reject', params)
}
//查询服务记录
export function checkServiceRecord(params) {
    return service.post('/ServiceRecord/check', params)
}