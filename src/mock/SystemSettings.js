import Mock from "mockjs"
var Random = Mock.Random

// 标签管理

// 解决mock数据每次随机问题：利用保存在本地实现
// 1.创建空对象保存数据
var LabelData = {}
// 2.判断本地有无数据
if (localStorage.getItem('labeldata')) {
    // 将获取的数据转为对象
    LabelData = JSON.parse(localStorage.getItem('labeldata'))
} else {
    // 没有获取随机数据，并进行本地存储
    LabelData = Mock.mock({
        'labeldata|150': [{
            id: '@id',
            tagname: "@pick(['高血压','冠心病','高血糖','高血脂','慢病护理','多动症'])",
            name: '@cname',
            time: '@datetime("yyyy/MM/dd HH:mm:ss")',
            status: "@boolean",
        }]
    })
    localStorage.setItem('labeldata', JSON.stringify(LabelData))
}

// 获取标签管理列表
Mock.mock('/api/LabelData/list', 'get', () => {
    return LabelData
})

// 修改标签管理使用状态
Mock.mock('/api/LabelData/revisestatus', 'post', (params) => {
    // 获取到要修改的那条数据，解构出来
    const { id } = JSON.parse(params.body)
    // console.log(id);
    // console.log(JSON.parse(params.body));
    // 找到对应的下标
    // console.log(LabelData)
    var index = LabelData.labeldata.findIndex(item => {
        return item.id == id
    })
    // console.log(index);
    // 如果存在，取反，并返回修改成功
    if (index != -1) {
        LabelData.labeldata[index].status = !LabelData.labeldata[index].status
        localStorage.setItem('labeldata', JSON.stringify(LabelData))
        return { code: 200, msg: '修改成功', labeldata: LabelData.labeldata }
    }
})
// 删除标签信息
Mock.mock('/api/LabelData/del', 'post', (params) => {
    const { id } = JSON.parse(params.body)
    console.log(id);
    var index = LabelData.labeldata.findIndex((item) => {
        return item.id == id
    })
    if (index != -1) {
        LabelData.labeldata.splice(index, 1)
        localStorage.setItem('labeldata', JSON.stringify(LabelData))
        return { code: 200, msg: "删除成功", labeldata: LabelData.labeldata }
    }
})

// 编辑标签信息
Mock.mock('/api/LabelData/edit', 'post', (params) => {
    const editlist = JSON.parse(params.body)
    console.log(editlist);
    const id = editlist.id
    var index = LabelData.labeldata.findIndex((item) => {
        return item.id == id
    })
    if (LabelData.labeldata[index].id == editlist.id &&
        LabelData.labeldata[index].tagname == editlist.tagname &&
        LabelData.labeldata[index].name == editlist.name &&
        LabelData.labeldata[index].time == editlist.time &&
        LabelData.labeldata[index].status == editlist.status) {
        return { code: '304', status: 'no', msg: '与原信息一致' }
    } else {
        LabelData.labeldata[index].id = editlist.id
        LabelData.labeldata[index].tagname = editlist.tagname
        LabelData.labeldata[index].name = editlist.name
        LabelData.labeldata[index].time = editlist.time
        LabelData.labeldata[index].status = editlist.status

        localStorage.setItem('labeldata', JSON.stringify(LabelData))
        return {
            code: '200',
            status: 'ok',
            labeldata: LabelData.labeldata
        }
    }
})
// 获取标签管理列表
Mock.mock('/api/LabelData/editlist', 'post', (params) => {
    var { id } = JSON.parse(params.body)
    console.log(id);
    var list = LabelData.labeldata.find((item) => {
        return item.id == id
    })
    return { code: 200, editlist: list }
})

// 新增标签
Mock.mock('/api/LabelData/add', 'post', (params) => {
    const labellist = JSON.parse(params.body)
    console.log(labellist);
    LabelData.labeldata.unshift(labellist)
    localStorage.setItem('labeldata', JSON.stringify(LabelData))
    return { code: 200, msg: '新增成功', labeldata: LabelData.labeldata }
})

// 搜索标签关键词
Mock.mock('/api/LabelData/search', 'post', (params) => {
    var { id } = JSON.parse(params.body)
    console.log(id);
    var index = LabelData.labeldata.findIndex((item) => {
        if (item.id == id) {
            return item.id == id
        } else if (item.tagname == id) {
            return item.tagname == id
        } else if (item.name == id) {
            return item.name == id
        } else {
            return null
        }
    })
    if (index != -1) {
        var search1 = []
        search1.push(LabelData.labeldata[index])
        console.log(search1);
        return {
            code: '200', status: 'ok', labeldata: search1
        }
    } else {
        return { code: '400', status: 'no' }
    }
})

// ----------------------------------------------
// 资讯管理
var InforData = {}
if (localStorage.getItem('infordata')) {
    InforData = JSON.parse(localStorage.getItem('infordata'))
} else {
    InforData = Mock.mock({
        'infordata|100': [{
            id: '@id',
            images: Random.image('100x100', '#fa746b', '#FFF', 'Infor'),
            title: '@ctitle',
            tclass: '@pick(["健康头条","慢病护理"])',
            name: '@pick(["李明明","王汉文","李民进"])',
            status: '@boolean',
            'number|100-600': 1,
            time: '@datetime("yyyy/MM/dd HH:mm:ss")',
            con: '@cparagraph'
        }]
    })
    localStorage.setItem('infordata', JSON.stringify(InforData))
}

// 获取咨讯管理列表
Mock.mock('/api/InforData/list', 'get', () => {
    return InforData
})

// 新增咨讯管理
Mock.mock('/api/InforData/add', 'post', (params) => {
    const inforlist = JSON.parse(params.body)
    console.log(inforlist);
    InforData.infordata.unshift(inforlist)
    localStorage.setItem('infordata', JSON.stringify(InforData))
    return { code: 200, msg: "新增成功", inforlist: InforData.infordata }
})
// 编辑咨讯(修改)
Mock.mock('/api/InforData/edit', 'post', (params) => {
    var editlist = JSON.parse(params.body)
    console.log(editlist);
    var id = editlist.id
    var index = InforData.infordata.findIndex((item) => {
        return item.id == id
    })
    if (InforData.infordata[index].id == editlist.id && InforData.infordata[index].images == editlist.images && InforData.infordata[index].title == editlist.title && InforData.infordata[index].tclass == editlist.tclass && InforData.infordata[index].status == editlist.status && InforData.infordata[index].con == editlist.con) {
        return { code: '304', status: 'no', msg: '与原信息一致' }
    } else {
        InforData.infordata[index].id = editlist.id
        InforData.infordata[index].images = editlist.images
        InforData.infordata[index].title = editlist.title
        InforData.infordata[index].tclass = editlist.tclass
        InforData.infordata[index].status = editlist.status
        InforData.infordata[index].con = editlist.con

        localStorage.setItem('infordata', JSON.stringify(InforData))
        return {
            code: '200',
            status: 'ok',
            inforlist: InforData.infordata
        }
    }
})
// 获取机构管理列表
Mock.mock('/api/InforData/editlist', 'post', (params) => {
    var { id } = JSON.parse(params.body)
    console.log(id);
    var list = InforData.infordata.find((item) => {
        return item.id == id
    })
    return { code: 200, editlist: list }
})
// 删除咨讯信息
Mock.mock('/api/InforData/del', 'post', (params) => {
    const { id } = JSON.parse(params.body)
    console.log(id);
    var index = InforData.infordata.findIndex((item) => {
        return item.id == id
    })
    if (index != -1) {
        InforData.infordata.splice(index, 1)
        localStorage.setItem('infordata', JSON.stringify(InforData))
        return { code: 200, msg: "删除成功", infordata: InforData.infordata }
    }
})
// 搜索咨询关键词
Mock.mock('/api/InforData/search', 'post', (params) => {
    var { id } = JSON.parse(params.body)
    console.log(id);
    var index = InforData.infordata.findIndex((item) => {
        if (item.id == id) {
            return item.id == id
        } else if (item.title == id) {
            return item.title == id
        } else {
            return null
        }
    })
    if (index != -1) {
        var search1 = []
        search1.push(InforData.infordata[index])
        console.log(search1);
        return {
            code: '200', status: 'ok', infordata: search1
        }
    } else {
        return { code: '400', status: 'no' }
    }
})

// --------------------------------------
// 药品管理
var DrugData = {}
if (localStorage.getItem('drugdata')) {
    DrugData = JSON.parse(localStorage.getItem('drugdata'))
} else {
    DrugData = Mock.mock({
        'drugdata|100': [{
            id: '@id',
            images: Random.image('100x100', '#fddb78', '#FFF', 'Drug'),
            title: '@pick(["阿司匹林","六味地黄丸","板蓝根","感康","布洛芬"])',
            tclass: '@pick(["非处方药","处方药"])',
            name: '@pick(["李明明","王汉文","李民进"])',
            status: '@boolean',
            'number|100-600': 1,
            time: '@datetime("yyyy/MM/dd HH:mm:ss")',
            con: '@cparagraph'
        }]
    })
    localStorage.setItem('drugdata', JSON.stringify(DrugData))
}

// 获取药品管理列表
Mock.mock('/api/DrugData/list', 'get', () => {
    return DrugData
})
// 编辑药品
Mock.mock('/api/DrugData/edit', 'post', (params) => {
    var editlist = JSON.parse(params.body)
    console.log(editlist);
    var id = editlist.id
    var index = DrugData.drugdata.findIndex((item) => {
        return item.id == id
    })
    if (DrugData.drugdata[index].id == editlist.id && DrugData.drugdata[index].images == editlist.images && DrugData.drugdata[index].title == editlist.title && DrugData.drugdata[index].tclass == editlist.tclass && DrugData.drugdata[index].status == editlist.status && DrugData.drugdata[index].name == editlist.name && DrugData.drugdata[index].number == editlist.number && DrugData.drugdata[index].time == editlist.time) {
        return { code: '304', status: 'no', msg: '与原信息一致' }
    } else {
        DrugData.drugdata[index].id = editlist.id
        DrugData.drugdata[index].images = editlist.images
        DrugData.drugdata[index].title = editlist.title
        DrugData.drugdata[index].tclass = editlist.tclass
        DrugData.drugdata[index].name = editlist.name
        DrugData.drugdata[index].status = editlist.status
        DrugData.drugdata[index].number = editlist.number
        DrugData.drugdata[index].time = editlist.time

        localStorage.setItem('drugdata', JSON.stringify(DrugData))
        return {
            code: '200',
            status: 'ok',
            druglist: DrugData.drugdata
        }
    }
})
// 获取药品管理列表
Mock.mock('/api/DrugData/editlist', 'post', (params) => {
    var { id } = JSON.parse(params.body)
    console.log(id);
    var list = DrugData.drugdata.find((item) => {
        return item.id == id
    })
    return { code: 200, editlist: list }
})
// 删除药品信息
Mock.mock('/api/DrugData/del', 'post', (params) => {
    const { id } = JSON.parse(params.body)
    console.log(id);
    var index = DrugData.drugdata.findIndex((item) => {
        return item.id == id
    })
    if (index != -1) {
        DrugData.drugdata.splice(index, 1)
        localStorage.setItem('drugdata', JSON.stringify(DrugData))
        return { code: 200, msg: "删除成功", drugdata: DrugData.drugdata }
    }
})
// 新增药品
Mock.mock('/api/DrugData/add', 'post', (params) => {
    const druglist = JSON.parse(params.body)
    console.log(druglist);
    DrugData.drugdata.unshift(druglist)
    localStorage.setItem('drugdata', JSON.stringify(DrugData))
    return { code: 200, msg: "新增成功", drugdata: DrugData.drugdata }
})
// 搜索药品关键词
Mock.mock('/api/DrugData/search', 'post', (params) => {
    var { id } = JSON.parse(params.body)
    console.log(id);
    var index = DrugData.drugdata.findIndex((item) => {
        if (item.id == id) {
            return item.id == id
        } else if (item.title == id) {
            return item.title == id
        } else {
            return null
        }
    })
    if (index != -1) {
        var search1 = []
        search1.push(DrugData.drugdata[index])
        console.log(search1);
        return {
            code: '200', status: 'ok', drugdata: search1
        }
    } else {
        return { code: '400', status: 'no' }
    }
})

// -------------------------------------
// 轮播图管理
var BanData = {}
if (localStorage.getItem('bandata')) {
    BanData = JSON.parse(localStorage.getItem('bandata'))
} else {
    BanData = Mock.mock({
        'bandata|4': [{
            id: '@id',
            images: Random.image('120x90', '#333', '#FFF', 'Ban'),
            banname: '@pick(["Banner"])',
            name: '@pick(["李明明","王汉文","李民进"])',
            time: '@datetime("yyyy/MM/dd HH:mm:ss")',
            status: "@boolean",
        }]
    })
    localStorage.setItem('bandata', JSON.stringify(BanData))
}

// 获取轮播图管理列表
Mock.mock('/api/BanData/list', 'get', () => {
    return BanData
})

// 修改轮播图管理使用状态
Mock.mock('/api/BanData/revisestatus', 'post', (params) => {
    const { id } = JSON.parse(params.body)
    console.log(id);
    // console.log(JSON.parse(params.body));
    // 找到对应的下标
    console.log(BanData)
    var index = BanData.bandata.findIndex(item => {
        return item.id == id
    })
    // console.log(index);
    // 如果存在，取反，并返回修改成功
    if (index != -1) {
        BanData.bandata[index].status = !BanData.bandata[index].status
        localStorage.setItem('bandata', JSON.stringify(BanData))
        return { code: 200, msg: '修改成功', bandata: BanData.bandata }
    }
})

// 编辑轮播图
Mock.mock('/api/BanData/edit', 'post', (params) => {
    var editlist = JSON.parse(params.body)
    console.log(editlist);
    var id = editlist.id
    var index = BanData.bandata.findIndex((item) => {
        return item.id == id
    })
    if (BanData.bandata[index].id == editlist.id && BanData.bandata[index].images == editlist.images && BanData.bandata[index].banname == editlist.banname && BanData.bandata[index].name == editlist.name && BanData.bandata[index].status == editlist.status && BanData.bandata[index].time == editlist.time) {
        return { code: '304', status: 'no', msg: '与原信息一致' }
    } else {
        BanData.bandata[index].id = editlist.id
        BanData.bandata[index].images = editlist.images
        BanData.bandata[index].banname = editlist.banname
        BanData.bandata[index].name = editlist.name
        BanData.bandata[index].status = editlist.status
        BanData.bandata[index].time = editlist.time

        localStorage.setItem('bandata', JSON.stringify(BanData))
        return {
            code: '200',
            status: 'ok',
            banlist: BanData.bandata
        }
    }
})
// 获取轮播图管理列表
Mock.mock('/api/BanData/editlist', 'post', (params) => {
    var { id } = JSON.parse(params.body)
    console.log(id);
    var list = BanData.bandata.find((item) => {
        return item.id == id
    })
    return { code: 200, editlist: list }
})

// 删除轮播图信息
Mock.mock('/api/BanData/del', 'post', (params) => {
    const { id } = JSON.parse(params.body)
    console.log(id);
    console.log(BanData)
    var index = BanData.bandata.findIndex(item => {
        return item.id == id
    })
    if (index != -1) {
        BanData.bandata.splice(index, 1)
        localStorage.setItem('bandata', JSON.stringify(BanData))
        return { code: 200, msg: '删除成功', bandata: BanData.bandata }
    }
})
// 新增轮播图
Mock.mock('/api/BanData/add', 'post', (params) => {
    const banlist = JSON.parse(params.body)
    console.log(banlist);
    BanData.bandata.unshift(banlist)
    localStorage.setItem('bandata', JSON.stringify(BanData))
    return { code: 200, msg: '新增成功', bandata: BanData.bandata }
})

// 搜索轮播图关键词
Mock.mock('/api/BanData/search', 'post', (params) => {
    var { id } = JSON.parse(params.body)
    console.log(id);
    var index = BanData.bandata.findIndex((item) => {
        if (item.id == id) {
            return item.id == id
        } else if (item.banname == id) {
            return item.banname == id
        } else if (item.name == id) {
            return item.name == id
        } else {
            return null
        }
    })
    if (index != -1) {
        var search1 = []
        search1.push(BanData.bandata[index])
        console.log(search1);
        return {
            code: '200', status: 'ok', bandata: search1
        }
    } else {
        return { code: '400', status: 'no' }
    }
})