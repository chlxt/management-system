import Mock from "mockjs";

// 模拟用户
var data = {};
var tokendata = {};
if (localStorage.getItem("user_info")) {
    data = JSON.parse(localStorage.getItem("user_info"));
} else {
    // 生成数据
    data = Mock.mock({
        "list|2": [
            {
                "id": '@increment()',
                'phone|13000000000-19000000000': 1,
                // phone: 'admin',
                name: '@cname',
                pass: 123456,
                // roles: ['/MyLayout/WorkBench', 'sign', '/MyLayout/PendingContract', '/MyLayout/SigningRecord', 'serve', '/MyLayout/PendingService', '/MyLayout/ServiceRecord', '/MyLayout/ResidentManagement', 'data', '/MyLayout/Organizational', '/MyLayout/Team', '/MyLayout/Doctor', '/MyLayout/ServicePackage', '/MyLayout/ServiceProject', 'stat', '/MyLayout/DataProfile', '/MyLayout/SigningPerformance', '/MyLayout/ServicePerformance', 'sys', '/MyLayout/LabelManagement', '/MyLayout/RoleManagement', '/MyLayout/InformationManagement', '/MyLayout/DrugManagement', '/MyLayout/BannerManagement']
                roles: []
            }
        ]
    })
    localStorage.setItem("user_info", JSON.stringify(data));
}
console.log(data.list);

// 获取用户列表
Mock.mock('/api/user/list', 'get', () => {
    console.log(data);
    return data
})

// 获取登录用户列表
Mock.mock('/api/user/login', "post", (req) => {
    var { phone, pass } = JSON.parse(req.body);
    console.log(phone, pass);
    if (phone != undefined && pass != undefined) {
        console.log("data.list", data.list);
        let index = data.list.findIndex((item) => {
            return item.phone == phone
        })
        if (index != -1) {
            if (data.list[index].pass == pass) {

                tokendata = Mock.mock({
                    'tokenarr': [
                        {
                            token: "@string(32)"
                        }
                    ]
                })
                return { code: 200, msg: "登录成功", token: tokendata.tokenarr[0].token, name: data.list[index].name, roles: data.list[index].roles };


            } else {
                return { code: 500, msg: "用户名或密码错误" };
            }

            
        }
        else {
            return { code: 501, msg: "用户不存在" };
        }
        console.log(pass.list);
       
    } else {
        return { code: 500, msg: "用户名或密码没有输入" };
    }
});

// 添加管理员
Mock.mock('/api/user/add', "post", (req) => {
    var { phone, pass, name, roles } = JSON.parse(req.body);
    console.log(phone, pass, name, roles);
    var index = data.list.findIndex((item) => {
        return item.phone == phone;
    });
    console.log(index);
    if (index == -1) {
        data.list.unshift({ id: data.list.length + 1, phone, pass, name, roles });
        localStorage.setItem("user_info", JSON.stringify(data));

        return { code: 200, msg: "添加成功", list: data.list };
    } else {
        return { code: 304, msg: "用户已存在" }
    }
})

// 修改密码接口
Mock.mock('/api/user/editpass', "post", (req) => {
    var { phone, pass } = JSON.parse(req.body);
    console.log(phone);
    console.log(pass);
    var index = data.list.findIndex((item) => {
        return phone == item.phone;
    });
    if (index != -1) {
        console.log(index);
        delete data.list[index].pass;
        console.log(data.list[index]);

        data.list[index].pass = pass;
        console.log(data.list[index]);
        localStorage.setItem("user_info", JSON.stringify(data));
        return { code: 200, msg: "修改密码成功", list: data.list };
    } else {
        return { code: 500, msg: "修改密码失败", list: data.list };
    }
});

// 编辑管理人员信息
Mock.mock('/api/user/edit', 'post', (params) => {
    var manlist = JSON.parse(params.body)
    console.log(manlist);
    var id = manlist.id
    var index = data.list.findIndex((item) => {
        return item.id == id
    })
    if (data.list[index].id == manlist.id && data.list[index].phone == manlist.phone && data.list[index].name == manlist.name && data.list[index].pass == manlist.pass && data.list[index].roles == manlist.roles) {
        return { code: '304', status: 'no', msg: '与原信息一致' }
    } else {
        data.list[index].id = manlist.id
        data.list[index].phone = manlist.phone
        data.list[index].name = manlist.name
        data.list[index].pass = manlist.pass
        data.list[index].roles = manlist.roles

        localStorage.setItem("user_info", JSON.stringify(data));
        return {
            code: '200',
            status: 'ok',
            list: data.list
        }
    }
})
// 获取管理员列表
Mock.mock('/api/user/editlist', 'post', (params) => {
    var { id } = JSON.parse(params.body)
    console.log(id);
    var list = data.list.find((item) => {
        return item.id == id
    })
    return { code: 200, editlist: list }
})
// 删除
Mock.mock('/api/user/del', 'post', (params) => {
    const { id } = JSON.parse(params.body)
    console.log(id);
    var index = data.list.findIndex(item => {
        return item.id == id
    })
    if (index != -1) {
        data.list.splice(index, 1)
        localStorage.setItem("user_info", JSON.stringify(data));
        return { code: 200, msg: '删除成功', list: data.list }
    }
})
// 搜索
Mock.mock('/api/user/search', 'post', (params) => {
    var { id } = JSON.parse(params.body)
    console.log(id);
    var index = data.list.findIndex((item) => {
        if (item.id == id) {
            return item.id == id
        } else if (item.phone == id) {
            return item.phone == id
        } else if (item.name == id) {
            return item.name == id
        } else {
            return null
        }
    })
    if (index != -1) {
        var search1 = []
        search1.push(data.list[index])
        console.log(search1);
        return {
            code: '200', status: 'ok', list: search1
        }
    } else {
        return { code: '400', status: 'no' }
    }
})