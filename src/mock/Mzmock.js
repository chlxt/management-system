import Mock from "mockjs"
import { localeData } from "moment"
var Random = Mock.Random

// 标签管理

// 解决mock数据每次随机问题：利用保存在本地实现
// 1.创建空对象保存数据
var TabelData = {}
// 2.判断本地有无数据
if (localStorage.getItem('tabeldata')) {
    // 将获取的数据转为对象
    TabelData = JSON.parse(localStorage.getItem('tabeldata'))
} else {
    // 没有获取随机数据，并进行本地存储
    TabelData = Mock.mock({
        'tabeldata|7': [{
            id: '@id',
            tagname: '@pick(["李明明团队","汪小敏团队","李军团队"])',
            'number1|100-600': 1,
            'number2|100-600': 1,
            'number3|100-600': 1,
            'number4|100-600': 1,
            'number5|100-600': 1,
            'number6|100-600': 1,
            'number7|100-600': 1,
        }]
    })
    localStorage.setItem('tabeldata', JSON.stringify(TabelData))
}
Mock.mock('/api/TabelData/list', 'get', () => {
    return TabelData
})
var BableData = {}
if (localStorage.getItem("babledata")) {
    BableData = JSON.parse(localStorage.getItem("babledata"))
} else {
    BableData = Mock.mock({
        'babledata|100': [{

            key: '@id',
            name: "@cname",
            'phone|13000000000-19999999999': 1,
            'car|620000000000000000-620999999999999999': 1,
            state: "@pick(['签约中','待签约','已签约'])",
            states: "@pick(['高血压','冠心病','高血糖'])",
            status: "@boolean",
            sex: "@pick(['男','女'])",
            'age|18-70': 1,
            '现居地': "@pick(['河南省南阳市','陕西省西安市','江苏省苏州市','甘肃省兰州市'])",
            jigou: "@pick(['罗西社区服务中心','天明社区服务中心','民进社区服务中心'])",
            team: "@pick(['李军团队','汪小敏团队','李明团队'])",
            time: "@pick(['2020/10/09 10:00','2021/3/13 7:00','2023/1/20 23:00'])",
            type: "@pick(['首次签约','第二次签约'])",
            doctor: "@pick(['李军','汪小敏','李明'])",
            service: "@pick(['基础包1','老年人服务包','慢性病护理包','基础包2','基础包3','基础包4','儿童护理包','高级特需包'])",
            money: "@pick(['120元','240元'])",
            zhouqi: "@pick(['一年','两年'])",

        }]
    })
    localStorage.setItem('babledata', JSON.stringify(BableData))
}
Mock.mock('/api/from/list', 'get', () => {
    return BableData
})

//获取用户列表
Mock.mock('/api/from/getlist', 'post', (req) => {
    var { key } = JSON.parse(req.body)
    var list = BableData.babledata.find((item) => {
        return item.key == key
    })
    return { code: 200, msg: '获取成功', babledata: list }
})

//修改用户状态
Mock.mock('/api/from/setstatus', 'post', (req) => {
    const { key } = JSON.parse(req.body)
    console.log(key)
    var index = BableData.babledata.findIndex((item) => {
        return item.key == key
    })
    if (index != -1) {
        BableData.babledata[index].status = !BableData.babledata[index].status;
        localStorage.setItem('babledata', JSON.stringify(BableData));
        return { code: 200, msg: '修改成功!', babledata: BableData.babledata }
    }
})
//删除用户
Mock.mock('/api/from/delstatus', 'post', (req) => {
    const { key } = JSON.parse(req.body)
    console.log(key)
    var index = BableData.babledata.findIndex((item) => {
        return item.id == key
    })
    if (index != -1) {
        BableData.babledata.splice(index, 1)
        localStorage.setItem('babledata', JSON.stringify(BableData));
        return { status: 200, msg: '删除成功!', babledata: BableData.babledata }
    }
})
//修改用户信息
Mock.mock('/api/from/change', 'post', (req) => {
    var changer = JSON.parse(req.body)

    console.log(changer);
    var key = changer.key
    var index = BableData.babledata.findIndex((item) => {
        return item.key == key
    })

    BableData.babledata[index] = changer
    localStorage.setItem("babledata", JSON.stringify(BableData))
    return { code: '200', status: "ok", list: BableData.babledata }

})
//新增用户信息
Mock.mock('/api/from/adduser', 'post', (req) => {
    var json = JSON.parse(req.body)
    BableData.babledata.unshift(json)
    localStorage.setItem('babledata', JSON.stringify(BableData))
    return { code: 200, msg: "新增成功", babledata: BableData.babledata }
})
//搜索用户信息

Mock.mock('/api/from/search', 'post', (req) => {

    var { key } = JSON.parse(req.body)
    var index = BableData.babledata.findIndex((item) => {

        if (item.key == key) {
            return item.key == key
        } else if (item.name == key) {
            return item.name == key
        } else if (item.car == key) {
            return item.car == key
        }
        else {
            return null
        }
        // return item.key==key/
    })
    console.log(index);
    if (index != -1) {
        var newsearch = []
        newsearch.push(BableData.babledata[index])
        console.log(newsearch);
        return { code: '200', status: 'ok', babledata: newsearch }
    } else {
        return { code: '400', status: 'no' }
    }



})
//每日签约数量统计
var ServeList = {}
if (localStorage.getItem('servelist')) {
    ServeList = JSON.parse(localStorage.getItem('servelist'))

} else {
    ServeList = Mock.mock({
        'servelist|20': [{
            'number|100-300': 1
        }]
    })
    localStorage.setItem('servelist', JSON.stringify(ServeList))
}
Mock.mock('/api/from/echarts', 'get', () => {
    var arr = []
    ServeList.servelist.forEach(item => {
        arr.push(item.number)

    });
    return arr
})