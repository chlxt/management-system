
import Mock from "mockjs"
var Random = Mock.Random

//待签约人员的数据
var PendingContract = {}
if (localStorage.getItem('PendingContract')) {
    PendingContract = JSON.parse(localStorage.getItem('PendingContract'))
} else {
    PendingContract = Mock.mock({
        "PendingContractl|100": [
            {
                status: '@id',
                "id|200000-500000": 1,
                name: '@cname',
                'phone|13000000000-19999999999': 1,
                state: "@pick(['待支付','待审核','已驳回'])",
                team: "@pick(['李军团队','汪小敏团队','李明团队'])",
                service: "@pick(['基础包1','老年人服务包','慢性病护理包','基础包2','基础包3','基础包4','儿童护理包','高级特需包'])",
                time: "@pick(['2020/10/09 10:00','2021/3/13 7:00','2023/1/20 23:00'])",
                jigou: "@pick(['罗西社区服务中心','天明社区服务中心','民进社区服务中心'])",
                doctor: "@pick(['李军','汪小敏','李明'])",
                zhouqi: '一年',
                type: "@pick(['首次签约','第二次签约'])",
                money: "@pick(['120元','240元'])",
                '现居地': "@pick(['河南省南阳市','陕西省西安市','江苏省苏州市','甘肃省兰州市'])",
                sex: "@pick(['男','女'])",
                'age|18-70': 1,
                fuwuService: "@pick(['高血压随访','糖尿病','心脏病'])",
                address: "@pick(['签约人家里','机构门诊'])"
            }
        ]
    })
    localStorage.setItem('PendingContract', JSON.stringify(PendingContract))
}
// 获取待签约用户列表
Mock.mock('/api/PendingContract/list', 'get', () => {
    return PendingContract
})
// 点击查询待签约
Mock.mock('/api/PendingContract/check', 'post', (res) => {
    var value = JSON.parse(res.body)
    //定义第一个不为空时的数组
    var list = PendingContract.PendingContractl.filter(item => {
        return item.state == value.id.v1
    });
    console.log(list);
    if (list.length != 0) {
        var arr = list.filter(item => item.jigou == value.id.v2)
        console.log(arr);
        if (arr.length != 0) {
            var str = arr.filter(item => item.team == value.id.v3)
            if (str.length != 0) {
                var obj = str.filter(item => item.service == value.id.v4)
                if (obj.length != 0) {
                    return { code: 200, checkPendingContractlist: obj }
                }
                return { code: 200, checkPendingContractlist: str }
            }
            return { code: 200, checkPendingContractlist: arr }
        } else {
            return { code: 200, checkPendingContractlist: list }
        }
    }
})
//修改待签约用户
Mock.mock('/api/PendingContract/Edit', 'post', (request) => {
    var value = JSON.parse(request.body)
    console.log(value.obj.status)

    var index = PendingContract.PendingContractl.findIndex(item => {
        return item.id == value.obj.id
    });
    console.log(index)
    if (PendingContract.PendingContractl[index] == value.obj) {
        return { code: '404', msg: '数据未修改' }
    } else {
        PendingContract.PendingContractl[index] = value.obj
        localStorage.setItem('PendingContract', JSON.stringify(PendingContract))
        return { code: '200', msg: '修改成功', PendingContract: PendingContract.PendingContractl }
    }
})
//通过id查找待签约用户查看详情
Mock.mock('/api/PendingContract/seek', 'post', (req) => {
    var { id } = JSON.parse(req.body)
    var list = PendingContract.PendingContractl.find((item) => {
        return item.id == id
    })
    return { code: 200, seekPendingContractlist: list }
})
//待签约详情的审核通过按钮
Mock.mock('/api/PendingContract/pass', 'post', (request) => {
    var value = JSON.parse(request.body)
    var index = PendingContract.PendingContractl.findIndex(item => {
        return item.id == value.id
    });
    console.log(value.id)
    console.log(index)
    if (PendingContract.PendingContractl[index] == value) {
        return { code: '404', msg: '数据未修改' }
    } else {
        PendingContract.PendingContractl[index].state = '待支付'
        localStorage.setItem('PendingContract', JSON.stringify(PendingContract))
        return { code: '200', msg: '修改成功', PendingContract: PendingContract.PendingContractl }
    }
})
//待签约详情的驳回按钮
Mock.mock('/api/PendingContract/reject', 'post', (request) => {
    var value = JSON.parse(request.body)
    var index = PendingContract.PendingContractl.findIndex(item => {
        return item.id == value.id
    });
    console.log(value.id)
    console.log(index)
    if (PendingContract.PendingContractl[index] == value) {
        return { code: '404', msg: '数据未修改' }
    } else {
        PendingContract.PendingContractl[index].state = '已驳回'
        localStorage.setItem('PendingContract', JSON.stringify(PendingContract))
        return { code: '200', msg: '修改成功', PendingContract: PendingContract.PendingContractl }
    }
})
// 签约记录用户列表
var SigningRecordList = {}
if (localStorage.getItem('SigningRecordList')) {
    SigningRecordList = JSON.parse(localStorage.getItem('SigningRecordList'))
} else {
    SigningRecordList = Mock.mock({
        "SigningRecordl|100": [
            {
                'status': '@id',
                "id|200000-500000": 1,
                name: '@cname',
                'phone|13000000000-19999999999': 1,
                state: "@pick(['李明','汪小敏','李军'])",
                team: "@pick(['李军团队','汪小敏团队','李明团队'])",
                service: "@pick(['基础包','老年人服务包','慢性病护理包'])",
                zhuang: "@pick(['生效中','已过期'])",
                time: "@pick(['2020/10/09 10:00','2021/3/13 7:00','2023/1/20 23:00'])",
                'age|18-70': 1,
                sex: "@pick(['男','女'])",
                zhouqi: "@pick(['一年','三年'])",
                money: "@pick(['120元','240元'])",
                type: "@pick(['首次签约','第二次签约'])",
                shenhe: "@pick(['汪峰','薛之谦'])",
                zhifufangshi: "@pick(['支付宝付款','微信付款','信用卡付款'])",
                '现居地': "@pick(['河南省南阳市','陕西省西安市','江苏省苏州市','甘肃省兰州市'])",
                jigou: "@pick(['罗西社区服务中心','天明社区服务中心','民进社区服务中心'])",
            }
        ]
    })
    localStorage.setItem('SigningRecordList', JSON.stringify(SigningRecordList))
}
// 获取签约记录列表
Mock.mock('/api/SigningRecord/list', 'get', () => {
    return SigningRecordList
})
// 通过id获取签约记录详情
Mock.mock('/api/SigningRecord/seek', 'post', (req) => {
    var { id } = JSON.parse(req.body)
    var list = SigningRecordList.SigningRecordl.find((item) => {
        return item.id == id
    })
    return { code: 200, seekSigningRecordlist: list }
})
//修改签约记录
Mock.mock('/api/SigningRecord/Edit', 'post', (request) => {
    var value = JSON.parse(request.body)
    console.log(value.obj.status)

    var index = SigningRecordList.SigningRecordl.findIndex(item => {
        return item.id == value.obj.id
    });
    console.log(index)
    if (SigningRecordList.SigningRecordl[index] == value.obj) {
        return { code: '404', msg: '数据未修改' }
    } else {
        SigningRecordList.SigningRecordl[index] = value.obj
        localStorage.setItem('SigningRecordList', JSON.stringify(SigningRecordList))
        return { code: '200', msg: '修改成功', PendingContract: SigningRecordList.SigningRecordl }
    }
})
//点击查询签约记录
Mock.mock('/api/SigningRecord/check', 'post', (res) => {
    var value = JSON.parse(res.body)
    //定义第一个不为空时的数组
    var list = SigningRecordList.SigningRecordl.filter(item => {
        return item.zhuang == value.id.v1
    });
    console.log(list);
    if (list.length != 0) {
        var arr = list.filter(item => item.jigou == value.id.v2)
        console.log(arr);
        if (arr.length != 0) {
            var str = arr.filter(item => item.team == value.id.v3)
            if (str.length != 0) {
                var obj = str.filter(item => item.service == value.id.v4)
                if (obj.length != 0) {
                    return { code: 200, checkSigningRecordlist: obj }
                }
                return { code: 200, checkSigningRecordlist: str }
            }
            return { code: 200, checkSigningRecordlist: arr }
        } else {
            return { code: 200, checkSigningRecordlist: list }
        }
    }
})

// 待处理服务数据
var PendingService = {}
if (localStorage.getItem('PendingService')) {
    PendingService = JSON.parse(localStorage.getItem('PendingService'))
} else {
    PendingService = Mock.mock({
        "PendingServicel|100": [
            {
                '身份证号': '@id',
                'id|1-8999': 1,
                "服务编号|200000-500000": 1,
                '姓名': '@cname',
                '手机号|13000000000-19999999999': 1,
                state: "@pick(['待审核','待服务','已驳回'])",
                '服务项目': "@pick(['高血压随访','糖尿病','心脏病'])",
                '服务包': "@pick(['基础包','老年人服务包','慢性病护理包'])",
                time: "@pick(['2020/10/09 10:00','2021/3/13 7:00','2023/1/20 23:00'])",
                '服务机构': "@pick(['罗西社区服务中心','天明社区服务中心','民进社区服务中心'])",
                '现居地': "@pick(['河南省南阳市','陕西省西安市','江苏省苏州市','甘肃省兰州市'])",
                '服务团队': "@pick(['李明团队','李军团队','汪小敏团队'])",
                '服务医生': "@pick(['李明','李军','汪小敏'])",
                '服务包': "@pick(['基础包','老年人服务包','慢性病护理包'])",
                'age|18-70': 1,
                '性别': "@pick(['男','女'])",
                '服务地点': "@pick(['签约人家里','机构门诊'])"

            }
        ]
    })
    localStorage.setItem('PendingService', JSON.stringify(PendingService))
}
// 获取待处理服务列表
Mock.mock('/api/PendingService/list', 'get', () => {
    return PendingService
})
//新增服务
Mock.mock('/api/PendingService/add', 'post', (request) => {
    let data = JSON.parse(request.body)
    PendingService.PendingServicel.unshift(data)
    localStorage.setItem('PendingService', JSON.stringify(PendingService))
    return PendingService.PendingServicel
})
// 通过id获取待处理服务的详情
Mock.mock('/api/PendingService/seek', 'post', (req) => {
    var { id } = JSON.parse(req.body)
    var list = PendingService.PendingServicel.find((item) => {
        return item.id == id
    })
    return { code: 200, seeklist: list }
})
//修改待服务详情
Mock.mock('/api/PendingService/Edit', 'post', (request) => {
    var value = JSON.parse(request.body)
    var index = PendingService.PendingServicel.findIndex(item => {
        return item.id == value.obj.id
    });
    console.log(index)
    if (PendingService.PendingServicel[index] == value.obj) {
        return { code: '404', msg: '数据未修改' }
    } else {
        PendingService.PendingServicel[index] = value.obj
        localStorage.setItem('PendingService', JSON.stringify(PendingService))
        return { code: '200', msg: '修改成功', PendingService: PendingService.PendingServicel }
    }
})
//查询待服务
Mock.mock('/api/PendingService/check', 'post', (res) => {
    var value = JSON.parse(res.body)
    //定义第一个不为空时的数组
    var list = PendingService.PendingServicel.filter(item => {
        return item.state == value.id.v1
    });
    console.log(list);
    if (list.length != 0) {
        var arr = list.filter(item => item.服务机构 == value.id.v2)
        console.log(arr);
        if (arr.length != 0) {
            var str = arr.filter(item => item.服务团队 == value.id.v3)
            if (str.length != 0) {
                var obj = str.filter(item => item.服务包 == value.id.v4)
                if (obj.length != 0) {
                    return { code: 200, checkPendingServicelist: obj }
                }
                return { code: 200, checkPendingServicelist: str }
            }
            return { code: 200, checkPendingServicelist: arr }
        } else {
            return { code: 200, checkPendingServicelist: list }
        }
    }
})

//待服务详情的审核通过按钮
Mock.mock('/api/PendingService/pass', 'post', (request) => {
    var value = JSON.parse(request.body)
    var index = PendingService.PendingServicel.findIndex(item => {
        return item.id == value.id
    });
    console.log(value.id)
    console.log(index)
    if (PendingService.PendingServicel[index] == value) {
        return { code: '404', msg: '数据未修改' }
    } else {
        PendingService.PendingServicel[index].state = '待服务'
        localStorage.setItem('PendingService', JSON.stringify(PendingService))
        return { code: '200', msg: '修改成功', PendingService: PendingService.PendingServicel }
    }
})
//待服务详情的驳回按钮
Mock.mock('/api/PendingService/reject', 'post', (request) => {
    var value = JSON.parse(request.body)
    var index = PendingService.PendingServicel.findIndex(item => {
        return item.id == value.id
    });
    console.log(value.id)
    console.log(index)
    if (PendingService.PendingServicel[index] == value) {
        return { code: '404', msg: '数据未修改' }
    } else {
        PendingService.PendingServicel[index].state = '已驳回'
        localStorage.setItem('PendingService', JSON.stringify(PendingService))
        return { code: '200', msg: '修改成功', PendingService: PendingService.PendingServicel }
    }
})
//服务记录的数据 
var ServiceRecord = {}
if (localStorage.getItem('ServiceRecord')) {
    ServiceRecord = JSON.parse(localStorage.getItem('ServiceRecord'))
} else {
    ServiceRecord = Mock.mock({
        "ServiceRecordl|100": [
            {
                status: '@id',
                "id|200000-500000": 1,
                name: '@cname',
                'phone|13000000000-19999999999': 1,
                state: "@pick(['李明','汪小敏','李军'])",
                team: "@pick(['高血压随访','低血糖','心脏病'])",
                service: "@pick(['基础包','老年人服务包','慢性病护理包'])",
                zhuang: "@pick(['已完成','已取消'])",
                time: "@pick(['2020/10/09 10:00','2021/3/13 7:00','2023/1/20 23:00'])",
                sex: "@pick(['男','女'])",
                "age|18-70": 1,
                '现居地': "@pick(['河南省南阳市','陕西省西安市','江苏省苏州市','甘肃省兰州市'])",
                serviceadrs: "@pick(['签约人人家里','机构门诊'])",
                laiyuan: "居民申请",
                tuandui: "@pick(['李明团队','汪小敏团队','李军团队'])",
                '服务机构': "@pick(['罗西社区服务中心','天明社区服务中心','民进社区服务中心'])",
            }
        ]
    })
    localStorage.setItem('ServiceRecord', JSON.stringify(ServiceRecord))
}
// 获取服务记录列表
Mock.mock('/api/ServiceRecord/list', 'get', () => {
    return ServiceRecord
})
// 通过id查询服务列表的详情
Mock.mock('/api/ServiceRecord/seek', 'post', (req) => {
    var { id } = JSON.parse(req.body)
    const list = ServiceRecord.ServiceRecordl.find(item => {
        return item.id == id
    })
    return { code: '200', msg: '成功', seekServiceRecord: list }
})
//查询服务记录
Mock.mock('/api/ServiceRecord/check', 'post', (res) => {
    var value = JSON.parse(res.body)
    //定义第一个不为空时的数组
    var list = ServiceRecord.ServiceRecordl.filter(item => {
        return item.zhuang == value.id.v1
    });
    console.log(list);
    if (list.length != 0) {
        var arr = list.filter(item => item.服务机构 == value.id.v2)
        console.log(arr);
        if (arr.length != 0) {
            var str = arr.filter(item => item.tuandui == value.id.v3)
            if (str.length != 0) {
                var obj = str.filter(item => item.service == value.id.v4)
                if (obj.length != 0) {
                    return { code: 200, checkServiceRecordlist: obj }
                }
                return { code: 200, checkServiceRecordlist: str }
            }
            return { code: 200, checkServiceRecordlist: arr }
        } else {
            return { code: 200, checkServiceRecordlist: list }
        }
    }
})