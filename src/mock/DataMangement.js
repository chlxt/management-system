// 使用 Mock
// var Mock = require('mockjs')
import Mock from 'mockjs'
var Random = Mock.Random
var aa = Mock.mock('@county(true)')
// 创建空对象用来保存数据(模拟机构管理数据)
var OrginData = {}
// 判断本地是否有该数据
if (localStorage.getItem("orgin_list")) {
    OrginData = JSON.parse(localStorage.getItem("orgin_list"))
} else {
    OrginData = Mock.mock({
        // mock配置机构管理数据
        "orginlist|150": [
            {
                key: '@id',
                images: Random.image(),
                title: aa + '卫生服务站',
                name: "@cname",
                'phone|13000000000-19999999999': 1,
                address: aa,
                status: "@boolean",
                arr: ['aa', 'bb', 'cc']
            }
        ]
    })
    // 进行本地持久化存储
    localStorage.setItem("orgin_list", JSON.stringify(OrginData))
}

//机构管理列表
Mock.mock('/api/orgin/list', 'get', () => {
    return OrginData
})
// 修改机构状态
Mock.mock('/api/orgin/setstatus', 'post', (req) => {
    const { key } = JSON.parse(req.body)
    console.log(key)
    var index = OrginData.orginlist.findIndex((item) => {
        return item.key == key
    })
    if (index != -1) {
        OrginData.orginlist[index].status = !OrginData.orginlist[index].status;
        localStorage.setItem('orgin_list', JSON.stringify(OrginData));
        return { code: 200, msg: '修改成功!', orginlist: OrginData.orginlist, status: OrginData.orginlist[index].status }
    }
})

// 获取机构管理列表
Mock.mock('/api/orgin/editlist', 'post', (req) => {
    var { key } = JSON.parse(req.body)
    console.log(key);
    var list = OrginData.orginlist.find((item) => {
        return item.key == key
    })
    return { code: 200, editlist: list }
})
// 删除机构列表
Mock.mock('/api/orgin/dellist', 'post', (req) => {
    var { key } = JSON.parse(req.body)
    var index = OrginData.orginlist.findIndex((item) => {
        return item.key == key
    })
    if (index != -1) {
        OrginData.orginlist.splice(index, 1)
        localStorage.setItem('orgin_list', JSON.stringify(OrginData))
        return { code: 200, msg: "删除成功", orginlist: OrginData.orginlist }
    }
})
// 修改机构信息
Mock.mock('/api/orgin/edit', 'post', (req) => {
    var editorder = JSON.parse(req.body)
    console.log(editorder)
    var key = editorder.key
    var index = OrginData.orginlist.findIndex((item) => {
        return item.key == key
    })
    console.log(index)
    if (OrginData.orginlist[index].name == editorder.name && OrginData.orginlist[index].phone == editorder.phone && OrginData.orginlist[index].address == editorder.address && OrginData.orginlist[index].title == editorder.title) {
        console.log(111)
        return { code: '304', status: 'no', msg: '与原机构一致' }
    } else {
        OrginData.orginlist[index].name = editorder.name
        OrginData.orginlist[index].phone = editorder.phone
        OrginData.orginlist[index].address = editorder.address
        OrginData.orginlist[index].title = editorder.title
        localStorage.setItem('orgin_list', JSON.stringify(OrginData))
        return {
            code: '200',
            status: 'ok',
            list: OrginData.orginlist
        }
    }
})
// 新增机构列表
Mock.mock('/api/orgin/add', 'post', (req) => {
    var json = JSON.parse(req.body)
    console.log(json)
    OrginData.orginlist.push(json)
    localStorage.setItem('orgin_list', JSON.stringify(OrginData))
    return { code: 200, msg: '新增成功', orginlist: OrginData.orginlist }
})

// 查询机构信息
Mock.mock('/api/orgin/search', 'post', (req) => {
    var { key } = JSON.parse(req.body)

    var index = OrginData.orginlist.findIndex((item) => {
        return item.key == key
    })
    if (index != -1) {
        var neworder = []
        neworder.push(OrginData.orginlist[index])
        // console.log(neworder)neworder
        return { code: '200', status: 'ok', orginlist: neworder }

    } else {
        return { code: '400', status: 'no' }
    }
})
// 模拟我管理的团队数据
var TeamList = {}
if (localStorage.getItem('team_list')) {
    TeamList = JSON.parse(localStorage.getItem('team_list'))
} else {
    TeamList = Mock.mock({
        "teamlist|100": [
            {
                key: '@id',
                images: Random.image('100x100', '#50B347', '#FFF', 'Head'),
                captain: "@pick(['李军','汪小敏','李明'])",
                name: function () {
                    let captain = this.captain
                    return captain + '团队'
                },
                "title|1-3": {
                    "01": "高血压",
                    "02": "糖尿病",
                    "03": "高血糖",
                    "04": "冠心病",
                    "05": "肺结核",
                    "06": "肾结石",
                    "07": "胆囊炎",
                    "08": "低血糖",
                },
                orgin: "@pick(['陕西省西安市雁塔区卫生服务站','陕西省西安市长安区卫生服务站','陕西省西安市未央区卫生服务站','陕西省西安市新城区卫生服务站','陕西省西安市灞桥区卫生服务站'])",
                "person|250-500": 1,
                'score|3-5': 1,
                time: "@datetime",
                status: "@boolean",
                // 团队成员
                "teamMember|4": [
                    {
                        key: '@id',
                        sex: "@pick(['男','女'])",
                        images: Random.image('100x100', '#50B347', '#FFF', 'Head'),
                        name: "@cname",
                        'phone|13000000000-19999999999': 1,
                        role: "@pick(['全科医师','主任医师','儿科医师','副主任医师'])",
                        team: "@pick(['李军团队','汪小敏团队','李明团队'])",
                        orgin: "@pick(['陕西省西安市雁塔区卫生服务站','陕西省西安市长安区卫生服务站','陕西省西安市未央区卫生服务站','陕西省西安市新城区卫生服务站','陕西省西安市灞桥区卫生服务站'])",
                        status: "@boolean",
                        doctorbreif: "1、北京大学第一医院内分泌科专家，从事内分泌相关疾病35余年，尤其擅长代谢性疾病引起的不孕不育诊疗，现任北京天伦医院不孕不育妇科特聘专家。2、1979—1984年就读于北京医学院医学系，随后在北京医科大学第一医院(现北京大学第一医院)内分泌科读研究生，毕业后留院工作至今。",
                        doctoradept: "甲状腺疾病及内分泌疾病，擅长甲亢、甲减、桥本病、糖尿病、多囊卵巢、代谢异常、甲状腺炎、甲状腺低下、甲状腺功能减退、甲状腺功能失调、甲状腺激素异常等引起的不孕不育症的治疗和孕期管理、生活指导，在治疗各种代谢内分泌异常方面有丰富的临床经验。",
                        "person|30-60": 1,
                        'score|3-5': 1,
                    }
                ],
                // 服务包管理
                "servicePackage|3": [
                    {
                        packageid: "@id",
                        packagename: "@pick(['基础包','老人服务包','儿童服务包','定制包'])",
                        images: function () {
                            let packagename = this.packagename
                            if (packagename == '基础包') {
                                return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%96%B0%E5%A2%9E%E5%9B%A2%E9%98%9F%E4%BF%A1%E6%81%AF/u5584.png'
                            } else if (packagename == '老人服务包') {
                                return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%96%B0%E5%A2%9E%E5%9B%A2%E9%98%9F%E4%BF%A1%E6%81%AF/u5793.png'
                            } else if (packagename == '儿童服务包') {
                                return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%9C%8D%E5%8A%A1%E5%8C%85%E7%AE%A1%E7%90%86/u6763.png'
                            } else {
                                return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%9C%8D%E5%8A%A1%E5%8C%85%E7%AE%A1%E7%90%86/u6793.svg'
                            }
                        },
                        "packagestatus|1-4": 1,
                        packageobject: function () {
                            let packagename = this.packagename
                            if (packagename == '基础包') {
                                return '所有人'
                            } else if (packagename == '老人服务包') {
                                return '55岁以上老人'
                            } else if (packagename == '儿童服务包') {
                                return '0-6岁的儿童'
                            } else {
                                return '定制的专属对象'
                            }
                        },
                        "title|1-3": {
                            "01": "高血压",
                            "02": "糖尿病",
                            "03": "高血糖",
                            "04": "冠心病",
                            "05": "肺结核",
                            "06": "高血糖",
                            "07": "肾结石",
                            "08": "胆囊炎",
                            "09": "低血糖",
                        },
                        "period|1-5": 1,
                        introduce: "主要以基本医疗服务和公共卫生服务为主，如部分常见病或 多发病的治疗和用药指导、重症的就医指导和转诊预约、居民健康档案的管理和 慢病管理指导等。",
                        "servicesAvailable|3": [
                            {
                                Availableid: "@id",
                                Availablename: "高血压随访服务",
                                "Availablenum|1-5": 1,
                                Availabletype: "@pick(['免费项目','付费项目'])",
                                Availableintroduce: "1.血压测量及记录 2.根据结果提供综合性健康指导"
                            }
                        ]
                    }
                ]
            }
        ]
    })
    localStorage.setItem('team_list', JSON.stringify(TeamList))
}
var doctorlist = TeamList.teamlist.map(item => {
    return item
});
// 模拟我加入的团队数据
var joinTeamList = {}
if (localStorage.getItem('team_list')) {
    joinTeamList = JSON.parse(localStorage.getItem('team_list'))
} else {
    joinTeamList = Mock.mock({
        "jointeamlist|30": [
            {
                key: '@id',
                images: Random.image('100x100', '#50B347', '#FFF', 'Head'),
                captain: "@pick(['李军','汪小敏','李明'])",
                name: function () {
                    let captain = this.captain
                    return captain + '团队'
                },
                "title|1-3": {
                    "01": "高血压",
                    "02": "糖尿病",
                    "03": "高血糖",
                    "04": "冠心病",
                    "05": "肺结核",
                    "06": "肾结石",
                    "07": "胆囊炎",
                    "08": "低血糖",
                },
                orgin: "@pick(['陕西省西安市雁塔区卫生服务站','陕西省西安市长安区卫生服务站','陕西省西安市未央区卫生服务站','陕西省西安市新城区卫生服务站','陕西省西安市灞桥区卫生服务站'])",
                "person|250-500": 1,
                'score|3-5': 1,
                time: "@datetime",
                status: "@boolean",
                // 团队成员
                "teamMember|4": [
                    {
                        key: '@id',
                        sex: "@pick(['男','女'])",
                        images: Random.image('100x100', '#50B347', '#FFF', 'Head'),
                        name: "@cname",
                        'phone|13000000000-19999999999': 1,
                        role: "@pick(['全科医师','主任医师','儿科医师','副主任医师'])",
                        team: "@pick(['李军团队','汪小敏团队','李明团队'])",
                        orgin: "@pick(['陕西省西安市雁塔区卫生服务站','陕西省西安市长安区卫生服务站','陕西省西安市未央区卫生服务站','陕西省西安市新城区卫生服务站','陕西省西安市灞桥区卫生服务站'])",
                        status: "@boolean",
                        doctorbreif: "1、北京大学第一医院内分泌科专家，从事内分泌相关疾病35余年，尤其擅长代谢性疾病引起的不孕不育诊疗，现任北京天伦医院不孕不育妇科特聘专家。",
                        doctoradept: "甲状腺疾病及内分泌疾病，擅长甲亢、甲减、桥本病、糖尿病、多囊卵巢、代谢异常、甲状腺炎、甲状腺低下、甲状腺功能减退、甲状腺功能失调、甲状腺激素异常等引起的不孕不育症的治疗和孕期管理、生活指导，在治疗各种代谢内分泌异常方面有丰富的临床经验。",
                        "person|30-60": 1,
                        'score|3-5': 1,
                    }
                ],
                // 服务包管理
                "servicePackage|3": [
                    {
                        packageid: "@id",
                        packagename: "@pick(['基础包','老人服务包','儿童服务包','定制包'])",
                        images: function () {
                            let packagename = this.packagename
                            if (packagename == '基础包') {
                                return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%96%B0%E5%A2%9E%E5%9B%A2%E9%98%9F%E4%BF%A1%E6%81%AF/u5584.png'
                            } else if (packagename == '老人服务包') {
                                return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%96%B0%E5%A2%9E%E5%9B%A2%E9%98%9F%E4%BF%A1%E6%81%AF/u5793.png'
                            } else if (packagename == '儿童服务包') {
                                return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%9C%8D%E5%8A%A1%E5%8C%85%E7%AE%A1%E7%90%86/u6763.png'
                            } else {
                                return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%9C%8D%E5%8A%A1%E5%8C%85%E7%AE%A1%E7%90%86/u6793.svg'
                            }
                        },
                        "packagestatus|1-4": 1,
                        packageobject: function () {
                            let packagename = this.packagename
                            if (packagename == '基础包') {
                                return '所有人'
                            } else if (packagename == '老人服务包') {
                                return '55岁以上老人'
                            } else if (packagename == '儿童服务包') {
                                return '0-6岁的儿童'
                            } else {
                                return '定制的专属对象'
                            }
                        },
                        "title|1-3": {
                            "01": "高血压",
                            "02": "糖尿病",
                            "03": "高血糖",
                            "04": "冠心病",
                            "05": "肺结核",
                            "06": "高血糖",
                            "07": "肾结石",
                            "08": "胆囊炎",
                            "09": "低血糖",
                        },
                        "period|1-5": 1,
                        introduce: "主要以基本医疗服务和公共卫生服务为主，如部分常见病或 多发病的治疗和用药指导、重症的就医指导和转诊预约、居民健康档案的管理和 慢病管理指导等。",
                        "servicesAvailable|3": [
                            {
                                Availableid: "@id",
                                Availablename: "高血压随访服务",
                                "Availablenum|1-5": 1,
                                Availabletype: "@pick(['免费项目','付费项目'])",
                                Availableintroduce: "1.血压测量及记录 2.根据结果提供综合性健康指导"
                            }
                        ]
                    }
                ]
            }
        ]
    })
    localStorage.setItem('team_list', JSON.stringify(joinTeamList))
}
// 加入的队伍请求列表
Mock.mock('/api/jointeam/list', 'get', () => {
    return joinTeamList
})
//团队管理列表的请求接口
Mock.mock('/api/team/list', 'get', () => {
    return TeamList
})
// 新增团队
Mock.mock('/api/team/addteam', 'post', (req) => {
    var json = JSON.parse(req.body)
    console.log(json)
    TeamList.teamlist.push(json)
    localStorage.setItem('team_list', JSON.stringify(TeamList))
    return { code: 200, msg: '新增成功', teamlist: TeamList.teamlist }
})
// 编辑团队
Mock.mock('/api/team/editteam', 'post', (req) => {
    var editTeam = JSON.parse(req.body)
    console.log(editTeam)
    var key = editTeam.key
    var index = TeamList.teamlist.findIndex((item) => {
        return item.key == key
    })
    console.log(index)
    if (TeamList.teamlist[index].name == editTeam.name && TeamList.teamlist[index].captain == editTeam.captain && TeamList.teamlist[index].orgin == editTeam.orgin) {
        console.log(111)
        return { code: '304', status: 'no', msg: '与原来一致' }
    } else {
        TeamList.teamlist[index].name = editTeam.name
        TeamList.teamlist[index].captain = editTeam.captain
        TeamList.teamlist[index].orgin = editTeam.orgin
        localStorage.setItem('team_list', JSON.stringify(TeamList))
        return {
            code: '200',
            status: 'ok',
            list: TeamList.teamlist
        }
    }
})
// 修改团队管理的状态
Mock.mock('/api/team/setStatus', 'post', (req) => {
    const { key } = JSON.parse(req.body)
    var index = TeamList.teamlist.findIndex(item => {
        return item.key == key
    })
    if (index != -1) {
        TeamList.teamlist[index].status = !TeamList.teamlist[index].status
        localStorage.setItem('team_list', JSON.stringify(TeamList))
        return { code: 200, msg: '修改成功', teamlist: TeamList.teamlist, status: TeamList.teamlist[index].status }
    }
})
// 单独修改团队状态
Mock.mock('/api/team/setStatus_a', 'post', (req) => {
    const { key } = JSON.parse(req.body)
    var index = TeamList.teamlist.findIndex(item => {
        return item.key == key
    })
    if (index != -1) {
        TeamList.teamlist[index].status = !TeamList.teamlist[index].status
        localStorage.setItem('team_list', JSON.stringify(TeamList))
        return { code: 200, msg: '修改成功', teamlist: TeamList.teamlist[index], status: TeamList.teamlist[index].status }
    }
})
// 解散团队列表
Mock.mock('/api/team/dissolve', 'post', (req) => {
    var { key } = JSON.parse(req.body)
    var index = TeamList.teamlist.findIndex((item) => {
        return item.key == key
    })
    if (index != -1) {
        TeamList.teamlist.splice(index, 1)
        localStorage.setItem('team_list', JSON.stringify(TeamList))
        return { code: 200, msg: "已解散", teamlist: TeamList.teamlist }
    }
})
// 查询医师详情
Mock.mock('/api/team/doctor', 'post', (req) => {
    var { teamid, doctorid } = JSON.parse(req.body)
    // 找出具体是哪一个团队
    var newteamList = TeamList.teamlist.find((item) => {
        return item.key == teamid
    })
    console.log(newteamList);
    console.log(doctorid);
    // 找出是这个团队的具体哪一个医生
    var newdoctorlist = newteamList.teamMember.find((item) => {
        return item.key == doctorid
    })
    if (newdoctorlist.length != 0) {
        return { code: 200, doctorDetail: newdoctorlist }
    }
})
// 查询服务包详情
Mock.mock('/api/team/package', 'post', (req) => {
    var { teamid, packageid } = JSON.parse(req.body)
    // 找出具体是哪一个团队
    var newteamList = TeamList.teamlist.find((item) => {
        return item.key == teamid
    })
    // 找出是这个团队的具体哪一个服务包
    var newpackagelist = newteamList.servicePackage.find((item) => {
        return item.packageid == packageid
    })
    if (newpackagelist.length != 0) {
        return { code: 200, packageDetail: newpackagelist }
    }
})
// 删除服务包
Mock.mock('/api/team/yichupackage', 'post', (req) => {
    var { key, packageid } = JSON.parse(req.body)
    var packlist = TeamList.teamlist.find((item) => {
        return item.key == key
    })
    if (packlist.length != 0) {
        var index = packlist.servicePackage.findIndex((item) => {
            return item.packageid == packageid
        })
        if (index != -1) {
            packlist.servicePackage.splice(index, 1)
        }
        localStorage.setItem('team_list', JSON.stringify(TeamList))
        return { code: 200, packageDetail: packlist.servicePackage }

    }
})
// 新增服务包
Mock.mock('/api/team/addPackage', 'post', (req) => {
    var { key, packageid } = JSON.parse(req.body)
    var newpackage = servicePackage.servicepackage.filter((item) => {
        return item.packageid == packageid
    })
    var index = TeamList.teamlist.findIndex((item) => {
        return item.key == key
    })
    if (index != -1) {
        TeamList.teamlist[index].servicePackage.push(newpackage[0])
    }
    localStorage.setItem('team_list', JSON.stringify(TeamList))
    return { code: 200 }
})
// 申请加入团队
Mock.mock('/api/team/join', 'post', (req) => {
    const { val } = JSON.parse(req.body)
    var newlist = TeamList.teamlist.filter((item) => {
        return item.key == val
    })
    if (newlist.length != 0) {
        return { code: 200, teamlist: newlist }
    } else {
        return { code: 300 }
    }
})
// 查询团队信息
// 通过下拉框搜索
var newTeamlist = []
Mock.mock('/api/team/search', 'post', (req) => {
    var { orgin } = JSON.parse(req.body)
    // console.log(orgin)
    newTeamlist = TeamList.teamlist.filter((item) => {
        return item.orgin == orgin
    })
    console.log(newTeamlist)
    return { code: 200, msg: "查询成功", teamlist: newTeamlist }
})
// 通过input框进行搜索
Mock.mock('/api/team/search_a', 'post', (req) => {
    console.log(newTeamlist);
    var { captain } = JSON.parse(req.body)
    var pattern = new RegExp("[\u4E00-\u9FA5]+")
    if (newTeamlist.length != 0) {
        if (pattern.test(captain)) {
            var newTeamlist_a = newTeamlist.filter((item) => {
                return item.captain == captain
            })
            return { code: 200, msg: "查询成功", teamlist: newTeamlist_a }
        } else {
            var newTeamlist_a = newTeamlist.filter((item) => {
                return item.key == captain
            })
            return { code: 200, msg: "查询成功", teamlist: newTeamlist_a }
        }
    } else {
        if (pattern.test(captain)) {
            var newTeamlist_b = TeamList.teamlist.filter((item) => {
                return item.captain == captain
            })
            console.log(newTeamlist_b)
            return { code: 200, msg: "查询成功", teamlist: newTeamlist_b }
        } else {
            var newTeamlist_b = TeamList.teamlist.filter((item) => {
                return item.key == captain
            })
            console.log(newTeamlist_b)
            return { code: 200, msg: "查询成功", teamlist: newTeamlist_b }
        }
    }
})
// 通过编号搜索
Mock.mock('/api/team/search_b', 'post', (req) => {
    var { key } = JSON.parse(req.body)
    console.log(key)
    var newTeamlist_a = TeamList.teamlist.find((item) => {
        return item.key == key
    })
    if (newTeamlist_a.length != 0) {
        return { code: 200, msg: "查询成功", teamlist: newTeamlist_a }
    } else {
        return { code: 300, msg: "未查询到" }
    }



})
// 退出队伍
Mock.mock('/api/team/exitteam', 'post', (req) => {
    var { key } = JSON.parse(req.body)
    var index = TeamList.teamlist.findIndex((item) => {
        return item.key == key
    })
    if (index != -1) {
        TeamList.teamlist.splice(index, 1)
        localStorage.setItem('team_list', JSON.stringify(TeamList))
        return { code: 200, msg: "退出", teamlist: TeamList.teamlist }
    }
})
// ----------------------------------------------------
// 模拟医生管理的数据
var DoctorList = {}
var doctorlist = TeamList.teamlist.map(item => {
    return item.teamMember
});
// doctorlist.map(item => item)
// var arr = [].concat.apply([], doctorlist)
var arr = doctorlist.flat()
if (localStorage.getItem('doctor_list')) {
    DoctorList = JSON.parse(localStorage.getItem('doctor_list'))
} else {
    DoctorList = Mock.mock({
        "doctorlist": [
            ...arr
            // {
            //     key: '@id',
            //     sex: "@pick(['男','女'])",
            //     images: Random.image('100x100', '#50B347', '#FFF', 'Head'),
            //     name: "@cname",
            //     'phone|13000000000-19999999999': 1,
            //     role: "@pick(['全科医师','主任医师','儿科医师','副主任医师'])",
            //     team: "@pick(['李军团队','汪小敏团队','李明团队'])",
            //     orgin: '@county(true)' + '卫生服务站',
            //     status: "@boolean",
            //     doctorbreif: "1、北京大学第一医院内分泌科专家，从事内分泌相关疾病35余年，尤其擅长代谢性疾病引起的不孕不育诊疗，现任北京天伦医院不孕不育妇科特聘专家。",
            //     doctoradept: "甲状腺疾病及内分泌疾病，擅长甲亢、甲减、桥本病、糖尿病、多囊卵巢、代谢异常、甲状腺炎、甲状腺低下、甲状腺功能减退、甲状腺功能失调、甲状腺激素异常等引起的不孕不育症的治疗和孕期管理、生活指导，在治疗各种代谢内分泌异常方面有丰富的临床经验。"
            // }
        ]
    })
    localStorage.setItem('doctor_list', JSON.stringify(DoctorList))
}
// 获取医生团队列表
Mock.mock('/api/doctor/list', 'get', () => {
    console.log(DoctorList);
    return DoctorList
})
// 修改医生状态
Mock.mock('/api/doctor/setStatus', 'post', (req) => {
    console.log(req);
    const { key } = JSON.parse(req.body)
    var index = DoctorList.doctorlist.findIndex(item => {
        return item.key == key
    })
    console.log(index)
    if (index != -1) {
        DoctorList.doctorlist[index].status = !DoctorList.doctorlist[index].status
        localStorage.setItem('doctor_list', JSON.stringify(DoctorList))
        return { code: 200, msg: '修改成功', doctorlist: DoctorList.doctorlist, status: DoctorList.doctorlist[index].status }
    }
})
// 查询具体医生
Mock.mock('/api/doctor/doctorlist', 'post', (req) => {
    var { key } = JSON.parse(req.body)
    console.log(key);
    var list = DoctorList.doctorlist.find((item) => {
        return item.key == key
    })
    return { code: 200, doctorlist: list }
})
// 查询医生信息
// 通过下拉框(所属机构)搜索
var doctorlist = []
var teamlist = []
Mock.mock('/api/doctor/search', 'post', (req) => {
    var { doctororgin } = JSON.parse(req.body)
    doctorlist = DoctorList.doctorlist.filter((item) => {
        return item.orgin == doctororgin
    })
    console.log(doctorlist)
    return { code: 200, msg: "查询成功", doctorlist: doctorlist }
})
// 通过下拉框(所属团队)搜索
Mock.mock('/api/doctor/search_b', 'post', (req) => {
    var { doctorteam } = JSON.parse(req.body)
    console.log(doctorteam);
    if (doctorlist.length != 0) {
        teamlist = doctorlist.filter((item) => {
            return item.team == doctorteam
        })
        console.log(doctorlist)
        return { code: 200, msg: "查询成功", doctorlist: teamlist }
    } else {
        teamlist = DoctorList.doctorlist.filter((item) => {
            return item.team == doctorteam
        })
        console.log(teamlist);
        console.log(doctorlist)
        return { code: 200, msg: "查询成功", doctorlist: teamlist }
    }
})
// 通过input框进行搜索
Mock.mock('/api/doctor/search_a', 'post', (req) => {
    console.log(doctorlist);
    var { value } = JSON.parse(req.body)
    var pattern = new RegExp("[\u4E00-\u9FA5]+")
    if (doctorlist.length != 0) {
        if (teamlist.length != 0) {
            if (pattern.test(value)) {
                var doctorlist_a = teamlist.filter((item) => {
                    return item.name == value
                })
                return { code: 200, msg: "查询成功", doctorlist: doctorlist_a }
            } else {
                var doctorlist_a = teamlist.filter((item) => {
                    return item.key == value
                })
                return { code: 200, msg: "查询成功", doctorlist: doctorlist_a }
            }
        } else {
            if (pattern.test(value)) {
                var doctorlist_a = doctorlist.filter((item) => {
                    return item.name == value
                })
                return { code: 200, msg: "查询成功", doctorlist: doctorlist_a }
            } else {
                var doctorlist_a = doctorlist.filter((item) => {
                    return item.key == value
                })
                return { code: 200, msg: "查询成功", doctorlist: doctorlist_a }
            }
        }
    } else if (teamlist.length != 0) {
        if (doctorlist.length != 0) {
            if (pattern.test(value)) {
                var doctorlist_a = doctorlist.filter((item) => {
                    return item.name == value
                })
                return { code: 200, msg: "查询成功", doctorlist: doctorlist_a }
            } else {
                var doctorlist_a = doctorlist.filter((item) => {
                    return item.key == value
                })
                return { code: 200, msg: "查询成功", doctorlist: doctorlist_a }
            }
        } else {
            if (pattern.test(value)) {
                var doctorlist_a = teamlist.filter((item) => {
                    return item.name == value
                })
                return { code: 200, msg: "查询成功", doctorlist: doctorlist_a }
            } else {
                var doctorlist_a = teamlist.filter((item) => {
                    return item.key == value
                })
                return { code: 200, msg: "查询成功", doctorlist: doctorlist_a }
            }
        }
    } else {
        if (pattern.test(value)) {
            var doctorlist_b = DoctorList.doctorlist.filter((item) => {
                console.log(item);
                return item.name == value
            })
            console.log(doctorlist_b)
            return { code: 200, msg: "查询成功", doctorlist: doctorlist_b }
        } else {
            var doctorlist_b = DoctorList.doctorlist.filter((item) => {
                return item.key == value
            })
            console.log(doctorlist_b)
            return { code: 200, msg: "查询成功", doctorlist: doctorlist_b }
        }
    }
})
// 编辑医生信息
Mock.mock('/api/doctor/edit', 'post', (req) => {

    var editdoctor = JSON.parse(req.body)
    console.log(editdoctor)
    var key = editdoctor.key
    var index = DoctorList.doctorlist.findIndex((item) => {
        return item.key == key
    })
    console.log(index)
    if (DoctorList.doctorlist[index].name == editdoctor.name && DoctorList.doctorlist[index].phone == editdoctor.phone && DoctorList.doctorlist[index].role == editdoctor.role && DoctorList.doctorlist[index].sex == editdoctor.sex && DoctorList.doctorlist[index].doctoradept == editdoctor.doctoradept && DoctorList.doctorlist[index].doctorbreif == editdoctor.doctorbreif) {
        console.log(111)
        return { code: '304', status: 'no', msg: '请做出修改' }
    } else {
        DoctorList.doctorlist[index].key = editdoctor.key
        DoctorList.doctorlist[index].name = editdoctor.name
        DoctorList.doctorlist[index].phone = editdoctor.phone
        DoctorList.doctorlist[index].role = editdoctor.role
        DoctorList.doctorlist[index].sex = editdoctor.sex
        DoctorList.doctorlist[index].doctoradept = editdoctor.doctoradept
        DoctorList.doctorlist[index].doctorbreif = editdoctor.doctorbreif
        localStorage.setItem('doctor_list', JSON.stringify(DoctorList))
        return {
            code: '200',
            status: 'ok',
            doctorlist: DoctorList.doctorlist
        }
    }
})
// 新增医生
Mock.mock('/api/doctor/add', 'post', (req) => {
    var json = JSON.parse(req.body)
    console.log(json)
    DoctorList.doctorlist.push(json)
    localStorage.setItem('doctor_list', JSON.stringify(DoctorList))
    return { code: 200, msg: '新增成功', doctorlist: DoctorList.doctorlist }
})
// 删除医生
Mock.mock('/api/doctor/del', 'post', (req) => {
    var { key } = JSON.parse(req.body)
    var index = DoctorList.doctorlist.findIndex((item) => {
        return item.key == key
    })
    if (index != -1) {
        DoctorList.doctorlist.splice(index, 1)
        localStorage.setItem('doctor_list', JSON.stringify(DoctorList))
        return { code: 200, doctorlist: DoctorList.doctorlist }
    }
})
// -------------------------------------------------------------
// 服务包管理
var servicePackage = {}
if (localStorage.getItem('servicepackage')) {
    servicePackage = JSON.parse(localStorage.getItem('servicepackage'))
} else {
    servicePackage = Mock.mock({
        "servicepackage|10": [
            {
                packageid: "@id",
                packagename: "@pick(['基础包','老人服务包','儿童服务包','定制包'])",
                images: function () {
                    let packagename = this.packagename
                    if (packagename == '基础包') {
                        return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%96%B0%E5%A2%9E%E5%9B%A2%E9%98%9F%E4%BF%A1%E6%81%AF/u5584.png'
                    } else if (packagename == '老人服务包') {
                        return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%96%B0%E5%A2%9E%E5%9B%A2%E9%98%9F%E4%BF%A1%E6%81%AF/u5793.png'
                    } else if (packagename == '儿童服务包') {
                        return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%9C%8D%E5%8A%A1%E5%8C%85%E7%AE%A1%E7%90%86/u6763.png'
                    } else {
                        return 'https://cdn7.axureshop.com/demo/1881228/images/%E6%9C%8D%E5%8A%A1%E5%8C%85%E7%AE%A1%E7%90%86/u6793.svg'
                    }
                },
                "packagestatus|1-4": 1,
                packageobject: function () {
                    let packagename = this.packagename
                    if (packagename == '基础包') {
                        return '所有人'
                    } else if (packagename == '老人服务包') {
                        return '55岁以上老人'
                    } else if (packagename == '儿童服务包') {
                        return '0-6岁的儿童'
                    } else {
                        return '定制的专属对象'
                    }
                },
                "price|200-500": 1,
                "title|1-3": {
                    "01": "高血压",
                    "02": "糖尿病",
                    "03": "高血糖",
                    "04": "冠心病",
                    "05": "肺结核",
                    "06": "高血糖",
                    "07": "肾结石",
                    "08": "胆囊炎",
                    "09": "低血糖",
                },
                "period|1-5": 1 + '年',
                introduce: "主要以基本医疗服务和公共卫生服务为主，如部分常见病或 多发病的治疗和用药指导、重症的就医指导和转诊预约、居民健康档案的管理和 慢病管理指导等。",
                "servicesAvailable|3": [
                    {
                        Availableid: "@id",
                        Availablename: "高血压随访服务",
                        "Availablenum|1-5": 1,
                        Availabletype: "@pick(['免费项目','付费项目'])",
                        Availableintroduce: "1.血压测量及记录 2.根据结果提供综合性健康指导"
                    }
                ]
            }]
    })
    localStorage.setItem('servicepackage', JSON.stringify(servicePackage))
}
// 获取服务包列表
Mock.mock('/api/service/package', 'get', () => {
    servicelist.length = 0
    return servicePackage
})
// 新增服务包
Mock.mock('/api/service/addpackage', 'post', (req) => {
    var json = JSON.parse(req.body)
    servicePackage.servicepackage.push(json)
    localStorage.setItem('servicepackage', JSON.stringify(servicePackage))
    return { code: 200 }
})
// 编辑服务包
Mock.mock('/api/service/editpackage', 'post', (req) => {
    var json = JSON.parse(req.body)
    var index = servicePackage.servicepackage.findIndex((item) => {
        return item.packageid == json.packageid
    })
    if (index != -1) {
        servicePackage.servicepackage[index].packagename = json.packagename
        servicePackage.servicepackage[index].packagestatus = json.packagestatus
        servicePackage.servicepackage[index].packageobject = json.packageobject
        servicePackage.servicepackage[index].period = json.period
        servicePackage.servicepackage[index].introduce = json.introduce
        servicePackage.servicepackage[index].servicesAvailable = json.servicesAvailable
        servicePackage.servicepackage[index].price = json.price
    }
    localStorage.setItem('servicepackage', JSON.stringify(servicePackage))
    return { code: 200 }

})
// 获取具体某一个服务包
Mock.mock('/api/service/packageDetail', 'post', (req) => {
    console.log(req);
    var { id } = JSON.parse(req.body)
    console.log(id);
    var index = servicePackage.servicepackage.find((item) => {
        return item.packageid == id
    })
    console.log(index);
    if (index) {
        return { code: 200, servicepackage: index }
    }
})
var servicelist = []
// 根据下拉框查询服务包
Mock.mock('/api/service/search_a', 'post', (req) => {
    var { packagestatus } = JSON.parse(req.body)
    if (packagestatus) {
        servicelist = servicePackage.servicepackage.filter((item) => {
            return item.packagestatus == packagestatus
        })
        console.log(servicelist)
        return { code: 200, msg: "查询成功", servicepackage: servicelist }
    }

})
// 通过input框进行搜索
Mock.mock('/api/service/search_b', 'post', (req) => {
    var { packagename } = JSON.parse(req.body)
    var pattern = new RegExp("[\u4E00-\u9FA5]+")
    if (servicelist.length != 0) {
        if (pattern.test(packagename)) {
            var servicelist_a = servicelist.filter((item) => {
                return item.packagename == packagename
            })
            return { code: 200, msg: "查询成功", servicepackage: servicelist_a }
        } else {
            var servicelist_a = servicelist.filter((item) => {
                return item.key == packagename
            })
            return { code: 200, msg: "查询成功", servicepackage: servicelist_a }
        }
    } else {
        if (pattern.test(packagename)) {
            var servicelist_b = servicePackage.servicepackage.filter((item) => {
                return item.packagename == packagename
            })
            return { code: 200, msg: "查询成功", servicepackage: servicelist_b }
        } else {
            var servicelist_b = servicePackage.servicepackage.filter((item) => {
                return item.key == packagename
            })
            return { code: 200, msg: "查询成功", servicepackage: servicelist_b }
        }
    }
})
// 修改服务包状态
Mock.mock('/api/servicepackage/setstatus', 'post', (req) => {
    var { id, status } = JSON.parse(req.body)
    var index = servicePackage.servicepackage.findIndex((item) => {
        return item.packageid == id
    })
    if (index != -1) {
        if (status == 2) {
            servicePackage.servicepackage[index].packagestatus = 1
        } else if (status == 3) {
            servicePackage.servicepackage[index].packagestatus = 1
        } else {
            servicePackage.servicepackage[index].packagestatus = 3
        }
        localStorage.setItem('servicepackage', JSON.stringify(servicePackage))
        return { code: 200, servicepackage: servicePackage.servicepackage[index] }
    }

})
// 修改服务包状态
Mock.mock('/api/servicepackage/bohui', 'post', (req) => {
    var { id, status } = JSON.parse(req.body)
    var index = servicePackage.servicepackage.findIndex((item) => {
        return item.packageid == id
    })
    if (index != -1) {
        servicePackage.servicepackage[index].packagestatus = 4
        localStorage.setItem('servicepackage', JSON.stringify(servicePackage))
        return { code: 200, servicepackage: servicePackage.servicepackage[index] }
    }

})
// -----------------------------------------------
// 服务项目管理
var servicesAvailable = {}
if (localStorage.getItem('service_available')) {
    servicesAvailable = JSON.parse(localStorage.getItem('service_available'))
} else {
    servicesAvailable = Mock.mock({
        "available|30": [
            {
                Availableid: "@id",
                Availablename: "高血压随访服务",
                "Availableprice|100-500": 1,
                "Availablenum|1-5": 1,
                Availabletype: "@pick(['免费项目','付费项目'])",
                Availableintroduce: "1.血压测量及记录 2.根据结果提供综合性健康指导"
            }
        ]
    })
    localStorage.setItem('service_available', JSON.stringify(servicesAvailable))
}
// 查询服务项目
Mock.mock('/api/service/servicesAvailable', 'get', () => {
    return servicesAvailable
})
// 根据编号查询
Mock.mock('/api/service/searchProject', 'post', (req) => {
    var { key } = JSON.parse(req.body)
    var newproject = servicesAvailable.available.filter(item => {
        return item.Availableid == key
    })
    if (newproject.length != 0) {
        return { code: 200, available: newproject[0] }
    }
})
// 新增项目
Mock.mock('/api/available/project', 'post', (req) => {
    var json = JSON.parse(req.body)
    servicesAvailable.available.push(json)
    localStorage.setItem('service_available', JSON.stringify(servicesAvailable))
    return { code: 200 }
})
// 删除服务项目
Mock.mock('/api/service/delAvailable', 'post', (req) => {
    var { key } = JSON.parse(req.body)
    console.log(key);
    var index = servicesAvailable.available.findIndex((item) => {
        return item.Availableid == key
    })
    console.log(index);
    if (index != -1) {
        servicesAvailable.available.splice(index, 1)
        localStorage.setItem('service_available', JSON.stringify(servicesAvailable))
        return { code: 200, msg: "删除成功", available: servicesAvailable.available }
    }
})
// 通过下拉框搜索
var newAvailablelist = []
Mock.mock('/api/available/search', 'post', (req) => {
    var { Availabletype } = JSON.parse(req.body)
    newAvailablelist = servicesAvailable.available.filter((item) => {
        return item.Availabletype == Availabletype
    })
    console.log(newAvailablelist)
    return { code: 200, msg: "查询成功", available: newAvailablelist }
})
// 通过input框进行搜索
Mock.mock('/api/available/search_a', 'post', (req) => {
    console.log(newAvailablelist);
    var { Availablevalue } = JSON.parse(req.body)
    var pattern = new RegExp("[\u4E00-\u9FA5]+")
    if (newAvailablelist.length != 0) {
        if (pattern.test(Availablevalue)) {
            var newAvailablelist_a = newAvailablelist.filter((item) => {
                return item.Availablename == Availablevalue
            })
            return { code: 200, msg: "查询成功", available: newAvailablelist_a }
        } else {
            var newAvailablelist_a = newAvailablelist.filter((item) => {
                return item.Availableid == Availablevalue
            })
            return { code: 200, msg: "查询成功", available: newAvailablelist_a }
        }
    } else {
        if (pattern.test(Availablevalue)) {
            var newAvailablelist_b = servicesAvailable.available.filter((item) => {
                return item.Availablename == Availablevalue
            })
            console.log(newAvailablelist_b)
            return { code: 200, msg: "查询成功", available: newAvailablelist_b }
        } else {
            var newAvailablelist_b = servicesAvailable.available.filter((item) => {
                return item.Availableid == Availablevalue
            })
            console.log(newAvailablelist_b)
            return { code: 200, msg: "查询成功", available: newAvailablelist_b }
        }
    }
})
// 编辑服务项目
Mock.mock('/api/service/editproject', 'post', (req) => {
    var json = JSON.parse(req.body)
    var index = servicesAvailable.available.findIndex((item) => {
        return item.Availableid == json.Availableid
    })
    if (servicesAvailable.available[index].Availablename == json.Availablename && servicesAvailable.available[index].Availableprice == json.Availableprice && servicesAvailable.available[index].Availablenum == json.Availablenum && servicesAvailable.available[index].Availabletype == json.Availabletype && servicesAvailable.available[index].Availableintroduce == json.Availableintroduce) {
        console.log(111)
        return { code: '304', status: 'no', msg: '请做出修改' }
    } else {
        servicesAvailable.available[index].Availablename = json.Availablename
        servicesAvailable.available[index].Availableprice = json.Availableprice
        servicesAvailable.available[index].Availablenum = json.Availablenum
        servicesAvailable.available[index].Availabletype = json.Availabletype
        servicesAvailable.available[index].Availableintroduce = json.Availableintroduce
        localStorage.setItem('service_available', JSON.stringify(servicesAvailable))
    }
    return { code: 200 }
})