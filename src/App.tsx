import React, { Suspense } from 'react';
import './App.css';
import { Spin } from 'antd';
import routes from './routers';
import { useRoutes } from 'react-router-dom';
// 引入页面框架
import MyLayout from './components/MyLayout';

function App() {
  return (
    <div className="App">
      {/* 渲染路由规则出口 */}
      {/* <Suspense fallback={<Spin />}> */}
      {
        useRoutes(routes)
      }
      {/* </Suspense> */}

    </div>
  );
}

export default App;
